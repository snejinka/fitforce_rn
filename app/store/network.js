import {NetInfo} from 'react-native'

const SET_NETWORK_STATUS = 'SET_NETWORK_STATUS';

const initialState = {
    isOnlineNetwork:false
}

export const setNetworkStatus = () => (dispatch) => {
    NetInfo.isConnected.fetch().then(isOnlineNetwork => {
        dispatch({
            type:SET_NETWORK_STATUS,
            isOnlineNetwork
        })
    });
    function handleFirstConnectivityChange(isOnlineNetwork) {
        dispatch({
            type:SET_NETWORK_STATUS,
            isOnlineNetwork
        })
        NetInfo.isConnected.removeEventListener(
            'change',
            handleFirstConnectivityChange
        );
    }
    NetInfo.isConnected.addEventListener(
        'change',
        handleFirstConnectivityChange
    );
}



const ACTION_HANDLERS = {
    [SET_NETWORK_STATUS]:(state,action) => ({
        ...state,
        isOnlineNetwork: action.isOnlineNetwork
    })
}


export default function network(state = initialState, action) {
    const reduceFn = ACTION_HANDLERS[action.type];
    if (!reduceFn) return state;
    return reduceFn(state, action);
}