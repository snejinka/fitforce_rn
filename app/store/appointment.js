import * as api from '../api'
import ErrorModal from '../utils/ErrorModal';
import RealmModels from '../models/RealmModels';
import uuid from 'uuid';
import moment from 'moment';

const
    SET_APPOINTMENTS = 'SET_APPOINTMENTS',
    SET_SELECTED_APPOINTMENT = 'SET_SELECTED_APPOINTMENT';

const initialState = {
    appointments:[],
    selectedAppointment:{}
}

function getNotDeletedAppointments() {
    return RealmModels.objects('Appointment').filtered('deleted=false');
}

function getDeletedAppointmentsIds() {
    let appointments = RealmModels.objects('Appointment').filtered('deleted=true'),
        appointmentsIds = [];
    appointments.forEach((item) => {
        appointmentsIds.push(item.id)
    })
    return appointmentsIds;
}

function getDeletedAppointments() {
    let appointments = RealmModels.objects('Appointment').filtered('deleted=true');
    return appointments;
}

function getNewAppointments() {
    let appointments = getNotDeletedAppointments();
    return appointments.filtered('new=true')
}

function getUnsyncAppointments() {
    let appointments = getNotDeletedAppointments();
    return appointments.filtered('synchronized=false');
}

function getRealmClientAppointments(client_id) {
    let clientAppointments = [];
    getNotDeletedAppointments().filter(appointment => {
        if(appointment.client_id == client_id){
            clientAppointments.push(appointment)
        }
    });
    return clientAppointments;

}

function getSerialAppointments(serial_id) {
    let serialAppointments = [];
    getNotDeletedAppointments().filter(appointment => {
        if(appointment.serial_id == serial_id){
            serialAppointments.push(appointment)
        }
    });
    return serialAppointments;
}

function getAppointmentsByDate(appointments) {
    let appointmentsByDate = [];
    appointments.filter(appointment => {
        if(moment(appointment.date).format('MM/DD/YYYY') >= moment().format('MM/DD/YYYY')){
            appointmentsByDate.push(appointment)
        }
    });

    return appointmentsByDate;
}

function getAppointmentsById(id) {
    getNotDeletedAppointments().filter(appointment => {
        if(appointment.id == id){
            return appointment;
        }
    });
}

function formatAppointmentObjectForRealm(appointment) {
    return({
        id: appointment.id,
        currency: appointment.currency,
        amount_in_cents: appointment.amount_in_cents,
        client_id: appointment.client_id,
        frequency: appointment.frequency,
        type: appointment.type,
        address: appointment.address,
        time_start: appointment.time_start,
        time_end: appointment.time_end,
        date: appointment.date,
        description: appointment.description,
        fitness_programm: appointment.fitness_programm,
    })
}

function setAppointments(appointments) {
    return{
        type:SET_APPOINTMENTS,
        appointments
    }
}

function setSelectedAppointment(selectedAppointment) {
    return{
        type:SET_SELECTED_APPOINTMENT,
        selectedAppointment
    }
}

export const getAppointments = () => (dispatch,getState) => {
    const state = getState(),
        token = state.user.idToken,
        {isOnlineNetwork} = state.network;
    dispatch(setAppointments([...getNotDeletedAppointments()]))
    if(isOnlineNetwork){
        syncCreateAppointments(token,dispatch)
        syncEditAppointments(token,dispatch)
        syncDeleteAppointments(token,dispatch)
        return api.fetchAppointments(token)
            .then((response) => response.json())
            .then((responseJson) => {
                RealmModels.write(() => {
                    responseJson.appointments.forEach((appointment) => {
                        RealmModels.create('Appointment', {...formatAppointmentObjectForRealm(appointment),new:false,synchronized:true},true)
                        dispatch(setAppointments([...getNotDeletedAppointments()]))
                    })
                });
            })
    }
}

export const getClientAppointments = (client_id) => (dispatch,getState) => {
    const state = getState(),
        token = state.user.idToken,
        {isOnlineNetwork} = state.network;
    dispatch(setAppointments(getRealmClientAppointments(client_id)))
    if(isOnlineNetwork){
        return api.fetchClientsAppointments(token,client_id)
    }
}

export const getSelectedAppointment = appointment_id => (dispatch) => {
    return new Promise((resolve,reject) => {
        getNotDeletedAppointments().filter(appointment => {
            if(appointment.id === appointment_id){
                dispatch(setSelectedAppointment(appointment))
                resolve()
            }
        });
    })

};


export const createAppointments = (appointments) => (dispatch,getState) => {
    const state = getState(),
        token = state.user.idToken,
        {isOnlineNetwork} = state.network;
    return new Promise((resolve,reject) => {
        RealmModels.write(() => {
            appointments.forEach((item) => {
                RealmModels.create('Appointment', {
                    ...item,
                    new:true,
                    synchronized:true
                })
                dispatch(setAppointments([...getRealmClientAppointments(item.client_id)]))
                ErrorModal.show('Appointment was created successfully.')
            })
        })
        if(isOnlineNetwork){
            syncCreateAppointments(token,dispatch)
        }
    })
}

function syncCreateAppointments(token,dispatch) {
    return new Promise((resolve,reject) => {
        if (getNewAppointments().length > 0) {
            return api.fetchCreateAppointments(token, [...getNewAppointments()])
                .then((response) => response.json())
                .catch((err) => reject(err))
                .then((responseJson) => {
                    RealmModels.write(() => {
                        responseJson.created.forEach((appointment) => {
                            RealmModels.create('Appointment', {...appointment,new:false},true)
                        })
                        resolve(responseJson)
                    });
                })
        }
    })
}

export const editSingleAppointments = (appointment) => (dispatch,getState) => {
    const state = getState(),
        token = state.user.idToken,
        {isOnlineNetwork} = state.network;
    RealmModels.write(() => {
        RealmModels.create('Appointment', {...appointment,new:false,synchronized:false},true)
        dispatch(setAppointments([...getNotDeletedAppointments()]))
        ErrorModal.show('Appointment was updated successfully.')
    })
    if(isOnlineNetwork){
        syncEditAppointments(token,dispatch)
    }
}

export const editSerialAppointments = (appointment) => (dispatch,getState) => {
    const state = getState(),
        token = state.user.idToken,
        {isOnlineNetwork} = state.network;
    if(appointment.serial_id){
        let serialAppointments = getSerialAppointments(appointment.serial_id),
            appointmentsByDate = getAppointmentsByDate(serialAppointments);
        RealmModels.write(() => {
            appointmentsByDate.forEach((item) => {
                if(appointment.id == item.id){
                    RealmModels.create('Appointment', {
                        ...appointment,
                        new: false,
                        synchronized: false
                    }, true)
                }
                if(appointment.id != item.id) {
                    RealmModels.create('Appointment', {
                        ...item,
                        time_start:appointment.time_start,
                        time_end:appointment.time_end,
                        address:appointment.address,
                        currency:appointment.currency,
                        amount_in_cents:appointment.amount_in_cents,
                        description:appointment.description,
                        new: false,
                        synchronized: false
                    }, true)
                }
                dispatch(setSelectedAppointment(appointment))
                dispatch(setAppointments([...getNotDeletedAppointments()]))
                ErrorModal.show('Appointments was updated successfully.')
            })
        })
    }
    if(isOnlineNetwork){
        syncEditAppointments(token,dispatch)
    }
}

function syncEditAppointments(token,dispatch) {
    return new Promise((resolve,reject) => {
        if (getUnsyncAppointments().length > 0) {
            return api.fetchEditSingleAppointments(token, [...getUnsyncAppointments()])
                .then((response) => response.json())
                .catch((err) => {
                    syncCreateAppointments(token,dispatch)
                    reject(responseJson)
                })
                .then((responseJson) => {
                    RealmModels.write(() => {
                        responseJson.updated.forEach((appointment) => RealmModels.create('Appointment', {...appointment,synchronized:true},true))
                        dispatch(setAppointments([...getNotDeletedAppointments()]))
                        resolve(responseJson)
                    });
                })
        }
    })
}

export const deleteAppointment = (appointment) => (dispatch,getState) => {
    const state = getState(),
        token = state.user.idToken,
        {isOnlineNetwork} = state.network;
    RealmModels.write(() => {
        RealmModels.create('Appointment', {
            ...appointment,
            new: false,
            synchronized: false,
            deleted:true
        }, true)
    })
    dispatch(setAppointments([...getNotDeletedAppointments()]))
    ErrorModal.show('Appointments was deleted successfully.')
    if(isOnlineNetwork){
        syncDeleteAppointments(token,dispatch)
    }
}

export const deleteSerialAppointments = (appointment) => (dispatch,getState) => {
    const state = getState(),
        token = state.user.idToken,
        {isOnlineNetwork} = state.network;
    if(appointment.serial_id){
        let serialAppointments = getSerialAppointments(appointment.serial_id);
        RealmModels.write(() => {
            serialAppointments.forEach((item) => {
                RealmModels.create('Appointment', {
                    ...item,
                    new: false,
                    synchronized: false,
                    deleted:true
                },true)
            })
        })
        dispatch(setAppointments([...getNotDeletedAppointments()]))
        ErrorModal.show('Serial appointments were deleted successfully.')
        if(isOnlineNetwork){
            syncDeleteAppointments(token,dispatch)
        }
    }
}

function syncDeleteAppointments(token,dispatch) {
    return new Promise((resolve,reject) => {
        if (getDeletedAppointmentsIds().length > 0) {
            return api.fetchDeleteAppointments(token, getDeletedAppointmentsIds())
                .then((response) => response.json())
                .then((responseJson) => {
                    if (responseJson.success) {
                        RealmModels.write(() => {
                            RealmModels.delete(getDeletedAppointments())
                            dispatch(setAppointments([...getNotDeletedAppointments()]))
                            resolve(responseJson)
                        })

                    } else {
                        reject(responseJson)
                    }
                })
        }
    })
}

export const hardDeleteAppointments = () => (dispatch) => {
    console.warn('hardDeleteAppointments')
    RealmModels.write(() => {
        RealmModels.delete(RealmModels.objects('Appointment'));
    })
    dispatch(setAppointments([]))
}



const ACTION_HANDLERS = {
    [SET_APPOINTMENTS](state,action){
        return{
            ...state,
            appointments:action.appointments
        }
    },
    [SET_SELECTED_APPOINTMENT](state,action){
        return{
            ...state,
            selectedAppointment:action.selectedAppointment
        }
    },
}

export default function appointment(state = initialState,action) {
    const reduceFn = ACTION_HANDLERS[action.type];
    if (!reduceFn) return state;
    return reduceFn(state, action);
}