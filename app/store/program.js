import * as api from '../api';
import ErrorModal from '../utils/ErrorModal';
import RealmModels from '../models/RealmModels';
import uuid from 'uuid';
import moment from 'moment';

const SET_FITNESS_PROGRAMS = 'SET_FITNESS_PROGRAMS';

const initialState = {
  fitness_programs: [
// {
//   id: 0,
//   name: 'Daily Stretching',
//   program_type: 'Flexibility Training',
//   program_level: 'Advanced',
//   description: '',
//   exercises: [
//     {
//       id: 0,
//       body_part: 'Quadriceps',
//       name: 'Chest push (multiple responce)',
//       sets: 6,
//       reps: 12,
//       rest: 20,
//       load: null
//     },
//     {
//       id: 1,
//       body_part: 'Quadriceps',
//       name: 'Narrow stance hack squats',
//       sets: 6,
//       reps: 12,
//       rest: 20,
//       load: null
//     },
//     {
//       id: 2,
//       body_part: 'Shoulders',
//       name: 'Lying rear delt raise',
//       sets: 6,
//       reps: 12,
//       rest: 20,
//       load: null
//     },
//     {
//       id: 3,
//       body_part: 'Shoulders',
//       name: 'Rack delivery',
//       sets: 6,
//       reps: 12,
//       rest: 20,
//       load: null
//     }
//   ]
// },
		// {
		// 	id: 1,
		// 	name: 'Gym Program for Intermediate',
		// 	program_type: 'Dynamic Strength Training',
		// 	program_level: 'Intermediate',
		// 	description: '',
		// 	exercises: []
		// },
		// {
		// 	id: 2,
		// 	name: 'Putting on Winter Mass',
		// 	program_type: 'Static Strength-training',
		// 	program_level: 'Athlete',
		// 	description: '',
		// 	exercises: []
		// },
		// {
		// 	id: 3,
		// 	name: 'Gym Program for Beginner. Day 1',
		// 	program_type: 'Aerobic Training',
		// 	program_level: 'Beginner',
		// 	description: '',
		// 	exercises: []
		// },
		// {
		// 	id: 4,
		// 	name: 'Gym Program for Beginner. Day 2',
		// 	program_type: 'Circuit Training',
		// 	program_level: 'Beginner',
		// 	description: '',
		// 	exercises: []
		// }
	]
}

function fetchFitnessPrograms() {
	return RealmModels.objects('FitnessProgram');
}

function setFitnessPrograms(data) {
	return {
		type: SET_FITNESS_PROGRAMS,
		data
	}
}

export const getFitnessPrograms = () => (dispatch,getState) => {
	const state = getState(),
    token = state.user.idToken,
    {isOnlineNetwork} = state.network;
	if (isOnlineNetwork) {
    return api.fetchFitnessPrograms(token)
    .then((response) => response.json())
    .then((responseJson) => {
      if (responseJson.fitness_programs && responseJson.fitness_programs.length) {
        return new Promise((resolve,reject) => {
          let fps = responseJson.fitness_programs;
          RealmModels.write(() => {
            // RealmModels.delete(RealmModels.objects('FitnessProgram'))
            fps.forEach((item) => {
              let exercises = item.exercises ? item.exercises : [];
              RealmModels.create('FitnessProgram', {...item, exercises: JSON.stringify(exercises)}, true)
            })
          })
          dispatch(setFitnessPrograms([...fetchFitnessPrograms()]))
          resolve()
        }).then(
          response => {},
          error => {}
          // console.warn(`${error}`)
        );
      }
    }).catch((err) => reject(err))
	} else {
    dispatch(setFitnessPrograms([...fetchFitnessPrograms()]))
  }
}

export const createFitnessProgram = (fitness_program) => (dispatch,getState) => {
	const state = getState(),
    token = state.user.idToken,
    {isOnlineNetwork} = state.network;
    if (isOnlineNetwork) {
      return api.createFitnessProgram(token, fitness_program)
        .then((response) => response.json())
        .then((responseJson) => {
          if (responseJson.success) {
            return new Promise((resolve,reject) => {
              let fp = responseJson.fitnessProgram;
              RealmModels.write(() => {
                RealmModels.create('FitnessProgram', {...fp, exercise: fp.exercise ? fp.exercise : [] })
                ErrorModal.show('Fitness Program was created successfully.')
              })
              dispatch(setFitnessPrograms([...fetchFitnessPrograms()]))
              resolve()
            }).then(
              response => {},
              error => {}
            );
          }
        }).catch((err) => reject(err))
    } else {
      ErrorModal.show('Please check Internet connection.')
    }
}

export const editFitnessProgram = (program) => (dispatch,getState) => {
  const state = getState(),
    token = state.user.idToken,
    {isOnlineNetwork} = state.network;
    if (isOnlineNetwork) {
      return api.updateFitnessProgram(token, program)
        .then((response) => response.json())
        .then((responseJson) => {
          if (responseJson.success) {
            return new Promise((resolve,reject) => {
              RealmModels.write(() => {
                let exercises = responseJson.fitnessProgramm.exercises ? responseJson.fitnessProgramm.exercises : [];
                RealmModels.create('FitnessProgram', {...responseJson.fitnessProgramm, exercises: JSON.stringify(exercises)}, true);
                ErrorModal.show('Fitness Program was updated successfully.')
              })
              dispatch(setFitnessPrograms([...fetchFitnessPrograms()]))
              resolve()
            }).then(
              response => {},
              error => {}
            );
          }
        }).catch((err) => reject(err))
    } else {
      ErrorModal.show('Please check Internet connection.')
    }
}

export const deleteFitnessProgram = (program) => (dispatch,getState) => {
  const state = getState(),
    token = state.user.idToken,
    {isOnlineNetwork} = state.network;
    if (isOnlineNetwork) {
      return api.deleteFitnessProgram(token, program.id)
        .then((response) => response.json())
        .then((responseJson) => {
          if (responseJson.success) {
            return new Promise((resolve,reject) => {
              let realmProgram = RealmModels.objects('FitnessProgram').find(item => item.id === program.id);
              RealmModels.write(() => {
                RealmModels.delete(realmProgram)
                ErrorModal.show('Fitness Program was deleted successfully.')
              })
              dispatch(setFitnessPrograms([...fetchFitnessPrograms()]))
              resolve()
            }).then(
              response => {},
              error => {}
            );
          }
        }).catch((err) => reject(err))
    } else {
      ErrorModal.show('Please check Internet connection.')
    }
}

export const addExercise = (program) => (dispatch,getState) => {
  const state = getState(),
    token = state.user.idToken,
    {isOnlineNetwork} = state.network;
    if (isOnlineNetwork) {
      return api.updateFitnessProgram(token, program)
        .then((response) => response.json())
        .then((responseJson) => {
          if (responseJson.success) {
            return new Promise((resolve,reject) => {
              let updatedProgram = responseJson.fitnessProgramm;
              RealmModels.write(() => {
                RealmModels.create('FitnessProgram', {...updatedProgram, exercises: JSON.stringify(updatedProgram.exercises)}, true)
                ErrorModal.show('Exercise was added successfully.')
              })
              dispatch(setFitnessPrograms([...fetchFitnessPrograms()]))
              resolve()
            }).then(
              response => {},
              error => {}
            );
          }
        }).catch((err) => reject(err))
    } else {
      ErrorModal.show('Please check Internet connection.')
    }
}

export const editExercise = (program) => (dispatch,getState) => {
  const state = getState(),
    token = state.user.idToken,
    {isOnlineNetwork} = state.network;
    if (isOnlineNetwork) {
      return api.updateFitnessProgram(token, program)
        .then((response) => response.json())
        .then((responseJson) => {
          if (responseJson.success) {
            return new Promise((resolve,reject) => {
              let updatedProgram = responseJson.fitnessProgramm;
              RealmModels.write(() => {
                RealmModels.create('FitnessProgram', {...updatedProgram, exercises: JSON.stringify(updatedProgram.exercises)}, true)
                ErrorModal.show('Exercise was updated successfully.')
              })
              dispatch(setFitnessPrograms([...fetchFitnessPrograms()]))
              resolve()
            }).then(
              response => {},
              error => {}
            );
          }
        }).catch((err) => reject(err))
    } else {
      ErrorModal.show('Please check Internet connection.')
    }
}

export const deleteExercise = (program) => (dispatch,getState) => {
  const state = getState(),
    token = state.user.idToken,
    {isOnlineNetwork} = state.network;
    if (isOnlineNetwork) {
      return api.updateFitnessProgram(token, program)
        .then((response) => response.json())
        .then((responseJson) => {
          if (responseJson.success) {
            return new Promise((resolve,reject) => {
              let updatedProgram = responseJson.fitnessProgramm;
              RealmModels.write(() => {
                RealmModels.create('FitnessProgram', {...updatedProgram, exercises: JSON.stringify(updatedProgram.exercises)}, true)
                ErrorModal.show('Exercise was deleted successfully.')
              })
              dispatch(setFitnessPrograms([...fetchFitnessPrograms()]))
              resolve()
            }).then(
              response => {},
              error => {}
            );
          }
        }).catch((err) => reject(err))
    } else {
      ErrorModal.show('Please check Internet connection.')
    }
}

const ACTION_HANDLERS = {
    [SET_FITNESS_PROGRAMS](state,action){
        return{
            ...state,
            fitness_programs: action.data
        }
    }
}

export default function program(state = initialState,action) {
    const reduceFn = ACTION_HANDLERS[action.type];
    if (!reduceFn) return state;
    return reduceFn(state, action);
}