import { combineReducers } from 'redux';
import Auth0 from 'react-native-auth0';
import user from './user';
import onboarding from './onboarding';
import network from './network';
import client from './client';
import appointment from './appointment';
import invoice from './invoice';
import program from './program';

export const auth0 = new Auth0({
  domain: 'shatalovbs.eu.auth0.com',
  clientId: 'WVWWRw7ExaC3L9Fjm7gsqG3Am7uio5qC',
});

const rootReducer = combineReducers({
  user,
  onboarding,
  network,
  client,
  appointment,
  invoice,
  program
});

export default rootReducer;
