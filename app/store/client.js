import * as api from '../api'
import RealmModels from '../models/RealmModels';
import Realm from 'realm';
import uuid from 'uuid';
import ErrorModal from '../utils/ErrorModal';


const SET_CLIENTS = 'SET_CLIENTS';
const SET_CLIENT = 'SET_CLIENT';


const initialState = {
    clients:[],
    client:{}
};

function setClientsInfo(clients) {
    return {
        type:SET_CLIENTS,
        clients
    }
}

function setClientInfo(client) {
    return{
        type:SET_CLIENT,
        client
    }
}

function getNotDeletedClients() {
    let clients = RealmModels.objects('Client');
    return clients.filtered('deleted=false');
}

function getDeletedClients() {
    let clients = RealmModels.objects('Client');
    return clients.filtered('deleted=true');
}

function getDeletedClientsIds() {
    let clients = getDeletedClients(),
        clientsIds = [];
    clients.filter(client => {
        clientsIds.push(client.id)
    });
    return clientsIds;
}

function getUnsyncClients() {
    let clients = getNotDeletedClients();
    return clients.filtered('synchronized=false');

}

function getNewClients() {
    let clients = getNotDeletedClients();
    return clients.filtered('new=true');

}

function syncEditClients(token,dispatch) {
    return new Promise((resolve,reject) => {
        if(getUnsyncClients().length > 0){
            return api.fetchEditClients(token, getUnsyncClients())
                .then((response) => response.json())
                .then((responseJson) => {
                    if(responseJson.success){
                        RealmModels.write(() => {
                            getUnsyncClients().forEach((client) => RealmModels.create('Client', {...client,synchronized:true},true))
                            dispatch(setClientsInfo(getNotDeletedClients()))
                        });
                        resolve(responseJson)
                    }else{
                        reject(responseJson)
                    }
                })
        }
    })

}

function syncCreateClients(token) {
    return new Promise((resolve,reject) => {
        if(getNewClients().length > 0) {
            return api.fetchCreateClients(token, getNewClients())
                .then((response) => response.json())
                .then((responseJson) => {
                    if (responseJson.success) {
                        RealmModels.write(() => {
                            RealmModels.delete(getNewClients())
                            responseJson.clients.map((client) => {
                                RealmModels.create('Client', {...client,synchronized:true,new:false})
                            })
                            resolve(responseJson)
                        });
                    } else {
                        ErrorModal.show(responseJson.error)
                        reject(responseJson)
                    }
                })
        }
    })
}

function syncDeleteClients(token,dispatch) {
    return new Promise((resolve,reject) => {
        if (getDeletedClientsIds().length > 0) {
            return api.fetchDeleteClients(token, getDeletedClientsIds())
                .then((response) => response.json())
                .then((responseJson) => {
                    if (responseJson.success) {
                        RealmModels.write(() => {
                            RealmModels.delete(getDeletedClients())
                            dispatch(setClientsInfo(getNotDeletedClients()))
                            resolve(responseJson)
                        })

                    } else {
                        reject(responseJson)
                    }
                })
        }
    })
}

function syncGetClients(token,dispatch) {
    return api.fetchClients(token)
        .then((response) => response.json())
        .catch(() => dispatch(setClientsInfo(getNotDeletedClients())))
        .then((responseJson) => {
            if(responseJson){
                RealmModels.write(() => {
                    responseJson.clients.forEach((client) => RealmModels.create('Client', {...client,synchronized:true},true))
                    dispatch(setClientsInfo(getNotDeletedClients()))
                });

            }
        })
}


export const getClientsInfo = () => (dispatch,getState) => {
    const state = getState(),
        token = state.user.idToken,
        {isOnlineNetwork} = state.network;
    dispatch(setClientsInfo(getNotDeletedClients()));
    if(isOnlineNetwork && token){
        if(getDeletedClients().length > 0){
            syncDeleteClients(token,dispatch)
        }
        if(getUnsyncClients().length >0){
            syncEditClients(token,dispatch);
        }
        if(getNewClients().length >0){
            syncCreateClients(token);
        }
        syncGetClients(token,dispatch)

    }
};

export const getClientInfo = client_id => (dispatch) => {
    let clients = RealmModels.objects('Client')
    clients.filter(client => {
        if(client.id === client_id){
            dispatch(setClientInfo(client))
        }
    });
};

export const editClientInfo = (client_id, client) => (dispatch,getState) => {
    const state = getState(),
        token = state.user.idToken,
        {isOnlineNetwork} = state.network;
    RealmModels.write(() => {
        RealmModels.create('Client', {
            ...client,
            id: client_id,
            synchronized:false
        },true);
        dispatch(setClientInfo(client))
        dispatch(setClientsInfo(RealmModels.objects('Client')))
    });
    if(isOnlineNetwork && token){
        syncEditClients(token,dispatch)
    }
};


export const createClient = (client) => (dispatch,getState) => {
    const state = getState(),
        token = state.user.idToken,
        {isOnlineNetwork} = state.network;
    //todo if max client > clients length => not create and show error
    RealmModels.write(() => {
        RealmModels.create('Client', {
            ...client,
            id: uuid.v4(),
            new: true
        });
    });
    dispatch(setClientsInfo(getNotDeletedClients()))
    if(isOnlineNetwork && token){
        syncCreateClients(token).then((res) => {
            dispatch(setClientsInfo(getNotDeletedClients()))
        })
    }
};

export const inviteClient = (client_id) => (dispatch,getState) => {
    const state = getState(),
        token = state.user.idToken;
    if(token){
        return api.fetchInviteClient(token,client_id)
            .then((response) => response.json())
            .then((responseJson) => {
                if(responseJson.success){
                    ErrorModal.show('Client has been invited!')
                }else{
                    ErrorModal.show('Oops something went wrong! Please try again later.')
                }
            })
    }
}

export const deleteClient = (client_id,client) => (dispatch,getState) => {
    const state = getState(),
        token = state.user.idToken,
        {isOnlineNetwork} = state.network;
    RealmModels.write(() => {
        RealmModels.create('Client', {
            ...client,
            id: client_id,
            deleted:true
        },true);
    });
    if(isOnlineNetwork && token){
        syncDeleteClients(token,dispatch)
    }
}

export const deleteManyClients = (clients_ids) => (dispatch,getState) => {
    const state = getState(),
        token = state.user.idToken,
        {isOnlineNetwork} = state.network;
    return new Promise((resolve,reject) => {
        clients_ids.forEach((client_id) => {
            RealmModels.write(() => {
                RealmModels.create('Client', {
                    id: client_id,
                    deleted:true
                },true);
                resolve()
            });
        })
        if(isOnlineNetwork && token){
            syncDeleteClients(token,dispatch)
        }
    })
    
}

const ACTION_HANDLERS ={
    [SET_CLIENTS](state,action){
        return{
            ...state,
            clients: action.clients
        }
    },
    [SET_CLIENT](state,action){
        return{
            ...state,
            client: action.client
        }
    }
};

export default function client(state = initialState, action) {
    const reduceFn = ACTION_HANDLERS[action.type];
    if (!reduceFn) return state;
    return reduceFn(state, action);
}