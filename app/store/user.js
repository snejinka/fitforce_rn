import ErrorModal from '../utils/ErrorModal';
import { auth0 } from './index';
import * as api from '../api'
const
    LOGIN = 'LOGIN',
    LOGOUT = 'LOGOUT',
    SET_USER_ID = 'SET_USER_ID',
    SET_USER_TOKENS = 'SET_USER_TOKENS',
    SET_CLIENTS = 'SET_CLIENTS',
    SET_CLIENT = 'SET_CLIENT',
    SET_EMAIL = 'SET_EMAIL',
    SET_USER_INFO = 'SET_USER_INFO',
    SET_SESSION = 'SET_SESSION';
import {AsyncStorage} from 'react-native';
import RealmModels from '../models/RealmModels';
import { Actions } from 'react-native-router-flux';
import {setNetworkStatus} from './network'
import { sessionStatus } from '../constants';
import {hardDeleteAppointments} from './appointment'



export function login() {
  return {
    type: LOGIN,
  };
}

export const setSession = (session) =>(dispatch) => {
  dispatch({
    type:SET_SESSION,
    session
  })
};

export function logout() {
  removeTokensStorage()

  RealmModels.write(() => {
    RealmModels.delete(RealmModels.objects('User'));
    RealmModels.delete(RealmModels.objects('Client'));
  });
  return {
    type: LOGOUT,
  };
}

function setUserId(id) {
  return {
    type: SET_USER_ID,
    id,
  };
}

function setUserTokens(tokens) {
  return {
    type: SET_USER_TOKENS,
    tokens,
  };
}

function setUserInfo(info) {
  let connection = info.identities[0].connection
  if(connection == 'facebook' || connection == 'google-oauth2'){
    let newInfo = {
      userMetadata: {
        firstName: info.givenName,
        lastName: info.familyName
      },
      ...info
    }
    RealmModels.write(() => {
      RealmModels.create('User', {
        id: info.identities[0].user_id,
        firstName: newInfo.userMetadata.firstName,
        lastName: newInfo.userMetadata.lastName,
        email: info.email,
      });
    });
    return {
      type: SET_USER_INFO,
      info: newInfo
    };
  }else{
    RealmModels.write(() => {
      RealmModels.create('User', {
        id: info.userId,
        firstName: info.userMetadata.firstName,
        lastName: info.userMetadata.lastName,
        email: info.email,
      });
    });
    return {
      type: SET_USER_INFO,
      info: info,
    };
  }
}

export const showAuth0Error = (response) => {
  let errMessage = response.message;
  if (errMessage === 'unknown error') {
    errMessage = response.json.error;
  }
  ErrorModal.show(errMessage);
};

const getFullUserInfo = (accessToken, idToken, refreshToken, userId) => (dispatch) => {
  auth0
    .users(idToken)
    .getUser({ id: userId })
    .then((userInfo) => {
      dispatch(setUserInfo(userInfo));
      RealmModels.write(() => {
        RealmModels.create('User', {
          id: userId,
          idToken: idToken,
          accessToken: accessToken,
          refreshToken: refreshToken
        },true);
      });
    })
    .catch(response => showAuth0Error(response));
};

const getUserInfo = (accessToken, idToken, refreshToken, callback) => (dispatch) => {
  auth0
    .auth
    .userInfo({ token: accessToken })
    .then((userInfo) => {
      dispatch(setUserId(userInfo.sub));
      dispatch(getFullUserInfo(accessToken, idToken, refreshToken, userInfo.sub));
      if (callback) {
        dispatch(callback());
      }
    })
    .catch(response => showAuth0Error(response));
};

export const resetPassword = (email) => (dispatch) => {
  
  return new Promise(() => {
    auth0
      .auth
      .resetPassword({ email: email, connection: 'Username-Password-Authentication' });
  })
};

export const signIn = (email, password) => (dispatch) => {
  return new Promise((resolve) => {
    auth0
        .auth
        .passwordRealm({ username: email, password, realm: 'Username-Password-Authentication',scope: 'openid offline_access' })
        .then((response) => {
          dispatch(setUserTokens({ idToken: response.idToken, accessToken: response.accessToken,refreshToken: response.refreshToken }));
          dispatch(getUserInfo(response.accessToken, response.idToken,response.refreshToken, login));
          setRefreshTokenStorage(response.refreshToken)
          setTokenStorage(response.idToken)
          if(response.idToken){
            resolve(response)
          }

        })
        .catch(response => showAuth0Error(response));
  })
 
};

export const refreshAuth0Token = (refreshToken) => (dispatch) => {
  return new Promise((resolve,reject) => {
    auth0
        .auth
        .refreshToken({refreshToken: refreshToken})
        .then(response => {
          dispatch(setUserTokens({ idToken: response.idToken, accessToken: response.accessToken}));
          setTokenStorage(response.idToken)
          resolve(response)
        })
        .catch(err => reject(err));
  })
  

};


export const setRefreshTokenStorage = async (refreshToken) => {
  if(!refreshToken){
    this.props.logout()
  }else {
    try {
      await AsyncStorage.setItem('refreshToken',refreshToken);
    } catch (error) {
      this.props.logout()
    }
  }

}

export const removeTokensStorage = async () => {
  try {
    await AsyncStorage.removeItem('refreshToken');
    await AsyncStorage.removeItem('token');
  } catch (error) {
    console.warn(error)
  }
}

export const setTokenStorage = async (token) => {
  if(!token){
    this.props.logout()
  }else{
    try {
      await AsyncStorage.setItem('token',token);
    } catch (error) {
      this.props.logout()
    }
  }

}

export const signUp = (userInfo) => (dispatch,getState) => {
  const state = getState(),
      {isOnlineNetwork} = state.network;
  return new Promise((resolve,reject) => {
    auth0
        .auth.createUser({
      email: userInfo.email,
      password: userInfo.password,
      metadata: {
        firstName: userInfo.firstName,
        lastName: userInfo.lastName,
      },
      connection: 'Username-Password-Authentication',
    }).then((res) => {
      resolve(res)
    }).catch(err => {
      reject(err)
    });
  })
  
};

export const socialLogin = connectionName => (dispatch) => {
  auth0
    .webAuth
    .authorize({ connection: connectionName, scope: 'openid email offline_access' })
    .then((response) => {
      dispatch(setUserTokens({ idToken: response.idToken, accessToken: response.accessToken,refreshToken: response.refreshToken }));
      dispatch(getUserInfo(response.accessToken, response.idToken,response.refreshToken, login));
      setRefreshTokenStorage(response.refreshToken)
      setTokenStorage(response.idToken)
    })
    .catch(response => showAuth0Error(response));
};

export const changeAuth0UserData= data => (dispatch,getState) => {
  const state = getState(),
        userId = state.user.id,
      {isOnlineNetwork} = state.network;
  if(isOnlineNetwork){
    return api.fetchNonInteractiveToken()
        .then((response) => response.json())
        .then((responseJson) => {
          api.fetchChangeAuth0UserById(responseJson.access_token,userId,data)
              .then((response) => response.json())
              .then((responseJson) => {
                if(responseJson.statusCode == 200){
                  if(data.password){
                    ErrorModal.show('Your password was changed successfully.');
                  }else{
                    RealmModels.write(() => {
                      RealmModels.create('User', {
                        id: responseJson.user_id,
                        data
                      },true);
                    });
                    dispatch({
                      type: SET_EMAIL,
                      email: data.email
                    })
                    ErrorModal.show('Your data was changed successfully.');
                  }
                }else{
                  showAuth0Error(responseJson)
                }
              })
        })
  }
}


const initialState = {
  idToken: '',
  accessToken: '',
  refreshToken: '',
  id: '',
  email: '',
  name: '',
  firstName: '',
  lastName: '',
  phone: '',
  address: '',
  services: '',
  lengthUnit: '',
  weightUnit: '',
  currency: '',
  taxRate: '',
  session: sessionStatus.guest,
};

const ACTION_HANDLERS = {
  [LOGIN](state) {
    return { ...state, session: sessionStatus.authorized };
  },
  [LOGOUT](state) {
    return { ...state,
      session: sessionStatus.guest,
      idToken: '',
      accessToken: '',
      refreshToken: '',
      id: '',
      email: '',
      name: '',
      firstName: '',
      lastName: '',
      phone: '',
      address: '',
      services: '',
      lengthUnit: '',
      weightUnit: '',
      currency: '',
      taxRate: '',
      clients:[],
    };
  },
  [SET_USER_ID](state, action) {
    return { ...state, id: action.id };
  },
  [SET_USER_TOKENS](state, action) {
    return { ...state, accessToken: action.tokens.accessToken, idToken: action.tokens.idToken,refreshToken: action.tokens.refreshToken };
  },
  [SET_USER_INFO](state, action) {
    const { info } = action;
    const { userMetadata } = info;
    return { ...state,
      email: info.email,
      name: info.name,
      firstName: userMetadata.firstName,
      lastName: userMetadata.lastName,
      phone: userMetadata.phone,
      address: userMetadata.address,
      services: userMetadata.services,
      lengthUnit: userMetadata.lengthUnit,
      weightUnit: userMetadata.weightUnit,
      currency: userMetadata.currency,
      taxRate: userMetadata.taxRate,
    };
  },
  [SET_EMAIL](state,action){
    return{
        ...state,
      email:action.email
    }
  },
  [SET_SESSION](state,action){
    return{
      ...state,
      session:action.session
    }
  }
};

export default function user(state = initialState, action) {
  const reduceFn = ACTION_HANDLERS[action.type];
  if (!reduceFn) return state;
  return reduceFn(state, action);
}
