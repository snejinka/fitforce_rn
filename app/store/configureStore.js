import { createStore, applyMiddleware, compose } from 'redux';
import { persistStore, autoRehydrate } from 'redux-persist';
import { AsyncStorage } from 'react-native';
import thunk from 'redux-thunk';
import rootReducer from '../store/index';

export default function configureStore(initialState) {
  const store = createStore(rootReducer, initialState, compose(
      applyMiddleware(thunk),
      autoRehydrate(),
    ),
  );

  persistStore(store, { whitelist: ['user'], storage: AsyncStorage });

  return store;
}
