import * as api from '../api'
import RealmModels from '../models/RealmModels';
import Realm from 'realm';
import uuid from 'uuid';
import ErrorModal from '../utils/ErrorModal';

const GET_LIST_INVOICES = 'GET_LIST_INVOICES';

const initialState = {
    invoices:[]
}


export const getListInvoices = () => (dispatch,getState) => {
    const state = getState(),
        token = state.user.idToken,
        {isOnlineNetwork} = state.network;
    if(isOnlineNetwork && token){
        return api.fetchGetListInvoices(token)
            .then((response) => response.json())
            .then((responseJson) => {
                dispatch({
                    type:GET_LIST_INVOICES,
                    data:{...responseJson}
                })
            })

    }
};

export const getClientListInvoices = (client_id) => (dispatch,getState) => {
    const state = getState(),
        token = state.user.idToken,
        {isOnlineNetwork} = state.network;
    if(isOnlineNetwork && token){
        return api.fetchGetClientListInvoices(token,client_id)
            .then((response) => response.json())
            .then((responseJson) => {
                dispatch({
                    type:GET_LIST_INVOICES,
                    data:{...responseJson}
                })
            })
    }
};

export const createInvoice = (data) => (dispatch,getState) => {
    const state = getState(),
        token = state.user.idToken,
        {isOnlineNetwork} = state.network;
    if(isOnlineNetwork && token){
        return api.fetchCreateInvoice(token,data)
            .then((response) => response.json())
            .then((responseJson) => {
                // dispatch({
                //     type:GET_LIST_INVOICES,
                //     data:{...responseJson}
                // })
            })
    }
};



const ACTION_HANDLERS = {
    [GET_LIST_INVOICES]:(state,action) => ({
        ...state,
        ...action.data
    })
}


export default function invoice(state = initialState, action) {
    const reduceFn = ACTION_HANDLERS[action.type];
    if (!reduceFn) return state;
    return reduceFn(state, action);
}