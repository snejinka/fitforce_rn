import * as api from '../api'
import ErrorModal from '../utils/ErrorModal';
import RealmModels from '../models/RealmModels';
const SET_FIELD = 'SET_FIELD',
      SET_TRAINER_INFO = 'SET_TRAINER_INFO',
      SET_SUBSCRIPTION_PLAN = 'SET_SUBSCRIPTION_PLAN'

function setTrainerInfo(trainer) {
  return{
    type:SET_TRAINER_INFO,
    trainer
  }
}

function getUser() {
  return RealmModels.objects('User')[0];
}

export const editTrainerInfo = (info) => (dispatch,getState) =>{
  const state = getState(),
      token = state.user.idToken,
      userId = state.user.id,
      {isOnlineNetwork} = state.network;
  RealmModels.write(() => {
    RealmModels.create('User', {
      id: userId,
      ...info
    },true);
  });
  dispatch(setTrainerInfo(getUser()))
  if(isOnlineNetwork){
    return api.fetchEditTrainer(token,info)
        .then((response) => response.json())
        .then((responseJson) => {
          dispatch(setTrainerInfo(responseJson.trainer))
        })
  }
}

export const getTrainerInfo = () => (dispatch,getState) =>{
  const state = getState(),
      token = state.user.idToken,
      userId = state.user.id,
      {isOnlineNetwork} = state.network;
  dispatch(setTrainerInfo(getUser()))
  return new Promise((resolve,reject) => {
    if(isOnlineNetwork){
      return api.fetchGetTrainer(token)
          .then((response) => response.json())
          .then((responseJson) => {
            RealmModels.write(() => {
              RealmModels.create('User', {
                id: userId,
                ...responseJson.trainer
              },true);
            });
            dispatch(setTrainerInfo(responseJson.trainer))
            resolve()
          })
    }
  })
}

export const createTrainer = (trainer) => (dispatch,getState) =>{
  return new Promise((resolve,reject) => {
    const state = getState(),
        token = state.user.idToken,
        {isOnlineNetwork} = state.network;
    if(isOnlineNetwork){
      return api.fetchCreateTrainer(token,trainer)
          .then((response) => response.json())
          .then((responseJson) => {
            RealmModels.write(() => {
              RealmModels.create('User', {
                id: responseJson.trainer.id,
                ...responseJson.trainer
              },true);
            });
            resolve()
            dispatch(setTrainerInfo(responseJson.trainer))
          })
          .catch((err) => reject(err))
    }else {
      ErrorModal.show('Please check Internet connection.')
    }
  })

}

export const updateSubscription = (stripeToken,plan,max_clients_count) => (dispatch,getState) => {
  const state = getState(),
      userId = state.user.id,
      token = state.user.idToken,
      {isOnlineNetwork} = state.network;
  return new Promise((resolve,reject) => {
    if(isOnlineNetwork){
      let data = stripeToken ? {token:stripeToken,plan} : {plan}
      return api.fetchUpdateTrainerSubscription(token,data)
          .then((response) => response.json())
          .then((responseJson) => {
            if(responseJson.success){
              RealmModels.write(() => {
                RealmModels.create('User', {
                  id: userId,
                  max_clients_count,
                  ...responseJson.trainer
                },true);
              });
              dispatch({
                type:SET_SUBSCRIPTION_PLAN,
                max_clients_count,
                subscription_plan:responseJson.trainer.subscription_plan
              })
              ErrorModal.show('Successfully.')
              resolve()
            }else {
              ErrorModal.show(responseJson)
              reject()
            }
          })
    }else {
      ErrorModal.show('Please check Internet connection.')
      reject()
    }
  })

}

export const setField = (fieldName, value) => (dispatch) => {
  dispatch({
    type: SET_FIELD,
    fieldName,
    value,
  })
};

const actionsMap = {
  [SET_FIELD](state, action) {
    return { 
      ...state, 
      [action.fieldName]: action.value 
    };
  },
  [SET_TRAINER_INFO](state, action) {
    return {
      ...state,
      ...action.trainer
    };
  },
  [SET_SUBSCRIPTION_PLAN](state,action){
    return{
      ...state,
      subscription_plan:action.subscription_plan,
      max_clients_count:action.max_clients_count
    }
  }
};

const initialState = {
  phone: '',
  address: '',
  services: [],
  lengthUnit: 'inch',
  weightUnit: 'lbs',
  currency: 'USD',
  taxRate: '',
  aboutMe: '',
  tempEmail:'',
  tempPassword:'',
  subscription_plan:'',
  stripe_customer_id:'',
  max_clients_count:3,
  success_last_payment:false
};

export default function onboarding(state = initialState, action) {
  const reduceFn = actionsMap[action.type];
  if (!reduceFn) return state;
  return reduceFn(state, action);
}
