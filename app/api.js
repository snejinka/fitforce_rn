export const API_URL = 'https://94hmu2r9pc.execute-api.us-west-2.amazonaws.com/v1',
             CLIENTS_PATH = API_URL + '/clients',
             APPOINTMENTS_PATH = API_URL + '/appointments',
             TRAINER_PATH = API_URL + '/trainer',
             AUTH0_URL = 'https://shatalovbs.eu.auth0.com',
             AUTH0_TOKEN_URL = '/oauth/token',
             AUTH0_USERS_URL = '/api/v2/users',
             SUBSCRIPTIONS_PATH = TRAINER_PATH + '/subscriptions',
             INVOICES_PATH = API_URL + '/invoices';

             FITNESS_PROGRAMS_PATH = API_URL + '/fitness-programs';

export function fetchClients(token) {
    return fetch(CLIENTS_PATH, {
        method:'GET',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': 'Bearer '  + token
    },
    })
}

export function fetchClientInfo(token, client_id) {
    return fetch(CLIENTS_PATH + '/' + client_id,{
        method:'GET',
        headers:{
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': 'Bearer '  + token
        }
    })
    
}

export function fetchEditClients(token, clients) {
    return fetch(CLIENTS_PATH,{
        method:'PUT',
        headers:{
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': 'Bearer '  + token
        },
        body: JSON.stringify({
            clients: [
                ...clients
            ]
        })
    })

}

export function fetchCreateClients(token, clients) {
    return fetch(CLIENTS_PATH,{
        method:'POST',
        headers:{
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': 'Bearer '  + token
        },
        body:JSON.stringify({
            clients: [
                ...clients
            ]
        })
    })

}

export function fetchDeleteClients(token,clients){
    return fetch(CLIENTS_PATH, {
        method:'DELETE',
        headers:{
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': 'Bearer '  + token
        },
        body:JSON.stringify({
            clients:[
                ...clients
            ]
        })
    })
}

export function fetchInviteClient(token,client_id){
    return fetch(CLIENTS_PATH + '/' + client_id + '/invite', {
        method:'POST',
        headers:{
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': 'Bearer '  + token
        }
    })
}

export function fetchAppointments(token) {
    return fetch(APPOINTMENTS_PATH, {
        method:'GET',
        headers:{
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': 'Bearer '  + token
        }
    })
}

export function fetchClientsAppointments(token,client_id) {
    return fetch(APPOINTMENTS_PATH +`/?client_id=${client_id}`, {
        method:'GET',
        headers:{
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': 'Bearer '  + token
        }
    })
}

export function fetchCreateAppointments(token,appointments) {
    return fetch(APPOINTMENTS_PATH, {
        method:'POST',
        headers:{
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': 'Bearer '  + token
        },
        body:JSON.stringify({
            appointments:[
                ...appointments
            ]
        })
    })
}

export function fetchEditSingleAppointments(token,appointments) {
    return fetch(APPOINTMENTS_PATH, {
        method:'PUT',
        headers:{
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': 'Bearer '  + token
        },
        body:JSON.stringify({
            appointments
        })
    })
}

export function fetchDeleteAppointments(token,appointments) {
    return fetch(APPOINTMENTS_PATH, {
        method:'DELETE',
        headers:{
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': 'Bearer '  + token
        },
        body:JSON.stringify({
            appointments
        })
    })
}

export function fetchCreateTrainer(token,trainer) {
    return fetch(TRAINER_PATH, {
        method:'POST',
        headers:{
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': 'Bearer '  + token
        },
        body:JSON.stringify({
            trainer
        })
    })
}

export function fetchEditTrainer(token,trainer) {
    return fetch(TRAINER_PATH, {
        method:'PUT',
        headers:{
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': 'Bearer '  + token
        },
        body:JSON.stringify({
            trainer
        })
    })
}

export function fetchGetTrainer(token) {
    return fetch(TRAINER_PATH, {
        method:'GET',
        headers:{
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': 'Bearer '  + token
        }
    })
}

export function fetchNonInteractiveToken() {
    return fetch(AUTH0_URL + AUTH0_TOKEN_URL, {
        method:'POST',
        headers:{
            'Content-Type': 'application/json',
        },
        body:JSON.stringify({
            client_id: 'krEPUzBfH9awFqVoqGCK3zOMUdoNJcEV',
            client_secret: 'a4L_xTGsiXiu5ruLWPdePMPgB_4j02d-m_BGZV_H4ByPkQ0WEwxcvAc78jicYuiV',
            audience: 'https://shatalovbs.eu.auth0.com/api/v2/',
            grant_type: 'client_credentials'
        })
    })
}


export function fetchChangeAuth0UserById(token,id,data) {
    return fetch(AUTH0_URL + AUTH0_USERS_URL + `/${id}`, {
        method:'PATCH',
        headers:{
            'Content-Type': 'application/json',
            'Authorization': 'Bearer '  + token
        },
        body:JSON.stringify({
            ...data
        })
    })
}

export function fetchUpdateTrainerSubscription(token,data) {
    return fetch(SUBSCRIPTIONS_PATH, {
        method:'PUT',
        headers:{
            'Content-Type': 'application/json',
            'Authorization': 'Bearer '  + token
        },
        body:JSON.stringify({
            ...data
        })
    })
}

export function fetchGetListInvoices(token) {
    return fetch(INVOICES_PATH, {
        method:'GET',
        headers:{
            'Content-Type': 'application/json',
            'Authorization': 'Bearer '  + token
        }
    })
}

export function fetchGetClientListInvoices(token, client_id) {
    return fetch(INVOICES_PATH + `/?client_id=${client_id}`, {
        method:'GET',
        headers:{
            'Content-Type': 'application/json',
            'Authorization': 'Bearer '  + token
        }
    })
}

export function fetchCreateInvoice(token, data) {
    return fetch(INVOICES_PATH, {
        method:'POST',
        headers:{
            'Content-Type': 'application/json',
            'Authorization': 'Bearer '  + token
        },
        body:JSON.stringify({
            ...data
        })
    })
}
// FITNESS PROGRAMS API
export function fetchFitnessPrograms(token) {
  return fetch(FITNESS_PROGRAMS_PATH, {
      method:'GET',
      headers:{
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': 'Bearer '  + token
      }
  })
}

export function createFitnessProgram(token,program) {
  return fetch(FITNESS_PROGRAMS_PATH, {
      method:'POST',
      headers:{
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': 'Bearer '  + token
      },
      body:JSON.stringify({
          "fitness_program": program
      })
  })
}

export function updateFitnessProgram(token,program) {
  return fetch(FITNESS_PROGRAMS_PATH, {
      method:'PUT',
      headers:{
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': 'Bearer '  + token
      },
      body:JSON.stringify({
          "fitness_program": program
      })
  })
}

export function deleteFitnessProgram(token, programId) {
  return fetch(FITNESS_PROGRAMS_PATH, {
      method:'DELETE',
      headers:{
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': 'Bearer '  + token
      },
      body:JSON.stringify({
          "fitness_program": programId
      })
  })
}