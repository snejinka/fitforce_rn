import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
  Content,
  Text,
  Button,
  Tabs,
  Tab,
  TabHeading,
  Container
} from 'native-base';
import { Actions } from 'react-native-router-flux';
import { Image, View, ScrollView } from 'react-native';
import styles from '../assets/styles/index';
import ImagePlan from '../assets/images/imgPlan.png';
import ImagePlan2 from '../assets/images/imgPlan2.png';
import stripe from 'tipsi-stripe';
import {subscriptionPlans} from '../constants';
import {updateSubscription,getTrainerInfo} from '../store/onboarding';
import Header from '../components/Header';
import ModalConfirm from '../components/ModalConfirm';


class Subscription extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedPlanId:'',
      selectedPlan:'',
      stripeToken:'',
      visibleSpinner:false,
      modalVisible:false,
      modalChooseCardVisible:false,
      maxClientsCountOnSelectedPlan:0
    }
    this.paymentData = {}
  }

  componentWillMount(){
    stripe.init({
      publishableKey: 'pk_test_ZCU4zPEbkl18CQQ65uNKMglB'
    })
  }

  handleUpgrade(plan){
    this.setState({
      selectedPlan:plan
    })
    if(this.props.clients.length > plan.max_clients_count){
      this.setState({
        maxClientsCountOnSelectedPlan:plan.max_clients_count,
        modalVisible:true
      })
    }else if(this.props.stripe_customer){
      //todo change stripe_customer to another field
      this.setState({
        modalChooseCardVisible:true
      })
    }else {
      this.stripeRequest(plan)

    }
  }

  stripeRequest(plan){
    const options = {
      smsAutofillDisabled: true
    };
    stripe.paymentRequestWithCardForm(options)
        .then(response => {
          if(response.tokenId){
            this.setState({
              stripeToken:response.tokenId,
              modalChooseCardVisible:false
            })
            this.props.updateSubscription(this.state.stripeToken,plan.name,plan.max_clients_count)
            this.props.getTrainerInfo()
          }
        })
        .catch(err => {
          this.setState({
            modalChooseCardVisible:false
          })
        })

  }

  renderCost(plan){
    if(plan.cost == 0){
      return plan.title.toUpperCase();
    }else {
      return `$${plan.cost}.00 USD`;
    }
  }


  render() {
    const {subscription_plan} = this.props;
    const {selectedPlan} = this.state;
    return (
      <Container>
        <Header title="My subscription" buttonTitle=" "/>
        <ModalConfirm
            isVisible={this.state.modalVisible}
            firstLineText={`You have ${this.props.clients.length - this.state.maxClientsCountOnSelectedPlan} excess clients. Please remove excess clients to change the plan`}
            secondLineText="to select this plan."
            buttonNoText="Cancel"
            buttonYesText="Clients List"
            handleNo={() => this.setState({modalVisible:false})}
            handleYes={() => {this.setState({modalVisible:false});Actions.deleteClients()}}
            handleClose={() => this.setState({modalVisible:false})}
        />
        <ModalConfirm
            isVisible={this.state.modalChooseCardVisible}
            firstLineText="Want to use a previous card or enter a new one?"
            buttonNoText="Previous"
            buttonYesText="New"
            handleNo={() => {this.setState({modalChooseCardVisible:false});this.props.updateSubscription(this.state.stripeToken,selectedPlan.name,selectedPlan.max_clients_count)}}
            handleYes={() => { this.stripeRequest(selectedPlan)}}
            handleClose={() => this.setState({modalChooseCardVisible:false})}
        />
        <ScrollView >
          <Text style={styles.tabsTitle}>Choose the right plan for you</Text>
          <Tabs style={styles.tabs} initialPage={1} tabBarUnderlineStyle={{ backgroundColor: 'transparent' }}>
            {subscriptionPlans.map((plan) => {
              return(
                  <Tab key={plan.id} heading={<TabHeading style={styles.tabHeading}><View style={styles.tabDot} /></TabHeading>}>
                    <Content padder style={styles.tabContent}>
                      <View style={styles.planBox}>
                        <View style={styles.planBoxHeader}>
                          <Text style={styles.planBoxHeaderTitle}>{this.renderCost(plan)}</Text>
                        </View>
                        <View style={styles.planBoxBody}>
                          <Image style={styles.planBoxBodyImage} source={ImagePlan} />
                          <View style={styles.planBoxBodyImageShadow} />
                          <Text style={styles.planBoxBodyTitle}>{plan.title.toUpperCase()}</Text>
                          <Text style={{ ...styles.planBoxBodyTitle, ...styles.planBoxBodySubTitle }}>Up to {plan.max_clients_count} Clients</Text>
                        </View>
                      </View>
                      {subscription_plan == plan.name
                          ?
                          <Button style={{ ...styles.btnPrimary, ...styles.btnDelete }} block>
                            <Text style={styles.btnPrimaryText}>{'Current Plan'.toUpperCase()}</Text>
                          </Button>
                          :
                          <Button
                              style={{ ...styles.btnPrimary }}
                              block onPress={() => this.handleUpgrade(plan)}>
                            <Text style={styles.btnPrimaryText}>{'Select plan'.toUpperCase()}</Text>
                          </Button>
                      }
                    </Content>
                  </Tab>
              )
            })}
          </Tabs>
        </ScrollView>
      </Container>

    );
  }
}



const mapDispatchToProps = dispatch => bindActionCreators({
  updateSubscription,
  getTrainerInfo
}, dispatch);

const mapStateToProps = state => ({
  onboarding: state.onboarding,
  subscription_plan: state.onboarding.subscription_plan,
  max_clients_count: state.onboarding.max_clients_count,
  stripe_customer: state.onboarding.stripe_customer,
  clients: state.client.clients,
});

export default connect(mapStateToProps, mapDispatchToProps)(Subscription);
