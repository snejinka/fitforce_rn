import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
  Container,
  Content,
  Text,
  Form,
  Item,
  Label,
  Icon,
  ListItem,
  Body,
  Button,
  Input,
  Separator
} from 'native-base';
import { View, ScrollView, Image } from 'react-native';
import styles from '../../assets/styles/index';
import Header from '../../components/Header';
import Avatar from '../../assets/images/appointmentAvatar1.png';
import IconMaterial from 'react-native-vector-icons/MaterialCommunityIcons';
import moment from 'moment';
import DatePicker from 'react-native-datepicker';
import uuid from 'uuid';
import CheckboxItem from '../../components/CheckboxItem';
import {createInvoice} from '../../store/invoice'


class CreateInvoice extends Component {
  constructor(props){
    super(props);
    this.state = {
      date:moment().format('MMM DD, YYYY'),
      dueDate:moment().format('MMM DD, YYYY'),
      memo:'',
      initial:true,
      selectedAppointmentsIds:[]
    }
    this.newInvoiceData = {}
    this.selectedAppointmentsIds = []
  }

  handleSaveInvoice(){
    this.props.createInvoice({invoices:[this.newInvoiceData]})
  }

  onPressDateCustom(date){
    this.setState({date:date})
  }

  onPressDueDateCustom(date){
    this.setState({dueDate:date})
  }

  convertDateToTimestamp(date){
    return moment(date).format('x')
  }

  renderCost(cost){
    if(cost){
      return cost/100
    }else {
      return 0
    }
  }

  humanizeDate(date){
    return moment(date).format('MMM DD, YYYY')
  }

  handleClickCheckbox(appointment_id){
    const appointmentIdIndex = this.selectedAppointmentsIds.indexOf(appointment_id);
    if (appointmentIdIndex > -1) {
      this.selectedAppointmentsIds.splice(appointmentIdIndex, 1)
    } else {
      this.selectedAppointmentsIds.push(appointment_id)
    }
    this.setState({
      selectedAppointmentsIds:this.selectedAppointmentsIds
    })
  }

  render() {
    const {client,appointments} = this.props;
    const {memo,date,dueDate} = this.state;
    this.newInvoiceData = {
      id:uuid.v4(),
      timestamp_created:this.convertDateToTimestamp(date),
      timestamp_due:this.convertDateToTimestamp(dueDate),
      client_id:client.id,
      memo,
      appointments:this.selectedAppointmentsIds
    };
    return (
      <Container>
        <Header title="Create New Invoice" rightButtonCallback={() => this.handleSaveInvoice()}/>
        <Content>
          <ScrollView style={{ ...styles.scrollviewNopadding }}>
            <Form style={styles.form}>
              <Item style={{ ...styles.inlineInput }} stackedLabel>
                <Label style={styles.label}>{'Client Name'.toUpperCase()}</Label>
                <View style={styles.inlineInputRow}>
                  <Image source={Avatar} style={styles.inlineInputImage}/>
                  <Input
                      style={styles.input}
                      value={`${client.first_name} ${client.last_name}`}
                      editable={false}/>
                </View>
              </Item>
              <View style={styles.formRow}>
                <Item style={{ ...styles.inlineInput, ...styles.inlineInputMargin }} stackedLabel>
                  <Label style={styles.label}>{'Date'.toUpperCase()}</Label>
                  <Input
                      style={styles.input}
                      value={date}
                      name="Date"
                      onFocus={() => this.datepicker.onPressDate()}
                  />
                  <DatePicker
                      style={{width: 0,height:0}}
                      date={date}
                      mode="date"
                      format="MMM DD,YYYY"
                      confirmBtnText="Confirm"
                      cancelBtnText="Cancel"
                      hideText={true}
                      showIcon={false}
                      onDateChange={(date) => this.onPressDateCustom(date)}
                      ref={(d) => { this.datepicker = d }}
                  />
                </Item>
                <Item stackedLabel style={styles.inlineInput}>
                  <Label style={styles.label}>{'Due Date'.toUpperCase()}</Label>
                  <Input
                      style={styles.input}
                      value={dueDate}
                      name="Date"
                      onFocus={() => this.datepickerDue.onPressDate()}
                  />
                  <DatePicker
                      style={{width: 0,height:0}}
                      date={dueDate}
                      mode="date"
                      format="MMM DD,YYYY"
                      confirmBtnText="Confirm"
                      cancelBtnText="Cancel"
                      hideText={true}
                      showIcon={false}
                      onDateChange={(date) => this.onPressDueDateCustom(date)}
                      ref={(d) => { this.datepickerDue = d }}
                  />
                </Item>
              </View>
              <Item style={{ ...styles.inlineInput }} stackedLabel>
                <Label style={styles.label}>{'Memo'.toUpperCase()}</Label>
                <Input
                    style={{...styles.input,...styles.inputMultiline}}
                    multiline={true}
                    value={memo}
                    onChangeText={(memo) => this.setState({memo})}
                />
              </Item>
              <ListItem style={{...styles.noBorderBottom, ...styles.noMarginLeft}}>
                <IconMaterial style={{...styles.checkboxIcon, ...styles.checkboxIconInactive}} name="circle-outline" />
                <Body>
                <Text style={{...styles.checkboxText, ...styles.greyText }}>Custom Invoice</Text>
                </Body>
              </ListItem>
              <Separator bordered style={{ ...styles.separator, ...styles.separatorInForm, ...styles.noMarginTop }}>
                <Text>{'Unpaid Appointments'.toUpperCase()}</Text>
              </Separator>
              <View style={styles.invoiceTotalContainer}>
                <View style={{...styles.invoiceItemBodyRow,...styles.inlineInput, ...styles.invoiceItemBodyRowMargin}}>
                  <IconMaterial style={{...styles.checkboxIcon}} name="check-circle" />
                  <Text style={{...styles.invoiceItemText, ...styles.invoiceItemTextSelectAll}}>Select All</Text>
                </View>
                {appointments.map((item) => {
                  if(!item.invoice_id){
                    return(
                        <View
                            key={item.id}
                            style={{...styles.invoiceItemBodyRow,...styles.inlineInput, ...styles.invoiceItemBodyRowMargin}}>
                          <CheckboxItem
                              checked={this.selectedAppointmentsIds.includes(item.id)}
                              handleClick={() => this.handleClickCheckbox(item.id)}
                              listItemStyle={[styles.noBorderBottom, styles.noMarginLeft,{position:'absolute',top:-10,left:0}]}/>
                          <View style={[styles.invoiceItemTextRow,{marginLeft:32}]}>
                            <Text style={styles.invoiceItemText}>{`${this.humanizeDate(item.date)}, 10:30 - 11:30 (1 h)`}</Text>
                            <Text style={{...styles.invoiceItemText, ...styles.textBold}}>5 fitness training sessions</Text>
                          </View>
                          <Text
                              style={{...styles.invoiceItemText,...styles.invoiceItemTextRight, ...styles.invoiceItemTextRightBottom, ...styles.textBold}}>${this.renderCost(item.amount_in_cents)}</Text>
                        </View>
                    )
                  }
                })}
                <View style={styles.invoiceItemBodyRow}>
                  <Text style={{...styles.invoiceItemText, ...styles.invoiceItemTextTotal}}>Total:</Text>
                  <Text style={{...styles.invoiceItemText,...styles.invoiceItemTextRight}}>$100.00</Text>
                </View>
                <View style={styles.invoiceItemBodyRow}>
                  <Text style={{...styles.invoiceItemText, ...styles.invoiceItemTextTotal}}>Tax (7%):</Text>
                  <Text style={{...styles.invoiceItemText,...styles.invoiceItemTextRight}}>$100.00</Text>
                </View>
                <View style={styles.invoiceItemBodyRow}>
                  <Text style={{...styles.invoiceItemText, ...styles.invoiceItemTextTotal, ...styles.textBold}}>Total (tax incl.):</Text>
                  <Text style={{...styles.invoiceItemText,...styles.invoiceItemTextRight, ...styles.textBold}}>$100.00</Text>
                </View>
              </View>
            </Form>
          </ScrollView>
        </Content>
      </Container>
    );
  }
}
const mapDispatchToProps = dispatch => bindActionCreators({
  createInvoice
}, dispatch);

const mapStateToProps = state => ({
});

export default connect(mapStateToProps, mapDispatchToProps)(CreateInvoice);
