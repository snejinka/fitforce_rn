import React, { Component } from 'react';
import {
  Container,
  Content,
  Form,
  Item,
  Label,
  Icon,
  Input,
} from 'native-base';
import { View, ScrollView } from 'react-native';
import { Dropdown } from 'react-native-material-dropdown';
import styles from '../../assets/styles/index';
import Header from '../../components/Header';

import { Actions } from 'react-native-router-flux';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { createFitnessProgram, editFitnessProgram } from '../../store/program';

import {fitnessProgramTypes,fitnessProgramLevels} from '../../constants';

class AddFitnessProgram extends Component {
  constructor(props) {
    super(props);
    this.state = {};
    this.newFitnessProgram = {};
  }

  componentWillMount() {
    this.setState({
      program_type: fitnessProgramTypes[0],
      program_level: fitnessProgramLevels[0]
    })
  }
  
  componentDidMount() {
    let { edit, program } = this.props;
    if (edit) {
      this.setState({
        name: program.name,
        program_type: fitnessProgramTypes[program.program_type],
        program_level: fitnessProgramLevels[program.program_level],
        description: program.description,
        exersices: program.exersices
      })
    }
  }

  handleSave = () => {
    const { program_type, program_level, description, name } = this.state;
    let { edit, program } = this.props;

    if (!edit) {
      if (name) {
        this.props.createFitnessProgram(this.newFitnessProgram).then((res) => {
          Actions.pop();
        });
      }
    } else {
      this.props.editFitnessProgram({...this.newFitnessProgram, id: program.id}).then((res) => {
        Actions.pop();
      });
    }
  }

  render() {
    const { programs } = this.props;
    const { program_type, program_level, description, name } = this.state;

    this.newFitnessProgram = {
      name,
      program_type: fitnessProgramTypes.filter((item) => item.value === program_type.value)[0].id,
      program_level: fitnessProgramLevels.filter((item) => item.value === program_level.value)[0].id,
      description: description ? description : '',
      exersices: []
    }

    return (
        <Container>
          <Header title={this.props.headerTitle || "Create New Program"} rightButtonCallback={this.handleSave}/>
          <Content>
            <ScrollView style={{ ...styles.scrollviewNopadding }}>
              <Form style={[styles.form,{marginBottom:150}]}>

                <Item style={{ ...styles.inlineInput }} stackedLabel>
                  <Label style={styles.label}>{'Fitness Program Name'.toUpperCase()}</Label>
                  <View style={styles.inlineInputRow}>
                    <Input
                      style={styles.input}
                      value={name}
                      onChangeText={(name) => this.setState({name})}
                      editable={true}
                    />
                  </View>
                </Item>
                <Item
                    style={[styles.inlineInput]}
                    stackedLabel>
                  <Input style={styles.label} value={'Difficulty Level'.toUpperCase()} />
                  <View style={styles.stackedInputIconContainer}>
                    <Dropdown
                        label=" "
                        fontSize={17}
                        textColor={'rgb(102, 120, 144)'}
                        value={program_level.value}
                        onChangeText={(program_level) => this.setState({program_level: {value: program_level}})}
                        inputContainerStyle={[{borderBottomColor: 'transparent'}]}
                        pickerStyle={{backgroundColor : "#fff",borderWidth:0}}
                        containerStyle={[styles.dropdown,{width:'auto',flex:1,borderWidth:0,paddingBottom:15}]}
                        data={fitnessProgramLevels}
                        renderAccessory={() => {return <Icon name="ios-arrow-down" style={{ ...styles.stackedInputIcon, ...styles.selectIcon,...styles.selectIconBlue }} />}}
                    />
                  </View>
                </Item>
                <Item
                    style={[styles.inlineInput]}
                    stackedLabel>
                  <Input style={styles.label} value={'Program Type'.toUpperCase()} />
                  <View style={styles.stackedInputIconContainer}>
                    <Dropdown
                        label=" "
                        fontSize={17}
                        textColor={'rgb(102, 120, 144)'}
                        value={program_type.value}
                        onChangeText={(program_type) => this.setState({program_type: {value: program_type}})}
                        inputContainerStyle={[{borderBottomColor: 'transparent'}]}
                        pickerStyle={{backgroundColor : "#fff",borderWidth:0}}
                        containerStyle={[styles.dropdown,{width:'auto',flex:1,borderWidth:0,paddingBottom:15}]}
                        data={fitnessProgramTypes}
                        renderAccessory={() => {return <Icon name="ios-arrow-down" style={{ ...styles.stackedInputIcon, ...styles.selectIcon,...styles.selectIconBlue }} />}}
                    />
                  </View>
                </Item>
                <Item style={{ ...styles.inlineInput }} stackedLabel>
                  <Label style={styles.label}>{'Fitness Program Description'.toUpperCase()}</Label>
                  <Input
                      style={{...styles.input, height: 140, paddingTop: 20}}
                      value={description}
                      multiline={true}
                      onChangeText={(description) => this.setState({description})}/>
                </Item>
              </Form>
            </ScrollView>
          </Content>
        </Container>
    );
  }
}

const mapDispatchToProps = dispatch => bindActionCreators({
  createFitnessProgram,
  editFitnessProgram
}, dispatch);

const mapStateToProps = state => ({
  programs: state.program.fitness_programs
});

export default connect(mapStateToProps, mapDispatchToProps)(AddFitnessProgram);