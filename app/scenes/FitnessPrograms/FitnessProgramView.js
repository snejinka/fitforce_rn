import React, { Component } from 'react';
import {
  Container,
  Content,
  Form,
  Item,
  Label,
  Icon,
  Input,
  Separator,
  Button
} from 'native-base';
import { View, Text, ScrollView, TouchableOpacity } from 'react-native';
import styles from '../../assets/styles/index';
import Header from '../../components/Header';

import { Actions } from 'react-native-router-flux';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { deleteFitnessProgram, deleteExercise } from '../../store/program';

import ExercisesList from './ExercisesList';
import {fitnessProgramLevels, fitnessProgramTypes} from '../../constants';
import Modal from 'react-native-simple-modal';

class FitnessProgramView extends Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
      active: null,
      coordinates: 90
    }
  }

  handleDeleteProgram = () => {
    const { program } = this.props;
    this.props.deleteFitnessProgram(program).then(() => {
      Actions.pop();
    });
  }

  handleEditProgram = () => {
    const { program } = this.props;
    Actions.addFitnessProgram({edit: true, program, headerTitle:'Edit programm'});
  }
  
  handleEditExercise = () => {
    const { program } = this.props,
          { active } = this.state;
    Actions.addExercise({edit: true, program, exercise: active});
    this.setState({open: false});
  }
  
  handleAddExercise = () => {
    const { program } = this.props;
    Actions.addExercise({program});
  }

  handleDeleteExercise = () => {
    const { program } = this.props;
    let { active } = this.state,
        updated = JSON.parse(program.exercises).filter((ex) => ex.id !== active.id);
    this.props.deleteExercise({...program,exercises: updated});
    this.setState({open: false});
  }

  openModal = (coordinates, open, active) => {
    this.setState({coordinates, open, active});
  }

  render() {
    const { program } = this.props;

    return (    
        <Container>
          <Header title={program.name} rightButtonCallback={this.handleEditProgram} buttonTitle={'Edit'}/>
          <Content>
            <ScrollView style={{ ...styles.scrollviewNopadding }}>
              <Form style={[styles.form]}>
                <Separator bordered style={{...styles.separator, ...styles.separatorInForm, ...styles.separatorNoMargin}}>
                  <Text>{'Fitness Program: '.toUpperCase() + program.name.toUpperCase()}</Text>
                </Separator>
                <Item style={{ ...styles.inlineInput, ...styles.inlineInputUnbordered, marginTop: 10 }} stackedLabel>
                  <Label style={styles.label}>{'Difficulty Level'.toUpperCase()}</Label>
                  <Text style={styles.formText}>{ fitnessProgramLevels[program.program_level].value }</Text>
                </Item>
                <Item style={{ ...styles.inlineInput, ...styles.inlineInputUnbordered, marginTop: 0 }} stackedLabel>
                  <Label style={styles.label}>{'Program Type'.toUpperCase()}</Label>
                  <Text style={styles.formText}>{ fitnessProgramTypes[program.program_type].value }</Text>
                </Item>
                <Item style={{ ...styles.inlineInput, ...styles.inlineInputUnbordered, marginTop: 0 }} stackedLabel>
                  <Label style={styles.label}>{'Fitness Program Description'.toUpperCase()}</Label>
                  <Text style={styles.formText}>{ program.description }</Text>
                </Item>
                <Separator bordered style={{...styles.separator, ...styles.separatorInForm}}>
                  <Text>{'Exercises'.toUpperCase()}</Text>
                </Separator>
                <Item style={{ marginLeft: -15, marginBottom: -10, ...styles.inlineInputUnbordered }}>
                  <Button iconLeft transparent style={styles.buttonAdd}
                    onPress={ this.handleAddExercise }
                  >
                      <Icon name="md-add" />
                      <Text style={styles.buttonAddText}>{'Add Exercise'.toUpperCase()}</Text>
                  </Button>
                </Item>
                { program.exercises ? (
                  <ExercisesList exercises={program.exercises ? JSON.parse(program.exercises) : []} openModal={(coordinates, open, active) => this.openModal(coordinates, open, active)} />
                ) : null }
                <Button style={{ ...styles.btnPrimary, ...styles.btnDelete, marginTop: 20, marginBottom: 20}} block
                  onPress={this.handleDeleteProgram }
                  >
                  <Text style={[styles.btnPrimaryText, {color: 'white'}]}>{'Delete Program'.toUpperCase()}</Text>
                </Button>
              </Form>
              <Modal
                open={this.state.open}
                modalDidOpen={() => console.log('modal did open')}
                modalDidClose={() => this.setState({open: false})}
                overlayBackground={'rgba(0, 0, 0, 0)'}
                modalStyle={{...styles.modal, top: this.state.coordinates}}
                style={{alignItems: 'center'}}>
                <View>
                  <TouchableOpacity
                    style={styles.edit}
                    onPress={this.handleEditExercise}>
                    <Text style={{color: 'rgb(97, 114, 136)'}}>{'Edit'.toUpperCase()}</Text>
                  </TouchableOpacity>
                  <TouchableOpacity
                    style={styles.delete}
                    onPress={this.handleDeleteExercise}>
                    <Text style={{color: 'rgb(97, 114, 136)'}}>{'Delete'.toUpperCase()}</Text>
                  </TouchableOpacity>
                </View>
              </Modal>
            </ScrollView>
          </Content>
        </Container>
    );
  }
}

const mapDispatchToProps = dispatch => bindActionCreators({
  deleteFitnessProgram,
  deleteExercise
  
}, dispatch);

const mapStateToProps = state => ({
  programs: state.program.fitness_programs
});

export default connect(mapStateToProps, mapDispatchToProps)(FitnessProgramView);