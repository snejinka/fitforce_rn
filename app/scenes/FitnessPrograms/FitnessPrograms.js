import React, { Component } from 'react';
import { Content, Container } from 'native-base';
import { View, ScrollView, FlatList } from 'react-native';
import styles from './assets/styles/index';
import Header from '../../components/Header';

import FitnessProgramsItem from './FitnessProgramsItem';

import { Actions } from 'react-native-router-flux';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
  createFitnessPrograms
} from '../../store/program';

class FitnessPrograms extends Component {
  _keyExtractor = (item, index) => item.id;

  renderItem = ({item}) => (
    <FitnessProgramsItem
      item={item}
    />
  );

  render() {
    const {programs} = this.props;

    return (
        <Container>
          <Header
              buttonSearch={true}
              hideButton={true}
              buttonAdd={true}
              title='Fitness Programs'
              plusButtonClick={Actions.addFitnessProgram}
              />
          <Content>
            <ScrollView style={{paddingTop: 20}}>
              <View style={styles.programItemsContainer}>
                <FlatList
                  data={[...programs]}
                  keyExtractor={this._keyExtractor}
                  renderItem={this.renderItem}
                />
              </View>
            </ScrollView>
          </Content>
        </Container>
    );
  }
}

const mapDispatchToProps = dispatch => bindActionCreators({
  createFitnessPrograms
}, dispatch);

const mapStateToProps = state => ({
  programs: state.program.fitness_programs
});

export default connect(mapStateToProps, mapDispatchToProps)(FitnessPrograms);