import React, { Component } from 'react';
import { Text, Button } from 'native-base';
import { Image, View, TouchableOpacity } from 'react-native';
import styles from '../../assets/styles/index';

import ShareIcn from '../../assets/images/share.png';
import StartIcn from '../../assets/images/start.png';

import { Actions } from 'react-native-router-flux';
import {fitnessProgramLevels, fitnessProgramTypes} from '../../constants';

class FitnessProgramsItem extends Component {
  constructor(props) {
    super(props);
    this.state = {
      types: {
        0: require('../../assets/images/flexibilityTraining.png'),
        1: require('../../assets/images/dynamicTraining.png'),
        2: require('../../assets/images/strengthTraining.png'),
        3: require('../../assets/images/aerobicTraining.png'),
        4: require('../../assets/images/circuitTraining.png'),
        5: require('../../assets/images/other.png')
      }
    }
  }

  render() {
    const { item } = this.props;
    return (
      <TouchableOpacity onPress={() => Actions.fitnessProgramView({program: item})}>
        <View style={styles.programItem}>
          <Image style={styles.programImage} source={this.state.types[item.program_type]} />
          <View style={styles.programItemHeader}>
            <View style={styles.programLevelContainer}>
              <View style={styles.programLevelBlock}>
                <Text style={styles.programLevelTitle}>{ fitnessProgramLevels[item.program_level].value }</Text>
              </View>
            </View>
            <View style={styles.programButtonBlock}>
              <Button rounded light style={styles.buttonRound}>
                <Image style={styles.buttonIcn} source={ShareIcn} />
              </Button>
              <Button rounded light style={styles.buttonRound}>
                <Image style={styles.buttonIcn} source={StartIcn} />
              </Button>
            </View>
          </View>
            <View style={styles.programItemBody}>
              <Text style={styles.programBodyTitle}>{ item.name }</Text>
              <Text style={styles.programBodySubTitle}>{ fitnessProgramTypes[item.program_type].value }</Text>
            </View>
        </View>
      </TouchableOpacity>
    );
  }
}

export default FitnessProgramsItem;