import React, { Component } from 'react';
import {
  Item,
  Button,
  Icon
} from 'native-base';
import { Image, View, Text, SectionList } from 'react-native';
import styles from '../../assets/styles/index';

class ExercisesList extends Component {
  renderSection = (section) => {
    return  <View style={styles.exerciseContainer}>
              <Item style={styles.exerciseTitleBlock}>
                <Text style={styles.exerciseTitle}>{ section.title }</Text>
              </Item>
            </View>
  }

  renderItem = (item) => {
    let name = item.name ? item.name : 'n/a',
        sets = item.sets ? item.sets : 'n/a',
        reps = item.reps ? item.reps : 'n/a',
        rest = item.rest ? item.rest : 'n/a',
        load = item.load ? item.load : 'n/a',
        blockIndex = item.blockIndex,
        itemIndex = item.itemIndex;

    return  <Item style={styles.exerciseItem}>
              <View style={{flexBasis: 270}}>
                <Text style={styles.exerciseLabel}>{this.capitalizeFirstLetter(name)}</Text>
                <Text style={styles.exerciseName}>{sets} sets of {reps} reps - Rest {rest} s - Load {load} kg</Text>
              </View>
              <Button light style={styles.buttonMore} onPress={() => this.handleMenuPress(blockIndex, itemIndex, item)}>
                <Icon style={styles.exercisesListItemOptions} name="ios-more-outline"/>
              </Button>
            </Item>
  }

  sortExercisesByBodyPart = () => {
    let sortedExercises = [];
    const { exercises } = this.props;

      if (exercises) {
        let parts = [];
  
        exercises.map((item) => {
          if (parts.indexOf(item.body_part) === -1) {
            parts.push(item.body_part);
          };
        });
    
        sortedExercises = parts.map((part) => {return {key: part, title: part, data:[]}});
        
        sortedExercises.map((exerciseItm, index) => {
          exercises.map((exercise, itemIndex) => {
            if (exercise.body_part === exerciseItm.title) {
              sortedExercises[index].data.push({...exercise, blockIndex: index, itemIndex: itemIndex});
            };
          });
        });
        return sortedExercises;
      }
  }

  _keyExtractor = (item, index) => item.id;

  handleMenuPress = (blockIndex, itemIndex, item) => {
    let coordinates = 90 + (blockIndex * 40) + (itemIndex * 70);
    this.props.openModal(coordinates, true, item);
  }

  capitalizeFirstLetter = (string) => {
    return string.charAt(0).toUpperCase() + string.slice(1);
  }

  render() {
    let exercises = this.sortExercisesByBodyPart();

    return (
      <View>
        <SectionList
          sections={exercises}
          renderItem={({item}) => this.renderItem(item)}
          renderSectionHeader={({section}) => this.renderSection(section)}
          keyExtractor={this._keyExtractor}
        />
      </View>
    );
  }
}

export default ExercisesList;