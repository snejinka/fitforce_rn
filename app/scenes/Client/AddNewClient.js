import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
  Content,
  Text,
  Form,
  Item,
  Input,
  Label,
  Button,
  Icon,
  Container,
} from 'native-base';
import { Image, View, ScrollView } from 'react-native';
import styles from '../../assets/styles/index';
import AvatarDefault from '../../assets/images/avatarDefault.png';
import Header from '../../components/Header';
import {createClient} from '../../store/client';
import { Actions } from 'react-native-router-flux';
import DatePicker from 'react-native-datepicker'
import ModalDropdown from 'react-native-modal-dropdown';
import {TextInputMask} from 'react-native-masked-text';
import ModalConfirm from '../../components/ModalConfirm';


class AddNewClient extends Component {
  constructor(props){
    super(props);
    this.state = {
      first_name:'',
      last_name:'',
      gender:'',
      DOB:'',
      email:'',
      address:'',
      medical_conditions:'',
      notes:'',
      phone_number:'',
      initial:true,
      modalVisible:false
    }
  }

  handleCreateClient(newClientData){
    let {first_name,last_name,gender,DOB,email,address,medical_conditions,notes,phone_number} = this.state;
    this.setState({
      initial:false
    })
    if(!first_name || !last_name || !email || !this.validateEmail(email) || phone_number && phone_number.length < 15){
      return;
    }
    if(this.props.clients.length == this.props.max_clients_count){
      this.setState({
        modalVisible:true
      })
    }else{
      this.props.createClient(newClientData);
      Actions.pop();
    }
  }

  validateEmail = (email) => {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
  };

  onPressDateCustom(date){
    this.setState({DOB: date})
  }
  
  
  render() {
    let {first_name,last_name,gender,DOB,email,address,medical_conditions,notes,phone_number,initial} = this.state;
    let newClientData = {
      first_name: first_name ? first_name : null,
      last_name: last_name ? last_name : null,
      phone_number: phone_number ? phone_number : null,
      gender: gender ? gender : null,
      DOB: DOB ? DOB : null,
      email: email ? email : null,
      address: address ? address : null,
      medical_conditions: medical_conditions ? medical_conditions : null,
      notes: notes ? notes : null
    }
    return (
      <Container>
        <Header title="Add New Client" buttonTitle="Save" rightButtonCallback={() => this.handleCreateClient(newClientData)}/>
        <Content>
          <ModalConfirm
              isVisible={this.state.modalVisible}
              firstLineText="On your current plan can be a maximum of 3 client. To add a new client you need"
              secondLineText="to upgrade your plan."
              buttonNoText="Cancel"
              buttonYesText="Ok"
              handleNo={() => this.setState({modalVisible:false})}
              handleYes={() => {this.setState({modalVisible:false});Actions.subscription()}}
              handleClose={() => this.setState({modalVisible:false})}
          />
          <ScrollView style={{ ...styles.scrollview, ...styles.scrollviewFull, ...styles.scrollviewNopadding }}>
            <View style={styles.uploadPhotoContainer}>
              <Image style={styles.uploadPhotoImage} source={AvatarDefault} />
              <Text style={styles.uploadPhotoText}>Upload Photo</Text>
            </View>
            <Form style={styles.form}>
              <View style={styles.formRow}>
                <Item
                    style={initial || first_name
                    ? { ...styles.inlineInput, ...styles.inlineInputMargin }
                    : {...styles.inlineInput, ...styles.inlineInputMargin,...styles.inputInvalid}}
                    stackedLabel>
                  <Label style={styles.label}>{'First Name'.toUpperCase()}</Label>
                  <Input
                      error={!first_name}
                      style={styles.input}
                      value={this.state.first_name}
                      name="first_name"
                      onChangeText={(text) => this.setState({first_name:text})}
                  />
                </Item>
                <Item
                    stackedLabel
                    error={!last_name}
                    style={initial || last_name
                    ? { ...styles.inlineInput }
                    : {...styles.inlineInput,...styles.inputInvalid}}>
                  <Label style={styles.label}>{'Last Name'.toUpperCase()}</Label>
                  <Input
                      error={!last_name}
                      style={styles.input}
                      value={this.state.last_name}
                      name="last_name"
                      onChangeText={(text) => this.setState({last_name:text})}
                  />
                </Item>
              </View>
              <View style={styles.formRow}>
                <Item style={{ ...styles.inlineInput, ...styles.inlineInputMargin }} stackedLabel>
                  <Label style={styles.label}>{'Gender'.toUpperCase()}</Label>
                  <ModalDropdown
                      style={styles.dropdown}
                      textStyle={styles.dropdownText}
                      dropdownStyle={styles.dropdownItemsContainer}
                      dropdownTextStyle={styles.dropdownText}
                      adjustFrame={(style) => {style.height = 120, style.top = 377, style.left = 20, style.width = 155}}
                      options={['male', 'female','other']}
                      onSelect={(i,value) => this.setState({gender:value})}
                  />
                </Item>
                <Item stackedLabel style={styles.inlineInput}>
                  <Label style={styles.label}>{'Birthday'.toUpperCase()}</Label>
                  <Input
                      style={styles.input}
                      value={this.state.DOB}
                      name="DOB"
                      onFocus={() => this.datepicker.onPressDate()}
                  />
                  <DatePicker
                      style={{width: 0,height:0}}
                      date={this.state.DOB}
                      mode="date"
                      format="YYYY/MM/DD"
                      maxDate={new Date()}
                      confirmBtnText="Confirm"
                      cancelBtnText="Cancel"
                      hideText={true}
                      showIcon={false}
                      onDateChange={(date) => this.onPressDateCustom(date)}
                      ref={(d) => { this.datepicker = d }}
                  />
                </Item>
              </View>
              <Item
                  style={initial || (email && this.validateEmail(email))
                  ? styles.inlineInput
                  : {...styles.inlineInput, ...styles.inputInvalid}} stackedLabel>
                <Label style={styles.label}>{'Email'.toUpperCase()}</Label>
                <View
                    style={styles.stackedInputIconContainer}>
                  <Input
                      value={email}
                      error={!email || !this.validateEmail(email)}
                      style={styles.input}
                      name="email"
                      onChangeText={(text) => this.setState({email:text})}
                  />
                  <Icon style={styles.stackedInputIcon} active name="ios-mail" />
                </View>
              </Item>
              <Item
                  error={phone_number && phone_number.length < 15}
                  style={!phone_number || (phone_number && phone_number.length == 15)
                      ? {...styles.inlineInput, flex:1}
                      : {...styles.inlineInput, flex:1, ...styles.inputInvalid}}
                  stackedLabel>
                <Label style={styles.label}>{'Phone Number'.toUpperCase()}</Label>
                <View style={styles.stackedInputIconContainer}>
                  <TextInputMask
                      style={{...styles.input,flex:1}}
                      value={phone_number}
                      type={'custom'}
                      onChangeText={(phone) => this.setState({phone_number:phone})}
                      options={{
					    mask: '+9 999-999-9999'
                      }} />
                  <Icon style={styles.stackedInputIcon} active name="ios-call" />
                  <Icon style={styles.stackedInputIcon} active name="ios-text" />
                </View>
              </Item>
              <Item style={{ ...styles.inlineInput }} stackedLabel>
                <Label style={styles.label}>{'Address'.toUpperCase()}</Label>
                <View style={styles.stackedInputIconContainer}>
                  <Input
                      style={styles.input}
                      value={this.state.address}
                      name="address"
                      onChangeText={(text) => this.setState({address:text})}
                  />
                  <Icon style={styles.stackedInputIcon} active name="ios-pin" />
                </View>
              </Item>
              <Item style={{ ...styles.inlineInput }} stackedLabel>
                <Label style={styles.label}>{'Medical Conditions'.toUpperCase()}</Label>
                <Input
                    style={styles.input}
                    multiline
                    value={this.state.medical_conditions}
                    name="medical_conditions"
                    onChangeText={(text) => this.setState({medical_conditions:text})}
                />
              </Item>
              <Item style={{ ...styles.inlineInput }} stackedLabel>
                <Label style={styles.label}>{'Notes'.toUpperCase()}</Label>
                <Input
                    style={{...styles.input,...styles.inputMultiline}}
                    multiline
                    value={this.state.notes}
                    name="notes"
                    onChangeText={(text) => this.setState({notes:text})}
                />
              </Item>
              <Item style={{ ...styles.inlineInput, ...styles.noBorderBottom }} stackedLabel>
                <Label style={styles.label}>{'Files'.toUpperCase()}</Label>
                <Button iconLeft transparent style={styles.buttonAdd}>
                  <Icon name="md-add" />
                  <Text style={styles.buttonAddText}>{'Add New File'.toUpperCase()}</Text>
                </Button>
              </Item>
              <Button style={{ ...styles.btnPrimary, ...styles.btnDisable }} block>
                <Text style={styles.btnPrimaryText}>{'INVITE USER TO FITFORCE'.toUpperCase()}</Text>
              </Button>
              <Button style={{ ...styles.btnPrimary, ...styles.btnDelete, ...styles.btnDisable }} block>
                <Text style={styles.btnPrimaryText}>{'Delete User'.toUpperCase()}</Text>
              </Button>
            </Form>
          </ScrollView>
        </Content>
      </Container>
    );
  }
}

const mapDispatchToProps = dispatch => bindActionCreators({
  createClient
}, dispatch);

const mapStateToProps = state => ({
  max_clients_count: state.onboarding.max_clients_count,
  clients: state.client.clients,
});

export default connect(mapStateToProps, mapDispatchToProps)(AddNewClient);
