import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
  Container,
  Content,
  Text,
  Form,
  Item,
  Input,
  Label,
  Button,
  Icon,
  List,
  ListItem,
  Left,
  Body,
  Right,
} from 'native-base';
import { Image, View, ScrollView } from 'react-native';
import styles,{combineStyles} from '../../assets/styles/index';
import Avatar from '../../assets/images/appointmentAvatar1.png';
import Header from '../../components/Header';
import {editClientInfo,deleteClient,inviteClient} from '../../store/client'
import { Actions,ActionConst } from 'react-native-router-flux';
import DatePicker from 'react-native-datepicker';
import ModalDropdown from 'react-native-modal-dropdown';
import {TextInputMask} from 'react-native-masked-text';
import Modal from 'react-native-modal'

class EditClient extends Component {
  constructor(props){
    super(props);
    this.state = {
      first_name:props.client.first_name,
      last_name:props.client.last_name,
      gender:props.client.gender,
      DOB:props.client.DOB,
      email:props.client.email,
      address:props.client.address,
      medical_conditions:props.client.medical_conditions,
      notes:props.client.notes,
      phone_number:props.client.phone_number,
      initial:true,
      isModalConfirmVisible:false
    }
  }

  handleSaveClient(newClientData){
    let {first_name,last_name,gender,DOB,email,address,medical_conditions,notes,phone_number} = this.state;
    this.setState({
      initial:false
    })
    if(!first_name || !last_name || !email || !this.validateEmail(email) || phone_number && phone_number.length < 15){
      return;
    }
    this.props.editClientInfo(this.props.client.id,newClientData);
    Actions.pop();
  }

  validateEmail = (email) => {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
  };

  onPressDateCustom(date){
    this.setState({DOB: date})
  }

  handleDeleteClient(){
    this.setState({isModalConfirmVisible:false})
    this.props.deleteClient(this.props.client.id,this.props.client)
    Actions.drawer()
    Actions.dashboard()
  }

  handleInviteClient(){
    this.props.inviteClient(this.props.client.id)
    Actions.drawer()
    Actions.dashboard()
  }

  render() {
    let {first_name,last_name,gender,DOB,email,address,medical_conditions,notes,phone_number,initial} = this.state;
    let newClientData = {first_name: first_name,last_name:last_name,phone_number:phone_number,gender:gender,DOB:DOB,email:email,address:address,medical_conditions:medical_conditions,notes:notes}
    return (
      <Container>
        <Header title={first_name + ' ' + last_name } buttonTitle="Save" rightButtonCallback={() => this.handleSaveClient(newClientData)}/>
        <Content>
          <ScrollView style={{ ...styles.scrollview, ...styles.scrollviewFull, ...styles.scrollviewNopadding,flex:1 }}>
            <View style={styles.uploadPhotoContainer}>
              <Image style={styles.uploadPhotoImage} source={Avatar} />
              <Text style={styles.uploadPhotoText}>Change Photo</Text>
            </View>
            <Form style={styles.form}>
              <View style={styles.formRow}>
                <Item
                    style={initial || first_name
                    ? { ...styles.inlineInput, ...styles.inlineInputMargin }
                    : {...styles.inlineInput, ...styles.inlineInputMargin,...styles.inputInvalid}}
                    stackedLabel>
                  <Label style={styles.label}>{'First Name'.toUpperCase()}</Label>
                  <Input
                      error={!first_name}
                      style={styles.input}
                      value={this.state.first_name}
                      name="first_name"
                      onChangeText={(text) => this.setState({first_name:text})}
                  />
                </Item>
                <Item
                    stackedLabel
                    error={!last_name}
                    style={initial || last_name
                    ? { ...styles.inlineInput }
                    : {...styles.inlineInput,...styles.inputInvalid}}>
                  <Label style={styles.label}>{'Last Name'.toUpperCase()}</Label>
                  <Input
                      error={!last_name}
                      style={styles.input}
                      value={this.state.last_name}
                      name="last_name"
                      onChangeText={(text) => this.setState({last_name:text})}
                  />
                </Item>
              </View>
              <View style={styles.formRow}>
                <Item style={{ ...styles.inlineInput, ...styles.inlineInputMargin,alignItems:'flex-start' }} stackedLabel>
                  <Label style={styles.label}>{'Gender'.toUpperCase()}</Label>
                  <ModalDropdown
                      style={styles.dropdown}
                      textStyle={styles.dropdownText}
                      dropdownStyle={styles.dropdownItemsContainer}
                      dropdownTextStyle={styles.dropdownText}
                      adjustFrame={(style) => {style.height = 120, style.top = 377, style.left = 20, style.width = 155}}
                      options={['male', 'female','other']}
                      onSelect={(i,value) => this.setState({gender:value})}
                  />
                </Item>
                <Item stackedLabel style={styles.inlineInput}>
                  <Label style={styles.label}>{'Birthday'.toUpperCase()}</Label>
                  <Input
                      style={styles.input}
                      value={this.state.DOB}
                      name="DOB"
                      onFocus={() => this.datepicker.onPressDate()}
                  />
                  <DatePicker
                      style={{width: 0,height:0}}
                      date={this.state.DOB}
                      mode="date"
                      format="YYYY/MM/DD"
                      maxDate={new Date()}
                      confirmBtnText="Confirm"
                      cancelBtnText="Cancel"
                      hideText={true}
                      showIcon={false}
                      onDateChange={(date) => this.onPressDateCustom(date)}
                      ref={(d) => { this.datepicker = d }}
                  />
                </Item>
              </View>
              <Item
                  style={initial || (email && this.validateEmail(email))
                  ? styles.inlineInput
                  : {...styles.inlineInput, ...styles.inputInvalid}} stackedLabel>
                <Label style={styles.label}>{'Email'.toUpperCase()}</Label>
                <View
                    style={styles.stackedInputIconContainer}>
                  <Input
                      value={email}
                      error={!email || !this.validateEmail(email)}
                      style={styles.input}
                      name="email"
                      onChangeText={(text) => this.setState({email:text})}
                  />
                  <Icon style={styles.stackedInputIcon} active name="ios-mail" />
                </View>
              </Item>
              <Item
                  error={phone_number && phone_number.length < 15}
                  style={!phone_number || (phone_number && phone_number.length == 15)
                      ? {...styles.inlineInput, flex:1}
                      : {...styles.inlineInput, flex:1, ...styles.inputInvalid}}
                  stackedLabel>
                <Label style={styles.label}>{'Phone Number'.toUpperCase()}</Label>
                <View style={styles.stackedInputIconContainer}>
                  <TextInputMask
                      style={{...styles.input,flex:1}}
                      value={phone_number}
                      type={'custom'}
                      onChangeText={(phone) => this.setState({phone_number:phone})}
                      options={{
					    mask: '+9 999-999-9999'
                      }} />
                  <Icon style={styles.stackedInputIcon} active name="ios-call" />
                  <Icon style={styles.stackedInputIcon} active name="ios-text" />
                </View>
              </Item>
              <Item style={{ ...styles.inlineInput }} stackedLabel>
                <Label style={styles.label}>{'Address'.toUpperCase()}</Label>
                <View style={styles.stackedInputIconContainer}>
                  <Input
                      style={styles.input}
                      value={this.state.address}
                      name="address"
                      onChangeText={(text) => this.setState({address:text})}
                  />
                  <Icon style={styles.stackedInputIcon} active name="ios-pin" />
                </View>
              </Item>
              <Item style={{ ...styles.inlineInput }} stackedLabel>
                <Label style={styles.label}>{'Medical Conditions'.toUpperCase()}</Label>
                <Input
                    style={styles.input}
                    multiline
                    value={this.state.medical_conditions}
                    name="medical_conditions"
                    onChangeText={(text) => this.setState({medical_conditions:text})}
                />
              </Item>
              <Item style={{ ...styles.inlineInput }} stackedLabel>
                <Label style={styles.label}>{'Notes'.toUpperCase()}</Label>
                <Input
                    style={{...styles.input,...styles.inputMultiline}}
                    multiline
                    value={this.state.notes}
                    name="notes"
                    onChangeText={(text) => this.setState({notes:text})}
                />
              </Item>
              <Item style={{ ...styles.inlineInput }} stackedLabel>
                <Label style={styles.label}>{'Files'.toUpperCase()}</Label>
                <Button iconLeft transparent style={styles.buttonAdd}>
                  <Icon name="md-add" />
                  <Text style={styles.buttonAddText}>{'Add New File'.toUpperCase()}</Text>
                </Button>
              </Item>
              <List style={styles.fileList}>
                <ListItem icon style={styles.fileListItem}>
                  <Left style={styles.fileListItemLeft}>
                    <Icon style={styles.fileListItemLeftIcon} name="ios-document-outline" />
                  </Left>
                  <Body style={styles.fileListItemBody}>
                    <Text style={styles.fileListItemText} ellipsizeMode="tail" numberOfLines={1} >Long File Name</Text>
                    <Text style={{ ...styles.fileListItemText, ...styles.fileListItemDate }} numberOfLines={1}>,Sep 13, 2017</Text>
                  </Body>
                  <Right style={styles.fileListItemRight}>
                    <Icon style={styles.fileListItemRightIcon} name="ios-trash" />
                    <Icon style={styles.fileListItemRightIcon} name="md-download" />
                  </Right>
                </ListItem>
                <ListItem icon style={styles.fileListItem}>
                  <Left style={styles.fileListItemLeft}>
                    <Icon style={styles.fileListItemLeftIcon} name="ios-document-outline" />
                  </Left>
                  <Body style={styles.fileListItemBody}>
                    <Text style={styles.fileListItemText} ellipsizeMode="tail" numberOfLines={1} >Very very Long File Name</Text>
                    <Text style={{ ...styles.fileListItemText, ...styles.fileListItemDate }} numberOfLines={1}>,Sep 13, 2017</Text>
                  </Body>
                  <Right style={styles.fileListItemRight}>
                    <Icon style={styles.fileListItemRightIcon} name="ios-trash" />
                    <Icon style={styles.fileListItemRightIcon} name="md-download" />
                  </Right>
                </ListItem>
                <ListItem icon style={{ ...styles.fileListItem, ...styles.fileListItemLast }}>
                  <Left style={styles.fileListItemLeft}>
                    <Icon style={styles.fileListItemLeftIcon} name="ios-document-outline" />
                  </Left>
                  <Body style={styles.fileListItemBody}>
                    <Text style={styles.fileListItemText} ellipsizeMode="tail" numberOfLines={1} >Long File Name</Text>
                    <Text style={{ ...styles.fileListItemText, ...styles.fileListItemDate }} numberOfLines={1}>,Sep 13, 2017</Text>
                  </Body>
                  <Right style={styles.fileListItemRight}>
                    <Icon style={styles.fileListItemRightIcon} name="ios-trash" />
                    <Icon style={styles.fileListItemRightIcon} name="md-download" />
                  </Right>
                </ListItem>

              </List>
              {this.props.client.status !== 'invited' &&
              <Button style={{ ...styles.btnPrimary, ...styles.btnDisable }} block onPress={() => this.handleInviteClient()}>
                <Text style={styles.btnPrimaryText}>{'INVITE USER TO FITFORCE'.toUpperCase()}</Text>
              </Button>
              }
              <Button style={{ ...styles.btnPrimary, ...styles.btnDelete, ...styles.btnDisable }} block onPress={() => this.setState({isModalConfirmVisible:true})}>
                <Text style={styles.btnPrimaryText}>{'Delete User'.toUpperCase()}</Text>
              </Button>
              <Modal isVisible={this.state.isModalConfirmVisible}>
                <View style={styles.modalPrimary }>
                  <Text style={styles.modalPrimaryTitle}>Are you sure you want to delete</Text>
                  <Text style={styles.modalPrimaryTitle}>{first_name + ' ' + last_name }?</Text>
                  <View style={styles.modalPrimaryButtonsContainer}>
                    <Button style={combineStyles(styles.btnPrimary,{margin:10})} block onPress={() => this.setState({isModalConfirmVisible:false})}>
                      <Text style={styles.btnPrimaryText}>No</Text>
                    </Button>
                    <Button style={combineStyles(styles.btnPrimary,{margin:10})} block onPress={() => this.handleDeleteClient()}>
                      <Text style={styles.btnPrimaryText}>Yes</Text>
                    </Button>
                  </View>
                </View>
              </Modal>
            </Form>
          </ScrollView>
        </Content>
      </Container>
    );
  }
}

const mapDispatchToProps = dispatch => bindActionCreators({
  editClientInfo,
  deleteClient,
  inviteClient
}, dispatch);

const mapStateToProps = state => ({
});

export default connect(mapStateToProps, mapDispatchToProps)(EditClient);
