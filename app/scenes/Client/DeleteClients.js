import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
  Text,
  List,
  ListItem,
  Left,
  Thumbnail,
  Body,
  Container,
  Button
} from 'native-base';
import { Image, ScrollView } from 'react-native';
import styles from '../../assets/styles/index';
import Avatar from '../../assets/images/appointmentAvatar1.png';
import { Actions } from 'react-native-router-flux';
import {getClientsInfo,getClientInfo,deleteManyClients} from  '../../store/client';
import {setSession} from '../../store/user';
import Header from '../../components/Header';
import CheckboxItem from '../../components/CheckboxItem';
import ModalConfirm from '../../components/ModalConfirm';


class DeleteClients extends Component {
    constructor(props){
        super(props);
        this.state = {
            modalVisible:false,
            deletedClientsIds:[]
        }
        this.deletedClientsIds = []
    }

    componentWillMount(){
        this.props.getClientsInfo()
    }
    componentWillUnmount(){
        this.props.getClientsInfo()
    }

    handleClickCheckbox(client_id){
        const clientIdIndex = this.deletedClientsIds.indexOf(client_id);
        if (clientIdIndex > -1) {
            this.deletedClientsIds.splice(clientIdIndex, 1)
        } else {
            this.deletedClientsIds.push(client_id)
        }
        this.setState({
            deletedClientsIds:this.deletedClientsIds
        })
    }

    handleDelete(){
        this.setState({
            modalVisible:true,
        })
    }

    handleYes(){
        this.setState({modalVisible:false})
        this.props.deleteManyClients(this.deletedClientsIds)

        if(this.props.user.session == 'blocked'){
            if(this.props.clients.length <= this.props.max_clients_count){
                this.props.setSession('authorized');
            }
        }
    }

  render() {
    return (
      <Container>
          <ModalConfirm
              isVisible={this.state.modalVisible}
              firstLineText={"Are you sure you want to delete the clients?"}
              buttonNoText="No"
              buttonYesText="Yes"
              handleNo={() => this.setState({modalVisible:false})}
              handleYes={() => this.handleYes()}
              handleClose={() => this.setState({modalVisible:false})}
          />
          <Header
              title="Delete users"
              hideButton={true}
          />
            <ScrollView style={{ ...styles.scrollview, ...styles.scrollviewFull, ...styles.scrollviewNopadding }}>
              <List style={styles.userList}>
                {this.props.clients.map((client,i) => {
                  return (
                      <ListItem avatar style={[styles.userListItem,{marginLeft:50,marginRight:0}]} key={i} onPress={() => {this.props.getClientInfo(client.id);Actions.viewClient({client:client})}}>
                          <CheckboxItem
                              key={'CheckboxItem'+i}
                              checked={this.deletedClientsIds.includes(client.id)}
                              handleClick={() => this.handleClickCheckbox(client.id)}
                              listItemStyle={[styles.noBorderBottom, styles.noMarginLeft,{position:'absolute',top:-20,left:-30}]}/>
                        <Left>
                          <Thumbnail source={Avatar} style={styles.userListAvatar} />
                        </Left>
                        <Body style={styles.userListItemBody}>
                        <Text style={styles.userListUsername}>{client.first_name + ' ' + client.last_name}</Text>
                        </Body>
                      </ListItem>
                  )
                })}
              </List>
            </ScrollView>
          {this.deletedClientsIds.length > 0 &&
              <Button
                  style={[styles.btnPrimary,{alignSelf:'stretch',justifyContent:'center',marginRight:20,marginLeft:20}]}
                  onPress={() => this.handleDelete()}
              >
                  <Text style={[styles.btnPrimaryText]}>{'Delete'.toUpperCase()}</Text>
              </Button>
          }
      </Container>
    );
  }
}


const mapDispatchToProps = dispatch => bindActionCreators({
    getClientsInfo,
    getClientInfo,
    deleteManyClients,
    setSession
}, dispatch);

const mapStateToProps = state => ({
    clients: state.client.clients,
    user: state.user,
    max_clients_count: state.onboarding.max_clients_count
});

export default connect(mapStateToProps, mapDispatchToProps)(DeleteClients);
