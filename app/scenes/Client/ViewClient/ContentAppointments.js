import React, { Component } from 'react';
import PropTypes from 'prop-types'
import { Image, View, ScrollView } from 'react-native';
import styles from '../../../assets/styles/index';
import IconAwesome from 'react-native-vector-icons/FontAwesome';
import AppointmentItem from './../../../components/AppointmentItem'

import {
    Text,
    Button,
    Icon,
} from 'native-base';
import {Actions} from 'react-native-router-flux'

class ContentAppointments extends Component{
    static propTypes = {
        appointments: PropTypes.array,
        clients: PropTypes.array
    }
    

    render(){
        const {appointments,clients} = this.props;
        return(
            <View style={styles.contentAppointments}>
                <Button iconLeft transparent style={[styles.buttonAdd,styles.contentAppointmentsButton]} onPress={() => Actions.createAppointment()}>
                    <Icon name="md-add" />
                    <Text style={styles.buttonAddText}>{'Create New Appointment'.toUpperCase()}</Text>
                </Button>
                <View style={[styles.dateWeekContainer]}>
                    <Button style={[styles.dateWeekContainerButtonArrow]}>
                        <IconAwesome style={[styles.dateWeekContainerIcon]} name="angle-left" />
                    </Button>
                    <Text style={[styles.dateWeekContainerText]}>Apr 14, 2017 - Apr 20, 2017</Text>
                    <Button style={[styles.dateWeekContainerButtonArrow]}>
                        <IconAwesome style={[styles.dateWeekContainerIcon]} name="angle-right" />
                    </Button>
                </View>
                <ScrollView style={[styles.scrollview,{marginLeft:-20, marginRight:-20}]}>
                    {appointments && appointments.map((appointment,i) => {
                        return(
                            <AppointmentItem appointments={appointments} appointment={appointment} clients={clients} keyProp={i} key={i}/>
                        )
                    })}
                </ScrollView>
            </View>
        )
    }
}

export default ContentAppointments;