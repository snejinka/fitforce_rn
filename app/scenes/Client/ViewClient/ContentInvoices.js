import React, { Component } from 'react';
import PropTypes from 'prop-types'
import { Image, View, ScrollView } from 'react-native';
import styles from '../../../assets/styles/index';
import TabInvoices from './../../../components/TabInvoices'

import {
    Text,
    Button,
    Icon,
} from 'native-base';
import {Actions} from 'react-native-router-flux'

class ContentAppointments extends Component{
    static propTypes = {
        appointments: PropTypes.array,
        clients: PropTypes.array,
        client: PropTypes.object,
    }
    

    render(){
        const {client,appointments} = this.props;
        return(
            <View>
                <View style={[styles.contentAppointments,{flex:0,borderBottomWidth:1,borderBottomColor:'rgba(57, 63, 98, 0.05)'}]}>
                    <Button 
                        iconLeft 
                        transparent 
                        style={[styles.buttonAdd,styles.contentAppointmentsButton]} 
                        onPress={() => Actions.createInvoice({client,appointments})}>
                        <Icon name="md-add" />
                        <Text style={styles.buttonAddText}>{'Create New Invoice'.toUpperCase()}</Text>
                    </Button>
                </View>
                <TabInvoices/>

            </View>

        )
    }
}

export default ContentAppointments;