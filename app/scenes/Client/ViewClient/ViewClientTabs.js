import React, { Component } from 'react';
import { Image, View, ScrollView } from 'react-native';
import styles from '../../../assets/styles/index';
import {
    Text,
    Button
} from 'native-base';
import Appointment from '../../../assets/images/appointment.png';
import Invoices from '../../../assets/images/invoices.png';
import FitnessRecord from '../../../assets/images/fitnessRecord.png';
import PropTypes from 'prop-types';
import {selectedTabsViewClient} from '../../../constants'


class ViewClientTabs extends Component{
    constructor(props){
        super(props)
        this.state = {
            selectedTab:selectedTabsViewClient.nothingSelected
        }
    }

    static propTypes = {
        changeSelectedTab: PropTypes.func
    }
    
    handleTab(tab){
        this.props.changeSelectedTab(tab)
        this.setState({
            selectedTab:tab
        })
    }

    render(){
        return(
            <View style={styles.profileLinks}>
                <Button 
                    style={[styles.profileLinksItem]}
                    onPress={() => this.handleTab(selectedTabsViewClient.appointments)}>
                    <Image style={styles.profileLinksItemImage} source={Appointment} />
                    <Text style={this.state.selectedTab == selectedTabsViewClient.appointments ? [styles.profileLinksItemText,styles.profileLinksItemTextActive] : [styles.profileLinksItemText]}>{'Appointments'.toUpperCase()}</Text>
                </Button>
                <Button style={styles.profileLinksItem} onPress={() => this.handleTab(selectedTabsViewClient.invoices)}>
                    <Image style={styles.profileLinksItemImage} source={Invoices} />
                    <Text style={styles.profileLinksItemText}>{'Invoices'.toUpperCase()}</Text>
                </Button>
                <Button style={styles.profileLinksItem} onPress={() => this.handleTab(selectedTabsViewClient.fitnessRecords)}>
                    <Image style={styles.profileLinksItemImage} source={FitnessRecord} />
                    <Text style={styles.profileLinksItemText}>{'Fitness Records and Logs'.toUpperCase()}</Text>
                </Button>
            </View>
        )
    }
}

export default ViewClientTabs;