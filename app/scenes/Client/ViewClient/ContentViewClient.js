import React, { Component } from 'react';
import { Image, View, ScrollView } from 'react-native';
import styles from '../../../assets/styles/index';
import {
    Text,
    Form,
    Item,
    Input,
    Label,
    Button,
    Icon,
    List,
    ListItem,
    Left,
    Body,
    Right,
} from 'native-base';
import PropTypes from 'prop-types';


class ContentViewClient extends Component{
    constructor(props){
        super(props);
        this.state = {
            
        }
    }
    static propTypes = {
        client: PropTypes.object
    }
    
    changeSelectedTab(tab){
        this.props.changeSelectedTab(tab)
    }

    render(){
        let {client} = this.props;
        return(
            <Form style={styles.form}>
                <View style={styles.formRow}>
                    <Item style={{ ...styles.inlineInput, ...styles.inlineInputMargin }} stackedLabel>
                        <Label style={styles.label}>{'First Name'.toUpperCase()}</Label>
                        <Input style={styles.input} value={client.first_name} editable={false}/>
                    </Item>
                    <Item stackedLabel style={styles.inlineInput}>
                        <Label style={styles.label}>{'Last Name'.toUpperCase()}</Label>
                        <Input style={styles.input} value={client.last_name} editable={false}/>
                    </Item>
                </View>
                <View style={styles.formRow}>
                    <Item style={{ ...styles.inlineInput, ...styles.inlineInputMargin }} stackedLabel>
                        <Label style={styles.label}>{'Gender'.toUpperCase()}</Label>
                        <Input style={styles.input} value={client.gender} editable={false}/>
                    </Item>
                    <Item stackedLabel style={styles.inlineInput}>
                        <Label style={styles.label}>{'Birthday'.toUpperCase()}</Label>
                        <Input style={styles.input} value={client.DOB} editable={false}/>
                    </Item>
                </View>
                <Item style={{ ...styles.inlineInput }} stackedLabel>
                    <Label style={styles.label}>{'Email'.toUpperCase()}</Label>
                    <View style={styles.stackedInputIconContainer}>
                        <Input style={styles.input} value={client.email} editable={false}/>
                        <Icon style={styles.stackedInputIcon} active name="ios-mail" />
                    </View>
                </Item>
                <Item style={{ ...styles.inlineInput }} stackedLabel>
                    <Label style={styles.label}>{'Phone Number'.toUpperCase()}</Label>
                    <View style={styles.stackedInputIconContainer}>
                        <Input style={styles.input} value={client.phone_number} editable={false}/>
                        <Icon style={styles.stackedInputIcon} active name="ios-call" />
                        <Icon style={styles.stackedInputIcon} active name="ios-text" />
                    </View>
                </Item>
                <Item style={{ ...styles.inlineInput }} stackedLabel>
                    <Label style={styles.label}>{'Address'.toUpperCase()}</Label>
                    <View style={styles.stackedInputIconContainer}>
                        <Input style={styles.input} value={client.address} editable={false}/>
                        <Icon style={styles.stackedInputIcon} active name="ios-pin" />
                    </View>
                </Item>
                <Item style={{ ...styles.inlineInput }} stackedLabel>
                    <Label style={styles.label}>{'Medical Conditions'.toUpperCase()}</Label>
                    <Input style={styles.input} multiline value={client.medical_conditions} editable={false}/>
                </Item>
                <Item style={{ ...styles.inlineInput }} stackedLabel>
                    <Label style={styles.label}>{'Notes'.toUpperCase()}</Label>
                    <Input style={{...styles.input,...styles.inputMultiline}} multiline value={client.notes} editable={false}/>
                </Item>
                <Item style={{ ...styles.inlineInput }} stackedLabel>
                    <Label style={styles.label}>{'Files'.toUpperCase()}</Label>
                    <Button iconLeft transparent style={styles.buttonAdd}>
                        <Icon name="md-add" />
                        <Text style={styles.buttonAddText}>{'Add New File'.toUpperCase()}</Text>
                    </Button>
                </Item>
                <List style={styles.fileList}>
                    <ListItem icon style={styles.fileListItem}>
                        <Left style={styles.fileListItemLeft}>
                            <Icon style={styles.fileListItemLeftIcon} name="ios-document-outline" />
                        </Left>
                        <Body style={styles.fileListItemBody}>
                        <Text style={styles.fileListItemText} ellipsizeMode="tail" numberOfLines={1} >Long File Name</Text>
                        <Text style={{ ...styles.fileListItemText, ...styles.fileListItemDate }} numberOfLines={1}>,Sep 13, 2017</Text>
                        </Body>
                        <Right style={styles.fileListItemRight}>
                            <Icon style={styles.fileListItemRightIcon} name="ios-trash" />
                            <Icon style={styles.fileListItemRightIcon} name="md-download" />
                        </Right>
                    </ListItem>
                    <ListItem icon style={styles.fileListItem}>
                        <Left style={styles.fileListItemLeft}>
                            <Icon style={styles.fileListItemLeftIcon} name="ios-document-outline" />
                        </Left>
                        <Body style={styles.fileListItemBody}>
                        <Text style={styles.fileListItemText} ellipsizeMode="tail" numberOfLines={1} >Very very Long File Name</Text>
                        <Text style={{ ...styles.fileListItemText, ...styles.fileListItemDate }} numberOfLines={1}>,Sep 13, 2017</Text>
                        </Body>
                        <Right style={styles.fileListItemRight}>
                            <Icon style={styles.fileListItemRightIcon} name="ios-trash" />
                            <Icon style={styles.fileListItemRightIcon} name="md-download" />
                        </Right>
                    </ListItem>
                    <ListItem icon style={{ ...styles.fileListItem, ...styles.fileListItemLast }}>
                        <Left style={styles.fileListItemLeft}>
                            <Icon style={styles.fileListItemLeftIcon} name="ios-document-outline" />
                        </Left>
                        <Body style={styles.fileListItemBody}>
                        <Text style={styles.fileListItemText} ellipsizeMode="tail" numberOfLines={1} >Long File Name</Text>
                        <Text style={{ ...styles.fileListItemText, ...styles.fileListItemDate }} numberOfLines={1}>,Sep 13, 2017</Text>
                        </Body>
                        <Right style={styles.fileListItemRight}>
                            <Icon style={styles.fileListItemRightIcon} name="ios-trash" />
                            <Icon style={styles.fileListItemRightIcon} name="md-download" />
                        </Right>
                    </ListItem>
                </List>
            </Form>
        )
    }
}

export default ContentViewClient;