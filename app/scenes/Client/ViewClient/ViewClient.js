import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';
import {
  Content,
  Container,
  Text,
  Form,
  Item,
  Input,
  Label,
  Button,
  Icon,
  List,
  ListItem,
  Left,
  Body,
  Right,
} from 'native-base';
import { Actions } from 'react-native-router-flux';
import { Image, View, ScrollView } from 'react-native';
import Header from '../../../components/Header'
import styles from '../../../assets/styles/index';
import Avatar from '../../../assets/images/appointmentAvatar1.png';
import ViewClientTabs from './ViewClientTabs';
import ContentViewClient from './ContentViewClient';
import ContentAppointments from './ContentAppointments';
import ContentInvoices from './ContentInvoices';
import {selectedTabsViewClient} from '../../../constants'
import {getClientAppointments,getAppointments} from '../../../store/appointment';
import {getClientListInvoices} from '../../../store/invoice';


class ViewClient extends Component {
  constructor(props){
    super(props)
    this.state = {
      selectedTab:selectedTabsViewClient.nothingSelected
    }
    this.changeSelectedTab = this.changeSelectedTab.bind(this)
  }

  componentWillMount(){
    this.props.getClientAppointments(this.props.client.id)
    this.props.getClientListInvoices(this.props.client.id)
  }

  componentWillUnmount(){
    this.props.getAppointments()
  }

  changeSelectedTab(tab){
    this.setState({
      selectedTab:tab
    })
  }
  
  renderContent(){
    let {client} = this.props;
    switch (this.state.selectedTab){
      case selectedTabsViewClient.nothingSelected:
            return <ContentViewClient client={client}/>
      case selectedTabsViewClient.appointments:
            return <ContentAppointments appointments={this.props.appointments} clients={this.props.clients}/>
      case selectedTabsViewClient.invoices:
            return <ContentInvoices invoices={this.props.invoices} client={this.props.client} appointments={this.props.appointments}/>
    }
  }
  
  
  render() {
    let {client} = this.props;
    return (
      <Container>
        <Header title={client.first_name + ' ' + client.last_name } buttonTitle="Edit" rightButtonCallback={() => Actions.editClient({client:client})}/>
        <Content style={{ ...styles.scrollview, ...styles.scrollviewFull, ...styles.scrollviewNopadding }}>
          <View style={styles.uploadPhotoContainer}>
            <Image style={styles.uploadPhotoImage} source={Avatar} />
            <Text style={styles.uploadPhotoText}>Change Photo</Text>
          </View>
          <ViewClientTabs changeSelectedTab={this.changeSelectedTab}/>
          {this.renderContent()}
        </Content>
      </Container>
    );
  }
}
const mapDispatchToProps = dispatch => bindActionCreators({
  getClientAppointments,
  getAppointments,
  getClientListInvoices
}, dispatch);

const mapStateToProps = state => ({
  client: state.client.client,
  clients: state.client.clients,
  appointments: state.appointment.appointments,
  invoices: state.invoice.invoices
});

export default connect(mapStateToProps, mapDispatchToProps)(ViewClient);
