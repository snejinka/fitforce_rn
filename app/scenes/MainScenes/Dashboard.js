import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
  Content,
  Text,
  Separator,
  Container,
  Button
} from 'native-base';
import { Actions,ActionConst } from 'react-native-router-flux';
import { Image, View, ScrollView, TouchableOpacity } from 'react-native';
import styles from '../../assets/styles/index';
import ClientWhite from '../../assets/images/clientWhite.png';
import MessageWhite from '../../assets/images/messageWhite.png';
import InvoiceWhite from '../../assets/images/invoiceWhite.png';
import FooterTabs from './../../components/FooterTabs';
import AppointmentItem from './../../components/AppointmentItem';
import {getClientsInfo} from '../../store/client';
import {getAppointments} from '../../store/appointment'
import {setNetworkStatus} from '../../store/network';
import {getTrainerInfo} from '../../store/onboarding';
import {setSession} from '../../store/user';
import Header from '../../components/Header';
import ModalConfirm from '../../components/ModalConfirm';



class Dashboard extends Component {
  constructor(props){
    super(props);
    this.state = {
      modalVisible:false
    }
  }

  componentWillMount(){
    this.props.setNetworkStatus();
    this.props.getAppointments();
    this.props.getTrainerInfo();
  }

  componentWillUnmount(){
    this.props.getAppointments()
  }

  handleAddClient(){
    if(this.props.clients.length +1 <= this.props.max_clients_count){
      Actions.addNewClient()
    }else {
      this.setState({
        modalVisible:true
      })
    }
  }
  

  render() {
    const {clients,user,appointments,navigationState} = this.props;
    return (
      <Container>
        <Header
            buttonSearch={true}
            hideButton={true}
            buttonAdd={true}
            humburgerMenuVisible={true}
            headerWithLogo={true}/>
        <Text style={styles.mainTitle}>Happy Monday{user.firstName ? `, ${user.firstName}` : ''}!</Text>
        <View style={styles.fastTilesContainer}>
          <TouchableOpacity onPress={() => this.handleAddClient()} style={styles.fastTilesItem}>
            <Text style={styles.fastTilesItemTitle}>ADD CLIENT</Text>
            <Image source={ClientWhite} />
            <Text style={styles.fastTilesItemSubtitle}>
              <Text style={styles.fastTilesItemSubtitle}>{clients.length}</Text> clients
            </Text>
          </TouchableOpacity>
          <TouchableOpacity onPress={Actions.messages} style={styles.fastTilesItem}>
            <Text style={styles.fastTilesItemTitle}>MESSAGES</Text>
            <Image source={MessageWhite} />
            <Text style={styles.fastTilesItemSubtitle}>
              <Text style={styles.fastTilesItemSubtitle}>24</Text> Unread
            </Text>
          </TouchableOpacity>
          <View style={styles.fastTilesItem}>
            <Text style={styles.fastTilesItemTitle}>INVOICES</Text>
            <Image source={InvoiceWhite} />
            <Text style={styles.fastTilesItemSubtitle}>
              <Text style={styles.fastTilesItemSubtitle}>24</Text> Unpaid
            </Text>
          </View>
        </View>
        <Separator style={styles.separator}>
          <Text style={styles.scrollviewHeaderText}>{'Today’s appointments'.toUpperCase()}</Text>
        </Separator>
        <ScrollView style={styles.scrollview}>
          {appointments.length > 0 && appointments.map((appointment,i) => {
            return(
                <AppointmentItem appointments={appointments} appointment={appointment} clients={clients} keyProp={i} key={i}/>
            )
          })}
        </ScrollView>
        <FooterTabs />
        <ModalConfirm
            isVisible={this.state.modalVisible}
            firstLineText="On your current plan can be a maximum of 3 client. To add a new client you need"
            secondLineText="to upgrade your plan."
            buttonNoText="Cancel"
            buttonYesText="Ok"
            handleNo={() => this.setState({modalVisible:false})}
            handleYes={() => {this.setState({modalVisible:false});Actions.subscription()}}
            handleClose={() => this.setState({modalVisible:false})}
        />
      </Container>
    );
  }
}



const mapDispatchToProps = dispatch => bindActionCreators({
  getClientsInfo,
  setNetworkStatus,
  getAppointments,
  setSession,
  getTrainerInfo
}, dispatch);

const mapStateToProps = state => ({
  user: state.user,
  clients: state.client.clients,
  isOnlineNetwork: state.network.isOnlineNetwork,
  appointments: state.appointment.appointments,
  success_last_payment: state.onboarding.success_last_payment,
  max_clients_count: state.onboarding.max_clients_count
});

export default connect(mapStateToProps, mapDispatchToProps)(Dashboard);