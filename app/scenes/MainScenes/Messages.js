import React, { Component } from 'react';
import {
  Content,
  Text,
  List,
  ListItem,
  Left,
  Thumbnail,
  Body,
  Right,
  Container
} from 'native-base';
import { View, ScrollView } from 'react-native';
import styles from '../../assets/styles/index';
import Avatar from '../../assets/images/appointmentAvatar1.png';
import FooterTabs from './../../components/FooterTabs';
import Header from '../../components/Header';

class Messages extends Component {
  render() {
    return (
      <Container>
        <Header
            buttonSearch={true}
            hideButton={true}
            buttonAdd={true}
            humburgerMenuVisible={true}
            headerWithLogo={true}/>
        <ScrollView style={{ ...styles.scrollview, ...styles.scrollviewFull, ...styles.scrollviewNopadding }}>
          <List style={styles.userList}>
            <ListItem avatar style={styles.userListItem}>
              <Left>
                <Thumbnail source={Avatar} style={styles.userListAvatar} />
              </Left>
              <Body style={styles.userListItemBody}>
                <Text style={styles.userListUsername}>Kumar Pratik</Text>
                <Text note style={styles.userListMessage} ellipsizeMode="tail" numberOfLines={1}>Doing what you like will always keep you happy</Text>
              </Body>
              <Right style={styles.userListItemRight}>
                <Text style={styles.userListTime}>yesterday</Text>
                <View style={styles.userListUnreadMessagesContainer}>
                  <Text style={styles.userListUnreadMessages}>20</Text>
                </View>
              </Right>
            </ListItem>
            <ListItem avatar style={styles.userListItem}>
              <Left>
                <Thumbnail source={Avatar} style={styles.userListAvatar} />
              </Left>
              <Body style={styles.userListItemBody}>
                <Text style={styles.userListUsername}>Kumar Pratik</Text>
                <Text note style={styles.userListMessage} ellipsizeMode="tail" numberOfLines={1}>Doing what you like will always keep you happy</Text>
              </Body>
              <Right style={styles.userListItemRight}>
                <Text style={styles.userListTime}>00:12</Text>
                <View style={styles.userListUnreadMessagesContainer}>
                  <Text style={styles.userListUnreadMessages}>7</Text>
                </View>
              </Right>
            </ListItem>
          </List>
        </ScrollView>
        <FooterTabs />
      </Container>
    );
  }
}

export default Messages;
