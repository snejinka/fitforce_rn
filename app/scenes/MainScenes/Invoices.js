import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
  Content,
  Text,
  Button,
  Tabs,
  Tab,
  TabHeading,
  Container,
  Icon
} from 'native-base';
import Switch from 'react-native-customisable-switch';
import { Image, View, ScrollView } from 'react-native';
import styles from '../../assets/styles/index';
import Avatar from '../../assets/images/appointmentAvatar1.png';
import Header from '../../components/Header';
import FooterTabs from './../../components/FooterTabs';
import {getListInvoices} from '../../store/invoice';
import InvoiceItem from '../../components/InvoiceItem';
import TabInvoices from '../../components/TabInvoices';


class Invoices extends Component {

  componentWillMount(){
    this.props.getListInvoices()
  }

  render() {
    return (
        <Container>
          <Header
              buttonSearch={true}
              hideButton={true}
              buttonAdd={true}
              humburgerMenuVisible={true}
              headerWithLogo={true}/>
          <Content>
            <TabInvoices/>
          </Content>
          <FooterTabs />
        </Container>


    );
  }
}


const mapDispatchToProps = dispatch => bindActionCreators({
  getListInvoices
}, dispatch);

const mapStateToProps = state => ({
  user: state.user,
  invoices: state.invoice.invoices,
});

export default connect(mapStateToProps, mapDispatchToProps)(Invoices);
