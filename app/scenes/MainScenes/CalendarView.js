import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
  Content,
  Text,
  Tabs,
  Tab,
  TabHeading,
  Button,
  Container
} from 'native-base';
import { Actions } from 'react-native-router-flux';
import { Image, View, ScrollView, TouchableOpacity } from 'react-native';

import styles from '../../assets/styles/index';
import AppointmentAvatar from '../../assets/images/appointmentAvatar1.png';
import LocationIcon from '../../assets/images/pinIcon.png';
import ReloadAppointment from '../../assets/images/reload.png';
import FooterTabs from './../../components/FooterTabs';
import { Calendar } from 'react-native-calendars';
import CalendarStrip from 'react-native-calendar-strip';
import IconMaterial from 'react-native-vector-icons/MaterialCommunityIcons';
import Header from '../../components/Header';

class CalendarView extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selected: undefined,
      heightOfList:370
    };
    this.onDayPress = this.onDayPress.bind(this);
  }


  renderArrowCalendar(direction){
    if(direction == 'left'){
      return(
        <IconMaterial style={{...styles.exercisesListItemTickIcon}} name="chevron-left" />
      )
    }else{
      return(
          <IconMaterial style={{...styles.exercisesListItemTickIcon}} name="chevron-right" />
      )
    }
  }
  onDayPress(day) {
    this.setState({
      selected: day.dateString
    });
  }

  renderResults(){
    return(
        <View style={styles.searchResultsContainer}>
          <View style={styles.searchResultsList}>
            <Image source={AppointmentAvatar} style={{...styles.searchResultsListItem}} />
            <Image source={AppointmentAvatar} style={{...styles.searchResultsListItem}} />
          </View>
          <Button style={styles.searchResultsButton}><Text style={styles.searchResultsButtonText}>Clear All</Text></Button>
        </View>
    )
  }
  
  render() {
    return (
        <Container style={{flex:1}}>
          <Header
              buttonSearch={true}
              hideButton={true}
              buttonAdd={true}
              humburgerMenuVisible={true}
              headerWithLogo={true}/>
          {this.renderResults()}
          <Tabs style={styles.tabs} initialPage={1} tabBarUnderlineStyle={{ backgroundColor: 'transparent' }}>
            <Tab style={{flex:1}} heading={<TabHeading style={styles.tabHeadingSquare}><Text style={styles.tabHeadingSquareText}>{'week'.toUpperCase()}</Text></TabHeading>}>
              <View padder style={{flex:1}}>
                <CalendarStrip
                    calendarAnimation={{type: 'sequence', duration: 30}}
                    daySelectionAnimation={{type: 'background', duration: 300, highlightColor: 'rgba(57, 63, 98, 0.05)'}}
                    style={{flex:3,paddingTop: 12, paddingBottom: 35,paddingLeft: 10,paddingRight: 10, borderBottomWidth:2, borderColor:'rgba(57, 63, 98, 0.05)', marginBottom:15}}
                    calendarHeaderStyle={{color: 'rgb(102, 120, 144)', fontSize:15, lineHeight:15, fontWeight:'400', marginBottom:50}}
                    calendarColor={'transparent'}
                    highlightColor={'rgba(57, 63, 98, 0.05)'}
                    dateNumberStyle={{color: 'rgb(97, 114, 136)', fontSize:15, fontWeight:'400'}}
                    dateNameStyle={{color: 'rgb(171, 183, 203)', fontSize:11, position:'absolute', top:-20}}
                    dateContainerStyle={{marginTop:0}}
                    highlightDateNumberStyle={{color: 'rgb(79, 155, 235)',fontWeight:'400', padding:0}}
                    highlightDateNameStyle={{color: 'rgb(79, 155, 235)', position:'absolute', top:-20}}
                    iconContainer={{flex: 0.1, marginTop:50}}
                    styleWeekend={false}
                    // leftSelector={[<IconMaterial name="chevron-left" key="icon" style={{color:'red'}}/>]}
                />
                <ScrollView style={{flex:6, flexGrow:10}}>
                  <View style={{ ...styles.appointment, ...styles.appointmentMade }}>
                    <View style={styles.bullet} />
                    <View style={styles.bulletLine} />
                    <View style={styles.appointmentInfo}>
                      <View style={styles.appointmentHeader}>
                        <Text style={styles.appointmentTime}>09:00 - 10:30</Text>
                        <Text style={styles.appointmentTotalTime}>(1.5h)</Text>
                        <Image source={ReloadAppointment} />
                      </View>
                      <View style={styles.appointmentBody}>
                        <Text style={styles.appointmentText}>
                          Strength Training with
                          <Text style={styles.appointmentTextBlue}>Anna</Text>
                        </Text>
                      </View>
                      <View style={styles.appointmentFooter}>
                        <Image style={styles.appointmentLocationIcon} source={LocationIcon} />
                        <Text style={styles.appointmentTextBlue}>2551 Linn Ave Union, CA 07083</Text>
                      </View>
                    </View>
                    <View style={styles.appointmentImage}>
                      <Image source={AppointmentAvatar} />
                    </View>
                  </View>
                  <View style={styles.appointment}>
                    <View style={styles.bullet} />
                    <View style={styles.bulletLine} />
                    <View style={styles.appointmentInfo}>
                      <View style={styles.appointmentHeader}>
                        <Text style={styles.appointmentTime}>09:00 - 10:30</Text>
                        <Text style={styles.appointmentTotalTime}>(1.5h)</Text>
                        <Image source={ReloadAppointment} />
                      </View>
                      <View style={styles.appointmentBody}>
                        <Text style={styles.appointmentText}>
                          Strength Training with
                          <Text style={styles.appointmentTextBlue}>Anna</Text>
                        </Text>
                      </View>
                      <View style={styles.appointmentFooter}>
                        <Image style={styles.appointmentLocationIcon} source={LocationIcon} />
                        <Text style={styles.appointmentTextBlue}>2551 Linn Ave Union, CA 07083</Text>
                      </View>
                    </View>
                    <View style={styles.appointmentImageContainer}>
                      <Image style={styles.appointmentImage} source={AppointmentAvatar} />
                    </View>
                  </View>
                  <View style={styles.appointment}>
                    <View style={styles.bullet} />
                    <View style={styles.appointmentInfo}>
                      <View style={styles.appointmentHeader}>
                        <Text style={styles.appointmentTime}>09:00 - 10:30</Text>
                        <Text style={styles.appointmentTotalTime}>(1.5h)</Text>
                        <Image source={ReloadAppointment} />
                      </View>
                      <View style={styles.appointmentBody}>
                        <Text style={styles.appointmentText}>
                          Strength Training with
                          <Text style={styles.appointmentTextBlue}>Anna</Text>
                        </Text>
                      </View>
                      <View style={styles.appointmentFooter}>
                        <Image style={styles.appointmentLocationIcon} source={LocationIcon} />
                        <Text style={styles.appointmentTextBlue}>2551 Linn Ave Union, CA 07083</Text>
                      </View>
                    </View>
                    <View style={styles.appointmentImageContainer}>
                      <Image style={styles.appointmentImage} source={AppointmentAvatar} />
                    </View>
                  </View>
                </ScrollView>
              </View>
            </Tab>
            <Tab heading={<TabHeading style={styles.tabHeadingSquare}><Text style={styles.tabHeadingSquareText}>{'month'.toUpperCase()}</Text></TabHeading>}>
              <View padder style={{flex:1}}>
                <Calendar
                    style={styles.calendar}
                    current={Date.now}
                    minDate={Date.now}
                    onDayPress={(day) => {this.onDayPress(day)}}
                    monthFormat={'MMMM yyyy'}
                    // onMonthChange={(month) => {console.warn('month changed', month)}}
                    hideArrows={false}
                    renderArrow={(direction) => (this.renderArrowCalendar(direction))}
                    hideExtraDays={false}
                    disableMonthChange={false}
                    firstDay={1}
                    theme={{
                        calendarBackground: '#ffffff',
                        textSectionTitleColor: 'rgb(171, 183, 203)',
                        selectedDayBackgroundColor: 'rgba(57, 63, 98, 0.05)',
                        selectedDayTextColor: 'rgb(79, 155, 235)',
                        todayTextColor: 'rgb(79, 155, 235)',
                        dayTextColor: 'rgb(97, 114, 136)',
                        textDisabledColor: 'rgba(157, 171, 194, 0.4)',
                        dotColor: 'rgb(255, 121, 121)',
                        selectedDotColor: 'rgb(255, 121, 121)',
                        arrowColor: 'rgb(255, 121, 121)',
                        monthTextColor: 'rgb(102, 120, 144)',
                        textDayFontFamily: 'Ubuntu',
                        textMonthFontFamily: 'Ubuntu',
                        textDayHeaderFontFamily: 'Ubuntu',
                        textDayFontSize: 15,
                        textMonthFontSize: 15,
                        textDayHeaderFontSize: 11
                      }}
                    markedDates={{
                        '2017-08-04': {selected: true, marked: true},
                        '2017-08-09': {marked: true},
                        '2017-08-12': {disabled: true},
                        [this.state.selected]: {selected: true}
                      }}
                />
                <ScrollView style={{flex:1, flexGrow:2}}>
                  <View style={{ ...styles.appointment, ...styles.appointmentMade }}>
                    <View style={styles.bullet} />
                    <View style={styles.bulletLine} />
                    <View style={styles.appointmentInfo}>
                      <View style={styles.appointmentHeader}>
                        <Text style={styles.appointmentTime}>09:00 - 10:30</Text>
                        <Text style={styles.appointmentTotalTime}>(1.5h)</Text>
                        <Image source={ReloadAppointment} />
                      </View>
                      <View style={styles.appointmentBody}>
                        <Text style={styles.appointmentText}>
                          Strength Training with
                          <Text style={styles.appointmentTextBlue}>Anna</Text>
                        </Text>
                      </View>
                      <View style={styles.appointmentFooter}>
                        <Image style={styles.appointmentLocationIcon} source={LocationIcon} />
                        <Text style={styles.appointmentTextBlue}>2551 Linn Ave Union, CA 07083</Text>
                      </View>
                    </View>
                    <View style={styles.appointmentImage}>
                      <Image source={AppointmentAvatar} />
                    </View>
                  </View>
                  <View style={styles.appointment}>
                    <View style={styles.bullet} />
                    <View style={styles.bulletLine} />
                    <View style={styles.appointmentInfo}>
                      <View style={styles.appointmentHeader}>
                        <Text style={styles.appointmentTime}>09:00 - 10:30</Text>
                        <Text style={styles.appointmentTotalTime}>(1.5h)</Text>
                        <Image source={ReloadAppointment} />
                      </View>
                      <View style={styles.appointmentBody}>
                        <Text style={styles.appointmentText}>
                          Strength Training with
                          <Text style={styles.appointmentTextBlue}>Anna</Text>
                        </Text>
                      </View>
                      <View style={styles.appointmentFooter}>
                        <Image style={styles.appointmentLocationIcon} source={LocationIcon} />
                        <Text style={styles.appointmentTextBlue}>2551 Linn Ave Union, CA 07083</Text>
                      </View>
                    </View>
                    <View style={styles.appointmentImageContainer}>
                      <Image style={styles.appointmentImage} source={AppointmentAvatar} />
                    </View>
                  </View>
                  <View style={styles.appointment}>
                    <View style={styles.bullet} />
                    <View style={styles.appointmentInfo}>
                      <View style={styles.appointmentHeader}>
                        <Text style={styles.appointmentTime}>09:00 - 10:30</Text>
                        <Text style={styles.appointmentTotalTime}>(1.5h)</Text>
                        <Image source={ReloadAppointment} />
                      </View>
                      <View style={styles.appointmentBody}>
                        <Text style={styles.appointmentText}>
                          Strength Training with
                          <Text style={styles.appointmentTextBlue}>Anna</Text>
                        </Text>
                      </View>
                      <View style={styles.appointmentFooter}>
                        <Image style={styles.appointmentLocationIcon} source={LocationIcon} />
                        <Text style={styles.appointmentTextBlue}>2551 Linn Ave Union, CA 07083</Text>
                      </View>
                    </View>
                    <View style={styles.appointmentImageContainer}>
                      <Image style={styles.appointmentImage} source={AppointmentAvatar} />
                    </View>
                  </View>
                </ScrollView>
              </View>
            </Tab>

          </Tabs>

          <FooterTabs style={{flex:1}}/>
        </Container>

    );
  }
}

const mapStateToProps = state => ({ user: state.user });

export default connect(mapStateToProps, null)(CalendarView);
