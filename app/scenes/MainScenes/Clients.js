import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
  Content,
  Text,
  List,
  ListItem,
  Left,
  Thumbnail,
  Body,
  Right,
  Container
} from 'native-base';
import { Image, ScrollView } from 'react-native';
import styles from '../../assets/styles/index';
import Clock from '../../assets/images/clock.png';
import Avatar from '../../assets/images/appointmentAvatar1.png';
import FooterTabs from './../../components/FooterTabs';
import { Actions } from 'react-native-router-flux';
import {getClientsInfo,getClientInfo} from  '../../store/client';
import {getTrainerInfo} from  '../../store/onboarding';
import Header from '../../components/Header';


class Clients extends Component {

    componentWillMount(){
        this.props.getTrainerInfo()
        this.props.getClientsInfo()
    }
    componentWillUnmount(){
        this.props.getClientsInfo()
    }
  render() {
    return (
      <Container>
          <Header
              buttonSearch={true}
              hideButton={true}
              buttonAdd={true}
              humburgerMenuVisible={true}
              headerWithLogo={true}/>
            <ScrollView style={{ ...styles.scrollview, ...styles.scrollviewFull, ...styles.scrollviewNopadding }}>
              <List style={styles.userList}>
                {this.props.clients.map((client,i) => {
                  return (
                      <ListItem avatar style={styles.userListItem} key={i} onPress={() => {this.props.getClientInfo(client.id);Actions.viewClient({client:client})}}>
                        <Left>
                          <Thumbnail source={Avatar} style={styles.userListAvatar} />
                        </Left>
                        <Body style={styles.userListItemBody}>
                        <Text style={styles.userListUsername}>{client.first_name + ' ' + client.last_name}</Text>
                        </Body>
                        <Right style={styles.userListItemRight}>
                          <Image source={Clock} />
                        </Right>
                      </ListItem>
                  )
                })}
              </List>
            </ScrollView>
            <FooterTabs />
      </Container>
    );
  }
}


const mapDispatchToProps = dispatch => bindActionCreators({
    getClientsInfo,
    getClientInfo,
    getTrainerInfo
}, dispatch);

const mapStateToProps = state => ({
  clients: state.client.clients
});

export default connect(mapStateToProps, mapDispatchToProps)(Clients);
