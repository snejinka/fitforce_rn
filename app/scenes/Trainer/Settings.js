import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
  Container,
  Content,
  Text,
  Form,
  Item,
  Input,
  Label,
  Icon,
  Separator,
  Button
} from 'native-base';
import { View, ScrollView,TouchableOpacity } from 'react-native';
import styles from '../../assets/styles/index';
import Header from '../../components/Header';
import {getTrainerInfo,editTrainerInfo,updateSubscription} from '../../store/onboarding';
import {changeAuth0UserData} from '../../store/user';
import {weightUnits,lengthUnits,currencies} from '../../constants'
import RadioButtonItem from '../../components/RadioButtonItem';
import { Dropdown } from 'react-native-material-dropdown';
import stripe from 'tipsi-stripe';


class Settings extends Component {
  constructor(props){
    super(props);
    this.state = {
      email: props.user.email,
      password: '123456',
      currency: props.onboarding.currency || 'USD',
      taxRate: props.onboarding.taxRate,
      weightUnit: props.onboarding.weightUnit,
      lengthUnit: props.onboarding.lengthUnit,
      initial:true,
      passwordInitial:true,
      stripeToken:''
    }
  }

  componentWillMount(){
    stripe.init({
      publishableKey: 'pk_test_ZCU4zPEbkl18CQQ65uNKMglB'
    })
  }

  handleSave(){
    const {email,currency,taxRate,weightUnit,lengthUnit,initial} = this.state;

    this.setState({
      initial:false
    })
    if(!email || !this.validateEmail(email) || !currency || !taxRate || !weightUnit || !lengthUnit){
      return;
    }
    this.props.editTrainerInfo(this.newTrainerData)
    if(this.props.user.email !== email){
      this.props.changeAuth0UserData({email})
    }
  }

  handleSavePassword(){
    const {password,passwordInitial,initial} = this.state;
    if(password.length < 6){
      return;
    }
    this.props.changeAuth0UserData({password})
  }

  validateEmail = (email) => {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
  };

  handleChangePayment(){
    const options = {
      smsAutofillDisabled: true
    };
    stripe.paymentRequestWithCardForm(options)
        .then(response => {
          if(response.tokenId){
            this.setState({
              stripeToken:response.tokenId,
            })
            this.props.updateSubscription(this.state.stripeToken,this.props.subscription_plan,this.props.max_clients_count)
          }
        })
        .catch(err => {
          console.warn(err)
        })
  }

  render() {
    const {email,password,currency,taxRate,weightUnit,lengthUnit,initial,passwordInitial} = this.state;
    this.newTrainerData = {currency,taxRate,weightUnit,lengthUnit}
    return (
      <Container>
        <Header title="Settings" rightButtonCallback={() => this.handleSave()}/>
        <Content>
          <ScrollView style={{ ...styles.scrollviewNopadding }}>
            <Separator bordered style={styles.separator}>
              <Text>{'Account'.toUpperCase()}</Text>
            </Separator>
            <Form style={styles.form}>
              <Item
                  error={!email}
                  style={initial || (email && this.validateEmail(email))
                      ? {...styles.inlineInput}
                      : {...styles.inlineInput, ...styles.inputInvalid}}
                  stackedLabel>
                <Label style={styles.label}>{'Update Email'.toUpperCase()}</Label>
                <Input
                    style={styles.input}
                    autoCorrect={false}
                    value={email}
                    onChangeText={(value) => this.setState({email:value})}/>
              </Item>
              <Item
                  error={!password || password.length < 6}
                  style={passwordInitial || password.length >= 6
                      ? {...styles.inlineInput}
                      : {...styles.inlineInput, ...styles.inputInvalid}}
                  stackedLabel>
                <Label style={styles.label}>{'Change Password'.toUpperCase()}</Label>
                <Input
                    style={styles.input}
                    autoCorrect={false}
                    secureTextEntry
                    value={password}
                    onChangeText={(value) => this.setState({password:value,passwordInitial:false})}/>
                {!this.state.passwordInitial &&
                  <TouchableOpacity style={{position:'absolute',right:0,top:20,padding:10}} onPress={() => this.handleSavePassword()}>
                    <Text style={[styles.text,{fontSize:14,color:'rgb(102, 120, 144)'}]}>Save</Text>
                  </TouchableOpacity>
                }

              </Item>
              <Item style={{ ...styles.inlineInput }} stackedLabel>
                <Label style={styles.label}>{'Main currency'.toUpperCase()}</Label>
                <View style={[styles.stackedInputIconContainer,{flex:1,alignSelf:'stretch'}]}>
                  <Dropdown
                      label=" "
                      value={this.state.currency}
                      onChangeText={(value) => this.setState({currency:value})}
                      inputContainerStyle={{borderBottomColor: 'transparent'}}
                      pickerStyle={{backgroundColor : "#fff",borderWidth:0}}
                      containerStyle={[styles.dropdown,{width:'auto',flex:1,borderWidth:0}]}
                      data={currencies}
                      renderAccessory={() => {return <Icon name="ios-arrow-down" style={{ ...styles.stackedInputIcon, ...styles.selectIcon }} />}}
                  />

                </View>
              </Item>
              <Item
                  error={!taxRate}
                  style={initial || (taxRate && taxRate.length >= 0)
                      ? {...styles.inlineInput}
                      : {...styles.inlineInput, ...styles.inputInvalid}}
                  stackedLabel>
                <Label style={styles.label}>{'Tax Rate (%)'.toUpperCase()}</Label>
                <Input
                    style={styles.input}
                    value={taxRate}
                    onChangeText={(value) => this.setState({taxRate:value})}/>
              </Item>
              <Button
                  style={[styles.btnPrimary,{alignSelf:'stretch',marginTop:20}]} onPress={() => this.handleChangePayment()}>
                <Text style={[styles.btnPrimaryText]}>{'Change payment method'.toUpperCase()}</Text>
              </Button>

              <Separator bordered style={{...styles.separator, ...styles.separatorInForm}}>
                <Text>{'Units'.toUpperCase()}</Text>
              </Separator>
              <Label style={{...styles.label, ...styles.labelCheckbox}}>{ 'KG or lbs'.toUpperCase()}</Label>
              {weightUnits.map((unit) => {
                return (
                    <RadioButtonItem
                        key={unit}
                        textStyle={styles.checkboxText}
                        text={unit}
                        selected={weightUnit === unit}
                        handleClick={() => this.setState({weightUnit:unit})}
                        listItemStyle={[styles.noBorderBottom]}/>
                )
              })}
              <Label style={{...styles.label, ...styles.labelCheckbox}}>{'cm or inch'.toUpperCase()}</Label>
              {lengthUnits.map((unit) => {
                return (
                    <RadioButtonItem
                        key={unit}
                        textStyle={styles.checkboxText}
                        text={unit}
                        selected={lengthUnit === unit}
                        handleClick={() => this.setState({lengthUnit:unit})}
                        listItemStyle={[styles.noBorderBottom]}/>
                )
              })}
            </Form>
          </ScrollView>
        </Content>
      </Container>
    );
  }
}

const mapDispatchToProps = dispatch => bindActionCreators({
  getTrainerInfo,
  editTrainerInfo,
  changeAuth0UserData,
  updateSubscription
}, dispatch);

const mapStateToProps = state => ({
  user: state.user,
  onboarding: state.onboarding,
  idToken: state.user.idToken,
  subscription_plan: state.onboarding.subscription_plan,
  max_clients_count: state.onboarding.max_clients_count,
});

export default connect(mapStateToProps, mapDispatchToProps)(Settings);
