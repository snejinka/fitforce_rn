import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
  Container,
  Content,
  Text,
  Form,
  Item,
  Input,
  Label,
  Icon,
  Separator,
  ListItem,
  CheckBox,
  Body,
} from 'native-base';
import { Image, View, ScrollView, TextInput } from 'react-native';
import styles from '../../assets/styles/index';
import AvatarDefault from '../../assets/images/avatarDefault.png';
import Header from '../../components/Header';
import {getTrainerInfo,editTrainerInfo} from '../../store/onboarding';
import {changeUserData} from '../../store/user';
import {servicesConstant} from '../../constants';
import CheckboxItem from '../../components/CheckboxItem';
import {TextInputMask} from 'react-native-masked-text';


class MyProfile extends Component {
  constructor(props){
    super(props);
    this.state = {
      firstName: props.user.firstName,
      lastName: props.user.lastName,
      phone: props.onboarding.phone,
      address: props.onboarding.address,
      services: props.onboarding.services,
      aboutMe:props.onboarding.aboutMe,
      initial:true
    }
    this.newTrainerData = {};
    this.handleSave = this.handleSave.bind(this);
  }

  handleSave(){
    const {firstName,lastName, phone} = this.state;
    this.setState({
      initial:false
    })
    if(!firstName || !lastName || (phone && phone.length < 15)){
      return;
    }
    this.props.editTrainerInfo(this.newTrainerData)
  }

  handleClickCheckbox(service){
    const { services } = this.state;
    const serviceIndex = services.indexOf(service);
    if (serviceIndex > -1) {
      services.splice(serviceIndex, 1);
    } else {
      services.push(service);
    }
  }

  render() {
    const {firstName,lastName,phone,address,services,aboutMe,initial} = this.state;
    this.newTrainerData = {firstName,lastName,phone,address,services,aboutMe}
    return (
      <Container>
        <Header title="My Profile" rightButtonCallback={this.handleSave}/>
        <Content>
          <ScrollView style={{ ...styles.scrollviewNopadding }}>
            <View style={styles.uploadPhotoContainer}>
              <Image style={styles.uploadPhotoImage} source={AvatarDefault} />
              <Text style={styles.uploadPhotoText}>Change Photo</Text>
            </View>
            <Separator bordered style={styles.separator}>
              <Text>{'Profile'.toUpperCase()}</Text>
            </Separator>
            <Form style={styles.form}>
              <View style={styles.formRow}>
                <Item
                    error={!firstName}
                    style={initial || firstName
                      ? {...styles.inlineInput,...styles.inlineInputMargin, flex:1}
                      : {...styles.inlineInput, flex:1,...styles.inlineInputMargin, ...styles.inputInvalid}}
                    stackedLabel>
                  <Label style={styles.label}>{'First Name'.toUpperCase()}</Label>
                  <Input
                      style={styles.input}
                      onChangeText={(firstName) => this.setState({firstName})}
                      value={firstName}/>
                </Item>
                <Item
                    error={!lastName}
                    style={initial || lastName
                      ? {...styles.inlineInput, flex:1}
                      : {...styles.inlineInput, flex:1, ...styles.inputInvalid}}
                    stackedLabel>
                  <Label style={styles.label}>{'Last Name'.toUpperCase()}</Label>
                  <Input
                      style={styles.input}
                      onChangeText={(lastName) => this.setState({lastName})}
                      value={lastName}/>
                </Item>
              </View>
              <Item
                  error={phone && phone.length < 15}
                  style={initial || !phone || (phone && phone.length == 15)
                      ? {...styles.inlineInput, flex:1}
                      : {...styles.inlineInput, flex:1, ...styles.inputInvalid}}
                  stackedLabel>
                <Label style={styles.label}>{'Phone Number'.toUpperCase()}</Label>
                <View style={styles.stackedInputIconContainer}>
                  <TextInputMask
                      style={{...styles.input,flex:1}}
                      value={phone}
                      type={'custom'}
                      onChangeText={(phone) => this.setState({phone:phone})}
                      options={{
					    mask: '+9 999-999-9999'
                      }} />
                  <Icon style={styles.stackedInputIcon} active name="ios-call" />
                  <Icon style={styles.stackedInputIcon} active name="ios-text" />
                </View>
              </Item>
              <Item style={{ ...styles.inlineInput }} stackedLabel>
                <Label style={styles.label}>{'Address'.toUpperCase()}</Label>
                <View style={styles.stackedInputIconContainer}>
                  <Input
                      style={styles.input}
                      onChangeText={(address) => this.setState({address})}
                      value={address} />
                  <Icon style={styles.stackedInputIcon} active name="ios-pin" />
                </View>
              </Item>
              <Item style={{ ...styles.inlineInput, ...styles.noBorderBottom }} stackedLabel>
                <Label style={{ ...styles.label, ...styles.labelMarginBottom }}>{'Services'.toUpperCase()}</Label>
              </Item>
              {servicesConstant.map((service) => {
                return (
                    <CheckboxItem
                        key={service}
                        textStyle={styles.checkboxText}
                        text={service}
                        checked={services.includes(service)}
                        textStyle={{ color: 'rgb(97, 114, 136)', fontFamily: 'Ubuntu', fontSize: 17 }}
                        handleClick={() => this.handleClickCheckbox(service)}
                        listItemStyle={[styles.noBorderBottom, styles.noMarginLeft]}/>
                )
              })}
              
              <Item style={{ ...styles.inlineInput, ...styles.labelMarginBottom }} stackedLabel>
                <Label style={{ ...styles.label, ...styles.labelMarginBottom }}>{'About me'.toUpperCase()}</Label>
                <TextInput
                  style={[styles.input,styles.inputMultiline,{alignSelf:'stretch'} ]}
                  placeholder="About me..."
                  multiline
                  numberOfLines={6}
                  onChangeText={aboutMe => this.setState({ aboutMe })}
                  value={aboutMe}
                />
              </Item>
            </Form>
          </ScrollView>
        </Content>
      </Container>
    );
  }
}
const mapDispatchToProps = dispatch => bindActionCreators({
  getTrainerInfo,
  editTrainerInfo,
  changeUserData
}, dispatch);

const mapStateToProps = state => ({
  user: state.user,
  onboarding: state.onboarding,
  idToken: state.user.idToken,
});

export default connect(mapStateToProps, mapDispatchToProps)(MyProfile);