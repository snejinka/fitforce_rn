const styles = {
  welcomeForm: {
    marginTop: 155,
  },
  mainForm: {
    marginTop: 155,
  },
  signupFinishForm: {
    marginTop: 115,
  },
  signupForm: {
    marginTop: 155,
  },
  signinForm: {
    marginTop: 155,
  },
  passwordForm: {
    marginTop: 190,
  },
  formButton: {
    marginTop: 20,
  },
  textInput: {
    marginTop: 10,
    marginBottom: 10,
    flex: 1,
  },
  mainFormTitle: {
    marginTop: 20,
  },
  formButtonText: {
    color: '#617288',
    fontFamily: 'Ubuntu-Medium',
    fontSize: 16,
  },
  forgotPassBtn: {
    marginLeft: 'auto',
    marginRight: 'auto',
  },
  forgotPassBtnText: {
    opacity: 0.9,
    fontSize: 15,
    marginTop: 5,
  },
  signupLink: {
    paddingLeft: 5,
    justifyContent: 'center',
    paddingBottom: 0,
  },
  footerMessageContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  loginMessageContainer: {
    marginTop: 5,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  loginLink: {
    paddingTop: 0,
    paddingLeft: 5,
    paddingBottom: 0,
    shadowColor: 'rgba(0, 0, 0, 0.3)',
  },
  divideText: {
    textAlign: 'center',
    marginTop: 9,
    marginBottom: 9,
  },
  socialBtn: {
    height: 40,
    justifyContent: 'flex-start',
  },
  socialBtnText: {
    fontFamily: 'Ubuntu-Medium',
  },
  facebookBtn: {
    marginTop: 30,
    backgroundColor: 'rgb(59, 87, 157)',
  },
  googleBtn: {
    marginTop: 13,
    backgroundColor: 'rgb(220, 78, 65)',
  },
  emailBtnText: {
    color: 'rgb(97, 114, 136)',
  },
  title: {
    marginBottom: 39,
  },
  listRadio: {
    marginTop: 28,
    marginLeft: 'auto',
    marginRight: 'auto',
  },
};

export default styles;
