import React, { Component } from 'react';
import {
  Content,
  Form,
  Item,
  Button,
  Text,
} from 'native-base';
import { Image, TextInput } from 'react-native';
import { Actions } from 'react-native-router-flux';
import styles from '../assets/styles/index';
import appStyles, { combineStyles } from '../../../assets/styles/index';
import SignupFooter from './SignupFooter';

const bgImage = require('../assets/images/bg.png');

class Signup extends Component {
  constructor(props) {
    super(props);
    this.state = {
      firstName: '',
      lastName: '',
      initial: true,
    };
  }

  nextStep() {
    const { firstName, lastName } = this.state;

    this.setState({ initial: false });
    if (!firstName || !lastName) {
      return;
    }

    Actions.signupFinish({ firstName, lastName });
  }

  changeFocus = () => {
    this._lastNameInput.focus();
  }

  render() {
    const { initial, firstName, lastName } = this.state;
    return (
      <Image source={bgImage}  style={appStyles.responsiveContainer}>
        <Content style={appStyles.content}>
          <Text
            style={combineStyles(
              appStyles.text,
              appStyles.title,
              styles.mainFormTitle)}
          >
            Sign Up
          </Text>
          <Form style={styles.signupForm}>
            <Item
              error={!firstName}
              style={initial || firstName
              ? appStyles.itemFirst
              : appStyles.itemFirstInvalid}
            >
              <TextInput
                placeholder="First Name"
                placeholderTextColor="white"
                style={combineStyles(appStyles.text, styles.textInput)}
                onChangeText={newFirstName => this.setState({ firstName: newFirstName })}
                onSubmitEditing={this.changeFocus}
                returnKeyType="next"
                autoCorrect={false}
              />
            </Item>
            <Item
              error={!lastName}
              style={initial || lastName
              ? appStyles.item
              : appStyles.itemInvalid}
            >
              <TextInput
                placeholder="Last Name"
                placeholderTextColor="white"
                ref={(c) => { this._lastNameInput = c; }}
                style={combineStyles(appStyles.text, styles.textInput)}
                onChangeText={newLastName => this.setState({ lastName: newLastName })}
                onSubmitEditing={() => this.nextStep()}
                returnKeyType="go"
                autoCorrect={false}
              />
            </Item>
            <Button style={styles.formButton} full light onPress={() => this.nextStep()}>
              <Text style={styles.formButtonText}>Next</Text>
            </Button>
          </Form>

        </Content>
        <SignupFooter step="first" />
      </Image>
    );
  }
}

export default Signup;
