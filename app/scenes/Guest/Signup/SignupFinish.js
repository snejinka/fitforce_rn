import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
  Content,
  Form,
  Item,
  Button,
  Text,
} from 'native-base';
import { Image, TextInput } from 'react-native';
import { signUp } from '../../../store/user';
import styles from '../assets/styles/index';
import appStyles, { combineStyles } from '../../../assets/styles/index';
import SignupFooter from './SignupFooter';
import ErrorModal from '../../../utils/ErrorModal';
import { Actions } from 'react-native-router-flux';

const bgImage = require('../assets/images/bg.png');

class SignupFinish extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: '',
      passwordConfirm: '',
      initial: true,
    };
  }

  signUp() {
    const { email, password, passwordConfirm } = this.state;
    const { firstName, lastName } = this.props;

    this.setState({ initial: false });
    if (!email || !password || !passwordConfirm) {
      return;
    }

    if (password !== passwordConfirm) {
      ErrorModal.show('Password doesn\'t match password confirmation.');
      return;
    }

    this.props.actions.signUp({ email, password, firstName, lastName }).then(() => {
      Actions.onboardingStart({email, password})
    }).catch(err => {
      ErrorModal.show(err.message);

    });
  }

  changeFocus = () => {
    if (this._passwordInput.isFocused()) {
      this._passwordConfirmInput.focus();
    }
    if (this._emailInput.isFocused()) {
      this._passwordInput.focus();
    }
  }

  render() {
    const { initial, email, password, passwordConfirm } = this.state;

    return (
      <Image source={bgImage}  style={appStyles.responsiveContainer}>
        <Content style={appStyles.content}>
          <Text
            style={combineStyles(
            appStyles.text,
            appStyles.title,
            styles.mainFormTitle)}
          >
          Sign Up
        </Text>

          <Form style={styles.signupFinishForm}>
            <Item
              error={!email}
              style={initial || email ? appStyles.itemFirst : appStyles.itemFirstInvalid}
            >
              <TextInput
                placeholder="Email"
                placeholderTextColor="white"
                autoCapitalize="none"
                keyboardType="email-address"
                style={combineStyles(appStyles.text, styles.textInput)}
                ref={(c) => { this._emailInput = c; }}
                returnKeyType="next"
                autoCorrect={false}
                onChangeText={newEmail => this.setState({ email: newEmail })}
                onSubmitEditing={this.changeFocus}
              />
            </Item>
            <Item
              error={!password}
              style={initial || password ? appStyles.item : appStyles.itemInvalid}
            >
              <TextInput
                placeholder="Password"
                placeholderTextColor="white"
                secureTextEntry
                style={combineStyles(appStyles.text, styles.textInput)}
                ref={(c) => { this._passwordInput = c; }}
                returnKeyType="next"
                autoCorrect={false}
                onChangeText={newPassword => this.setState({ password: newPassword })}
                onSubmitEditing={this.changeFocus}
              />
            </Item>
            <Item
              error={!passwordConfirm}
              style={initial || passwordConfirm ? appStyles.item : appStyles.itemInvalid}
            >
              <TextInput
                placeholder="Password Confirmation"
                placeholderTextColor="white"
                secureTextEntry
                returnKeyType="go"
                autoCorrect={false}
                style={combineStyles(appStyles.text, styles.textInput)}
                ref={(c) => { this._passwordConfirmInput = c; }}
                onChangeText={
                  newPasswordConfirm => this.setState({ passwordConfirm: newPasswordConfirm })
                }
                onSubmitEditing={() => this.signUp()}
              />
            </Item>
            <Button
              full
              light
              style={styles.formButton}
              onPress={() => this.signUp()}
            >
              <Text style={styles.formButtonText}>Sign Up</Text>
            </Button>
          </Form>

        </Content>
        <SignupFooter step="last" />
      </Image>
    );
  }
}

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators({
    signUp,
  }, dispatch),
});

export default connect(null, mapDispatchToProps)(SignupFinish);
