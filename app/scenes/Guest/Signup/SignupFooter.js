import React from 'react';
import { Form, Button, Text, Footer } from 'native-base';
import { Image } from 'react-native';
import { Actions } from 'react-native-router-flux';
import styles from '../assets/styles/index';
import appStyles, { combineStyles } from '../../../assets/styles/index';

const listRadioFirst = require('../assets/images/list-radio-first.png');
const listRadioLast = require('../assets/images/list-radio-last.png');

const SignupFooter = props => (
  <Footer style={combineStyles(appStyles.transparent, { height: 100 })}>
    <Form>
      <Form style={styles.footerMessageContainer}>
        <Text style={combineStyles(appStyles.text, appStyles.appOpacity)}>
          Already have an account?
        </Text>
        <Button
          onPress={Actions.signin}
          style={styles.loginLink}
          transparent
          light
        >
          <Text style={combineStyles(appStyles.boldText, appStyles.appOpacity)}>Sign In</Text>
        </Button>
      </Form>
      <Image
        style={styles.listRadio}
        source={props.step === 'first' ? listRadioFirst : listRadioLast}
      />
    </Form>
  </Footer>
);

export default SignupFooter;
