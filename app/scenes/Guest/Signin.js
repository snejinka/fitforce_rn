import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
  Content,
  Form,
  Item,
  Input,
  Button,
  Text,
  Footer,
} from 'native-base';
import { Image, TextInput } from 'react-native';
import { Actions } from 'react-native-router-flux';
import ErrorModal from '../../utils/ErrorModal';
import { signIn,setSession } from '../../store/user';
import { getTrainerInfo, } from '../../store/onboarding';
import { getClientsInfo } from '../../store/client';
import styles from './assets/styles/index';
import appStyles, { combineStyles } from '../../assets/styles/index';

const bgImage = require('./assets/images/bg.png');

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: '',
      initial: true,
    };
  }

  componentDidMount() {
    if(this.props.passwordReset){
      ErrorModal.show('You should receive an email about resetting your password shortly. Please follow the instructions and sign in with your new password.');
    }

  }

  signIn() {
    const { email, password } = this.state;

    this.setState({ initial: false });
    if (!email || !password) {
      return;
    }
    this.props.signIn(email, password).then(() => {
      this.props.getClientsInfo()
      this.props.getTrainerInfo()
          .then(() => {
        if(!this.props.success_last_payment){
          this.props.setSession('blocked');
        }
      })
    })
  }

  changeFocus = () => {
    this._passwordInput.focus();
  }

  render() {
    const { initial, email, password } = this.state;
    return (
      <Image source={bgImage}  style={appStyles.responsiveContainer}>
        <Content style={appStyles.content}>
          <Text
            style={combineStyles(
              appStyles.text,
              appStyles.title,
              styles.mainFormTitle)}
          >
            Sign In
          </Text>
          <Form style={styles.signinForm}>
            <Item
              error={!email}
              style={initial || email
              ? appStyles.itemFirst
              : appStyles.itemFirstInvalid}
            >
              <TextInput
                placeholder="Email"
                placeholderTextColor="white"
                autoCapitalize="none"
                keyboardType="email-address"
                style={combineStyles(appStyles.text, styles.textInput)}
                returnKeyType="next"
                autoCorrect={false}
                onChangeText={newEmail => this.setState({ email: newEmail })}
                onSubmitEditing={this.changeFocus}
              />
            </Item>
            <Item
              error={!password}
              style={initial || password
              ? appStyles.item
              : appStyles.itemInvalid}
            >
              <TextInput
                placeholder="Password"
                placeholderTextColor="white"
                secureTextEntry
                style={combineStyles(appStyles.text, styles.textInput)}
                ref={(c) => { this._passwordInput = c; }}
                returnKeyType="go"
                autoCorrect={false}
                onChangeText={newPassword => this.setState({ password: newPassword })}
                onSubmitEditing={() => this.signIn()}
              />
            </Item>
            <Button style={styles.formButton} full light onPress={() => this.signIn()}>
              <Text style={styles.formButtonText}>Sign In</Text>
            </Button>
          </Form>
          <Button style={styles.forgotPassBtn} transparent light onPress={Actions.forgotPassword}>
            <Text style={styles.forgotPassBtnText}>Forgot Password?</Text>
          </Button>
        </Content>

        <Footer style={appStyles.transparent}>
          <Form style={styles.footerMessageContainer}>
            <Text style={combineStyles(appStyles.text, appStyles.appOpacity)}>
              Don’t have an account?
            </Text>
            <Button style={styles.signupLink} transparent light onPress={Actions.signup}>
              <Text style={combineStyles(appStyles.boldText, appStyles.appOpacity)}>Sign Up</Text>
            </Button>
          </Form>
        </Footer>
      </Image>
    );
  }
}

const mapDispatchToProps = dispatch => bindActionCreators({
  signIn,
  getClientsInfo,
  getTrainerInfo,
  setSession
}, dispatch);

const mapStateToProps = state => ({
  user: state.user,
  success_last_payment: state.onboarding.success_last_payment

});

export default connect(mapStateToProps, mapDispatchToProps)(Login);
