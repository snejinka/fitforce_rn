import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
  Content,
  Form,
  Item,
  Button,
  Text,
  Footer,
} from 'native-base';
import { Image, TextInput } from 'react-native';
import { resetPassword } from '../../store/user';
import { Actions } from 'react-native-router-flux';
import styles from './assets/styles/index';
import appStyles, { combineStyles } from '../../assets/styles/index';

const bgImage = require('./assets/images/bg.png');

class ForgotPassword extends Component {
  
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      initial: true
    };
  }
  

  resetPassword() {

    const { email } = this.state;

    this.setState({ initial: false });

    if (!email) {
      return;
    }

    this.props.resetPassword(email).then(Actions.signin({ passwordReset: true }))
  }

  render() {
    const { email, initial } = this.state;
    return(
      <Image source={bgImage}>
        <Content style={appStyles.content}>
          <Text
            style={combineStyles(
              appStyles.text,
              appStyles.title,
              styles.mainFormTitle)}
          >
          Reset Your Password
        </Text>
          <Form style={styles.passwordForm}>
            <Item
              error={!email}
              style={initial || email
              ? appStyles.itemFirst
              : appStyles.itemFirstInvalid}
            >
              <TextInput
                placeholder="Email"
                placeholderTextColor="white"
                autoCapitalize="none"
                keyboardType="email-address"
                style={combineStyles(appStyles.text, styles.textInput)}
                returnKeyType="go"
                autoCorrect={false}
                onChangeText={newEmail => this.setState({ email: newEmail })}
                onSubmitEditing={() => this.resetPassword()}
              />
            </Item>
            <Button style={styles.formButton} full light onPress={() => this.resetPassword()}>
              <Text style={styles.formButtonText}>Submit</Text>
            </Button>
          </Form>
        </Content>
      </Image>
    );
  }
}

const mapDispatchToProps = dispatch => bindActionCreators({
  resetPassword,
}, dispatch);

export default connect(null, mapDispatchToProps)(ForgotPassword);
