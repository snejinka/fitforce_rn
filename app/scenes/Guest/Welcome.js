import React, { Component } from 'react';
import {
  Content,
  Form,
  Button,
  Text,
  Icon,
} from 'native-base';
import { Image } from 'react-native';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Actions } from 'react-native-router-flux';
import styles from './assets/styles/index';
import appStyles, { combineStyles } from '../../assets/styles/index';
import LogoImage from '../../components/LogoImage';
import { socialLogin } from '../../store/user';

const bgImage = require('./assets/images/bg.png');

class Welcome extends Component {
  socialLogin(connectionName) {
    this.props.actions.socialLogin(connectionName);
  }

  render() {
    return (
      <Image source={bgImage} style={appStyles.responsiveContainer}>
        <Content style={appStyles.content}>
          <LogoImage />

          <Form style={styles.mainForm}>

            <Button
              full
              iconLeft
              style={combineStyles(styles.facebookBtn, styles.socialBtn)}
              onPress={() => this.socialLogin('facebook')}
            >
              <Icon name="logo-facebook" />
              <Text style={styles.socialBtnText}>
                Continue with Facebook
              </Text>
            </Button>

            <Button
              full
              iconLeft
              style={combineStyles(styles.googleBtn, styles.socialBtn)}
              onPress={() => this.socialLogin('google-oauth2')}
            >
              <Icon name="logo-google" />
              <Text style={styles.socialBtnText}>
                Continue with Google
              </Text>
            </Button>

            <Text style={combineStyles(appStyles.text, styles.divideText)}>OR</Text>

            <Button
              full
              iconLeft
              light
              style={styles.socialBtn}
              onPress={Actions.signup}
            >
              <Icon style={styles.emailBtnText} name="md-mail" />
              <Text style={combineStyles(styles.socialBtnText, styles.emailBtnText)}>
                Sign up with email
              </Text>
            </Button>
          </Form>

          <Form style={styles.loginMessageContainer}>
            <Text style={appStyles.text}>
              Already have an account?
            </Text>
            <Button
              onPress={Actions.signin}
              style={styles.loginLink}
              transparent
              light
            >
              <Text style={appStyles.boldText}>Login</Text>
            </Button>
          </Form>
        </Content>
      </Image>
    );
  }
}

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators({
    socialLogin,
  }, dispatch),
});

const mapStateToProps = state => ({ user: state.user });

export default connect(mapStateToProps, mapDispatchToProps)(Welcome);
