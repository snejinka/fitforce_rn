import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
  Content,
  Form,
  Text,
  Item,
  Input,
  Icon,
} from 'native-base';
import ModalPicker from 'react-native-modal-picker';
import { Actions } from 'react-native-router-flux';
import styles from './assets/styles/index';
import appStyles, { combineStyles } from '../../assets/styles/index';
import { setField,editTrainerInfo,createTrainer } from '../../store/onboarding';
import { updateSelf, signIn } from '../../store/user';
import OnboardingFooter from './OnboardingFooter';
import LogoImage from '../../components/LogoImage';

class CurrencyStep extends Component {
  constructor(props){
    super(props);
    this.onboardingData = {};
  }
  completeOnboarding() {
    this.props.actions.signIn(this.props.onboarding.tempEmail,this.props.onboarding.tempPassword).then(() => {
      this.props.actions.setField('tempEmail','');
      this.props.actions.setField('tempPassword','');
      this.props.actions.createTrainer(this.onboardingData)
    })
  }

  render() {
    const { currency, taxRate,phone,address,services,lengthUnit,weightUnit } = this.props.onboarding;
    this.onboardingData = {currency, taxRate,phone,address,services,lengthUnit,weightUnit}
    const { actions } = this.props;
    const data = [
      { key: 0, label: 'USD' },
      { key: 1, label: 'CAD' },
      { key: 2, label: 'EUR' },
    ];

    return (
      <Form style={styles.container}>
        <Content style={appStyles.content}>
          <LogoImage style={appStyles.logoImage} />

          <Form style={styles.mainForm}>
            <Text style={combineStyles(appStyles.text, appStyles.subTitle)}>
            Will you be invoicing your clients using FitForce.com for Trainers?
            If yes, please set the default currency and tax rate.
          </Text>


            <ModalPicker
              data={data}
              initValue="Select one"
              onChange={option => actions.setField('currency', option.label)}
            >
              <Form style={styles.pickerForm}>
                <Form style={styles.leftPicketForm}>
                  <Text style={appStyles.text}>{currency || 'USD'}</Text>
                </Form>

                <Form style={styles.rightPickerForm}>
                  <Icon
                    ios="ios-arrow-down"
                    android="md-arrow-down"
                    style={{ color: 'white' }}
                  />
                </Form>
              </Form>
            </ModalPicker>


            <Item
              style={combineStyles(appStyles.item, styles.taxInput)}
            >
              <Input
                keyboardType="numeric"
                style={appStyles.text}
                placeholderTextColor="white"
                placeholder="Tax Rate (%)"
                onChangeText={newTaxRate => actions.setField('taxRate', newTaxRate)}
                value={taxRate}
              />
            </Item>
          </Form>
        </Content>
        <OnboardingFooter step={4} clickAction={() => this.completeOnboarding()} />
      </Form>
    );
  }
}

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators({
    updateSelf,
    setField,
    signIn,
    editTrainerInfo,
    createTrainer
  }, dispatch),
});

const mapStateToProps = state => ({
  onboarding: state.onboarding,
  idToken: state.user.idToken,
  userId: state.user.id
});

export default connect(mapStateToProps, mapDispatchToProps)(CurrencyStep);
