const styles = {
  container: {
    height: '100%',
    backgroundColor: 'rgb(79, 155,235)',
  },
  content: {
    width: '80%',
    marginLeft: 'auto',
    marginRight: 'auto',
    marginTop: 35,
  },
  mainForm: {
    marginTop: 105,
  },
  addressForm: {
    marginTop: 147,
  },
  inputIcon: {
    color: 'white',
    fontSize: 24,
  },
  startBtn: {
    marginTop: 37,
  },
  startBtnText: {
    fontFamily: 'Ubuntu-Medium',
    color: 'rgb(79, 155, 235)',
  },
  footerBtnText: {
    fontFamily: 'Ubuntu-Medium',
    color: 'rgb(79, 155, 235)',
  },
  footerForm: {
    width: '70%',
    marginLeft: 'auto',
    marginRight: 'auto',
  },
  footerImg: {
    marginTop: 27,
    marginLeft: 'auto',
    marginRight: 'auto',
  },
  addressItemFirst: {
    marginTop: 10,
  },
  listItem: {
    borderBottomColor: 'transparent',
    marginTop: 0,
    marginBottom: 0,
    paddingTop: 10,
    paddingBottom: 10,
  },
  servicesList: {
    marginTop: 20,
  },
  pickerForm: {
    borderBottomColor: 'rgba(255, 255, 255, 0.2)',
    borderBottomWidth: 2,
    marginLeft: 0,
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  leftPicketForm: {
    marginLeft: 0,
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-start',
  },
  rightPicketForm: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-end',
  },
  taxInput: {
    borderBottomColor: 'rgba(255, 255, 255, 0.2)',
    borderBottomWidth: 2,
  },
  unitsForm: {
    marginTop: 141,
  },
  unitsContentItemForm: {
    flex: 1,
    flexDirection: 'row',
    marginTop: 21,
    alignItems: 'center',
    justifyContent: 'center',
  },
  switcher: {
    flex: 1,
    flexDirection: 'row',
    resizeMode: 'contain',
  },
  switcherLabel: {
    fontSize: 11,
    width: '50%',
    paddingLeft: '10%',
  },
  switcherItem: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
  },
  switcherItemLeft: {
    justifyContent: 'center',
  },
  switcherItemRight: {
    justifyContent: 'center',
  },
  switcherButton: {
    height: 30,
  },
  switcherItemActive: {
    resizeMode: 'contain',
  },
  switcherTextActive: {
    color: 'rgb(79, 155, 235)',
  },
  googlePlaces: {
    textInputContainer: {
      height: 50,
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: 'transparent',
    },
    description: {
      color: 'white',
    },
    textInput: {
      backgroundColor: 'transparent',
      fontFamily: 'Ubuntu',
      fontSize: 17,
      paddingLeft: 0,
      paddingTop: 0,
      paddingRight: 0,
      paddingBottom: 0,
      color: 'white',
    },
    poweredContainer: {
      backgroundColor: 'transparent',
    },
  },
};

export default styles;
