import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
  Content,
  Form,
  Text,
  List,
  ListItem,
} from 'native-base';
import { Image } from 'react-native';
import CheckBox from 'react-native-check-box';
import { Actions } from 'react-native-router-flux';
import { setField } from '../../store/onboarding';
import styles from './assets/styles/index';
import appStyles, { combineStyles } from '../../assets/styles/index';
import OnboardingFooter from './OnboardingFooter';
import LogoImage from '../../components/LogoImage';

const checkedIcon = require('./assets/images/checked.png');
const uncheckedIcon = require('./assets/images/unchecked.png');

class AddressStep extends Component {
  setField(serviceName) {
    const { services } = this.props.onboarding;
    const serviceIndex = services.indexOf(serviceName);
    if (serviceIndex > -1) {
      services.splice(serviceIndex, 1);
    } else {
      services.push(serviceName);
    }

    this.props.actions.setField(services);
  }

  render() {
    const { services } = this.props.onboarding;
    const servicesArray = ['Health and fitness training', 'Yoga and Pilates', 'Diet and nutrition', 'Physiotherapy', 'Other'];

    return (
      <Form style={styles.container}>
        <Content style={styles.content}>
          <LogoImage style={appStyles.logoImage} />

          <Form style={styles.mainForm}>
            <Text style={combineStyles(appStyles.text, appStyles.subTitle)}>
              What services will you be providing?
            </Text>
          </Form>

          <List style={styles.servicesList}>
            {servicesArray.map((serviceName) => {
              const checked = services.includes(serviceName);
              return (
                <ListItem
                  key={serviceName}
                  style={styles.listItem}
                >
                  <CheckBox
                    style={{ flex: 1 }}
                    onClick={() => this.setField(serviceName)}
                    isChecked={checked}
                    rightText={serviceName}
                    rightTextStyle={appStyles.thinText}
                    checkedImage={<Image source={checkedIcon} />}
                    unCheckedImage={<Image source={uncheckedIcon} />}
                  />
                </ListItem>
              );
            },
        )}
          </List>
        </Content>
        <OnboardingFooter step={2} clickAction={Actions.unitsStep} />
      </Form>
    );
  }
}

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators({
    setField,
  }, dispatch),
});

const mapStateToProps = state => ({ onboarding: state.onboarding });

export default connect(mapStateToProps, mapDispatchToProps)(AddressStep);
