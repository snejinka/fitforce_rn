import React from 'react';
import { Form, Button, Text, Footer } from 'native-base';
import { Image } from 'react-native';
import styles from './assets/styles/index';
import appStyles, { combineStyles } from '../../assets/styles/index';

/* eslint-disable global-require*/
const stepsImages = {
  1: require('./assets/images/steps-1.png'),
  2: require('./assets/images/steps-2.png'),
  3: require('./assets/images/steps-3.png'),
  4: require('./assets/images/steps-4.png'),
};
/* eslint-enable global-require*/

const SignupFooter = props => (
  <Footer style={combineStyles(appStyles.transparent, { height: 110 })}>
    <Form style={styles.footerForm}>
      <Form>
        <Button full rounded light style={styles.footerBtn} onPress={props.clickAction}>
          <Text style={styles.footerBtnText}>
            {props.step === 4 ? 'Done' : 'Next' }
          </Text>
        </Button>
      </Form>
      <Image
        style={styles.footerImg}
        source={stepsImages[props.step]}
      />
    </Form>
  </Footer>
);

export default SignupFooter;
