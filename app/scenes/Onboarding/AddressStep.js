import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
  Content,
  Form,
  Text,
  Item,
  Input,
  Icon,
} from 'native-base';
import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete';
import { Actions } from 'react-native-router-flux';
import { setField } from '../../store/onboarding';
import styles from './assets/styles/index';
import appStyles, { combineStyles } from '../../assets/styles/index';
import OnboardingFooter from './OnboardingFooter';
import LogoImage from '../../components/LogoImage';
import {TextInputMask} from 'react-native-masked-text';


class AddressStep extends React.Component{

  handleNext(){
    const { phone } = this.props.onboarding;
    if(phone && phone.length < 15){
      return;
    }
    Actions.serviceStep()
  }
  render(){
    const { phone } = this.props.onboarding;
    return (
        <Form style={styles.container}>
          <Content style={appStyles.content}>
            <LogoImage style={appStyles.logoImage} />

            <Form style={styles.addressForm}>
              <Text style={combineStyles(appStyles.text, appStyles.subTitle)}>
                Please complete your contact info
              </Text>

              <Item
                  error={phone && phone.length < 15}
                  style={!phone || (phone && phone.length == 15)
                      ? {...appStyles.itemFirst,...styles.addressItemFirst, flex:1}
                      : [appStyles.itemFirst,styles.addressItemFirst,{flex:1,borderBottomWidth:2,borderColor:'red'}]}
              >
                <TextInputMask
                    placeholderTextColor="white"
                    style={{...styles.input,flex:1,color:'#fff'}}
                    value={phone}
                    type={'custom'}
                    onChangeText={newPhone => this.props.actions.setField('phone', newPhone)}
                    options={{
					    mask: '+9 999-999-9999'
                      }} />
                <Icon
                    active
                    style={styles.inputIcon}
                    ios="ios-call"
                    android="md-call"
                />
              </Item>

              <GooglePlacesAutocomplete
                  placeholder="Address"
                  minLength={2}
                  autoFocus={false}
                  returnKeyType={'search'}
                  listViewDisplayed="auto"
                  fetchDetails={false}
                  renderDescription={row => row.description}
                  placeholderTextColor="white"
                  onPress={(data) => {
                    this.props.actions.setField('address', data.description);
                  }}
                  query={{
                    key: 'AIzaSyCDLvhGAlADSoGUYQuhAk2P2d5pkWDexZE',
                    language: 'en',
                    types: '(cities)',
                  }}
                  styles={styles.googlePlaces}
                  nearbyPlacesAPI="GooglePlacesSearch"
                  GooglePlacesSearchQuery={{
                    rankby: 'distance',
                  }}
                  debounce={200}
                  renderRightButton={() => (<Icon
                    active
                    style={combineStyles(styles.inputIcon, { paddingRight: 8 })}
                    ios="ios-pin"
                    android="md-pin"
                  />)}
              />

            </Form>
          </Content>
          <OnboardingFooter step={1} clickAction={() => this.handleNext()} />
        </Form>
    );
  }
}

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators({
    setField,
  }, dispatch),
});

const mapStateToProps = state => ({ onboarding: state.onboarding });

export default connect(mapStateToProps, mapDispatchToProps)(AddressStep);
