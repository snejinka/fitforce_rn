import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
  Content,
  Form,
  Button,
  Text,
} from 'native-base';
import { Image } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { setField } from '../../store/onboarding';
import styles from './assets/styles/index';
import appStyles, { combineStyles } from '../../assets/styles/index';
import OnboardingFooter from './OnboardingFooter';
import LogoImage from '../../components/LogoImage';

const switcher = require('./assets/images/switcher.png');
const switcherLeftActive = require('./assets/images/activeLeftSwitcher.png');
const switcherRightActive = require('./assets/images/activeRightSwitcher.png');

const SwitcherButton = props => (
  <Button
    transparent
    style={styles.switcherButton}
    onPress={() => props.onPress(props.stateVariable, props.text)}
  >
    <Text style={combineStyles(appStyles.text, props.textStyle)}>{props.text}</Text>
  </Button>
);

const SwitcherItem = props => (
  <Form style={combineStyles(styles.switcherItem, styles.switcherItemLeft)}>
    <SwitcherButton
      stateVariable={props.stateVariable}
      text={props.text}
      onPress={props.onPress}
    />
  </Form>
);

const ActiveSwitcherItem = props => (
  <Image
    source={props.image}
    style={combineStyles(styles.switcherItem, styles.switcherItemRight, styles.switcherItemActive)}
  >
    <Text style={combineStyles(appStyles.text, styles.switcherTextActive)}>{props.text}</Text>
  </Image>
);

const UnitsStep = (props) => {
  const { lengthUnit, weightUnit } = props.onboarding;

  return (
    <Form style={styles.container}>
      <Content style={appStyles.content}>
        <LogoImage style={appStyles.logoImage} />

        <Form style={styles.unitsForm}>
          <Text style={combineStyles(appStyles.text, appStyles.subTitle)}>
            Set your measurement system
          </Text>

          <Form style={styles.unitsContentItemForm}>
            <Text style={combineStyles(appStyles.thinText, styles.switcherLabel)}>CM OR INCH</Text>

            {(() => {
              if (lengthUnit === 'cm') {
                return (
                  <Image source={switcher} style={styles.switcher}>
                    <ActiveSwitcherItem
                      text="cm"
                      image={switcherLeftActive}
                    />
                    <SwitcherItem
                      text="inch"
                      stateVariable="lengthUnit"
                      onPress={props.actions.setField}
                    />
                  </Image>
                );
              }
              return (
                <Image source={switcher} style={styles.switcher}>
                  <SwitcherItem
                    text="cm"
                    stateVariable="lengthUnit"
                    onPress={props.actions.setField}
                  />
                  <ActiveSwitcherItem
                    text="inch"
                    image={switcherRightActive}
                  />
                </Image>
              );
            })()}

          </Form>
          <Form style={styles.unitsContentItemForm}>
            <Text style={combineStyles(appStyles.thinText, styles.switcherLabel)}>KG OR LBS</Text>
            {(() => {
              if (weightUnit === 'kg') {
                return (
                  <Image source={switcher} style={styles.switcher}>
                    <ActiveSwitcherItem
                      text="kg"
                      image={switcherLeftActive}
                    />
                    <SwitcherItem
                      text="lbs"
                      stateVariable="weightUnit"
                      onPress={props.actions.setField}
                    />
                  </Image>
                );
              }
              return (
                <Image source={switcher} style={styles.switcher}>
                  <SwitcherItem
                    text="kg"
                    stateVariable="weightUnit"
                    onPress={props.actions.setField}
                  />
                  <ActiveSwitcherItem
                    text="lbs"
                    image={switcherRightActive}
                  />
                </Image>
              );
            })()}
          </Form>
        </Form>
      </Content>
      <OnboardingFooter step={3} clickAction={Actions.currencyStep} />
    </Form>
  );
};

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators({
    setField,
  }, dispatch),
});

const mapStateToProps = state => ({ onboarding: state.onboarding });

export default connect(mapStateToProps, mapDispatchToProps)(UnitsStep);
