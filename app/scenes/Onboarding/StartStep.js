import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
  Content,
  Form,
  Button,
  Text,
  Icon,
  Footer,
} from 'native-base';
import { Actions } from 'react-native-router-flux';
import { signIn } from '../../store/user';
import { setField,createTrainer } from '../../store/onboarding';
import styles from './assets/styles/index';
import appStyles, { combineStyles } from '../../assets/styles/index';
import LogoImage from '../../components/LogoImage';

class StartStep extends React.Component{
  constructor(props){
    super(props);
  }
  componentWillMount(){
    this.props.setField('tempEmail',this.props.email)
    this.props.setField('tempPassword',this.props.password)

  }
  render(){
      const {email,password} = this.props;
    return(
        <Form style={styles.container}>
          <Content style={appStyles.content}>
            <LogoImage style={appStyles.logoImage} />

            <Form style={styles.mainForm}>
              <Text style={combineStyles(appStyles.text, appStyles.title)}>Congratulations!</Text>

              <Text style={combineStyles(appStyles.text, appStyles.subTitle)}>
                You have now completed FitForce.com for Trainers account registration.
              </Text>

              <Text style={combineStyles(appStyles.text, appStyles.subTitle, { marginTop: 15 })}>
                Answer a few simple questions for quick and easy onboarding.
              </Text>

              <Button full rounded iconRight light style={styles.startBtn} onPress={Actions.addressStep}>
                <Text style={styles.startBtnText}>
                  Start Onboarding
                </Text>
                <Icon style={styles.startBtnText} name="arrow-forward" />
              </Button>
            </Form>
          </Content>

          <Footer style={appStyles.transparent}>
            <Form>
              <Button
                  onPress={() => {
                    this.props.signIn(email,password)
                    .then(res => this.props.createTrainer({email,password}))
                    }}
                  transparent
                  light>
                <Text style={appStyles.thinText}>Skip</Text>
              </Button>
            </Form>
          </Footer>
        </Form>
    )
  }


}

const mapDispatchToProps = dispatch => bindActionCreators({
  signIn,
  setField,
  createTrainer
}, dispatch);

const mapStateToProps = state => ({ user: state.user });

export default connect(mapStateToProps, mapDispatchToProps)(StartStep);
