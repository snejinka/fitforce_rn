import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
  Content,
  Text,
  Separator,
  Container,
  Button
} from 'native-base';
import { Actions } from 'react-native-router-flux';
import { Image, View, ScrollView, TouchableOpacity } from 'react-native';
import styles from '../assets/styles/index';
import { logout,setSession } from '../store/user';
import { updateSubscription,getTrainerInfo } from '../store/onboarding';
import LogoImage from '../components/LogoImage';
import stripe from 'tipsi-stripe';
import {hardDeleteAppointments} from '../store/appointment'



class BlockScreen extends Component {
  constructor(props){
    super(props);
    this.state = {
      stripeToken:''
    }
  }

  componentWillMount(){
    stripe.init({
      publishableKey: 'pk_test_ZCU4zPEbkl18CQQ65uNKMglB'
    })
  }

  handleChangePayment(){
    const options = {
      smsAutofillDisabled: true
    };
    stripe.paymentRequestWithCardForm(options)
        .then(response => {
          if(response.tokenId){
            this.setState({
              stripeToken:response.tokenId,
            })
            this.props.updateSubscription(this.state.stripeToken,this.props.subscription_plan,this.props.max_clients_count).then(() => {
              this.props.getTrainerInfo().then(() => {
                if(this.props.success_last_payment){
                  this.props.setSession('authorized');
                }
              })
            })
          }
        })
        .catch(err => {
          console.warn(err)
        })
  }
  
  handleSwitchtoFree(){
    Actions.deleteClients()
  }
  

  render() {
    return (
      <Container style={styles.containerBlue}>
        <Content style={[styles.responsiveContent,{paddingLeft:'10%',paddingRight:'10%'}]}>
          <View style={{marginBottom:100,marginTop:50}}>
            <Text style={[styles.title,styles.whiteTitle,{paddingLeft:'5%',paddingRight:'5%',marginBottom:80}]}>Your payment method was declined</Text>
            <Text style={[styles.title,styles.whiteSubTitle]}>Unfortunately your most recent subscription payment was declined. Please update your credit card information or downgrade to the Free Plan. Note that the Free Plan has a 3-client limit and excess clients will have to be deleted.</Text>
            <Text style={[styles.title,styles.whiteSubTitle]}>For assistance, please contact
              <Text style={[styles.title,styles.whiteSubTitle,{fontWeight:'500'}]}> support@fitforce.com</Text>
            </Text>
          </View>
          <View>
            <Button
                full rounded light style={styles.btnWhite}
                onPress={() => this.handleSwitchtoFree()}>
              <Text style={styles.btnWhiteText}>Switch to Free</Text>
            </Button>
            <Button
                full rounded light style={styles.btnWhite}
                onPress={() => this.handleChangePayment()}>
              <Text style={styles.btnWhiteText}>Change Card</Text>
            </Button>
            <Button
                full rounded light style={styles.btnLink}
                onPress={() => {
                  this.props.logout()
                  this.props.hardDeleteAppointments()
                  }}>
              <Text style={styles.btnLinkText}>Log Out</Text>
            </Button>
          </View>
        </Content>
      </Container>
    );
  }
}



const mapDispatchToProps = dispatch => bindActionCreators({
  logout,
  updateSubscription,
  getTrainerInfo,
  setSession,
  hardDeleteAppointments
}, dispatch);

const mapStateToProps = state => ({
  user: state.user,
  clients: state.client.clients,
  success_last_payment: state.onboarding.success_last_payment,
  subscription_plan: state.onboarding.subscription_plan,
  max_clients_count: state.onboarding.max_clients_count,
});

export default connect(mapStateToProps, mapDispatchToProps)(BlockScreen);