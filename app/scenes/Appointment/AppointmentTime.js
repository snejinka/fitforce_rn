import React, {Component} from 'react';
import PropTypes from 'prop-types'
import {
    Item,
    Label,
    Input,
} from 'native-base';
import styles from '../../assets/styles/index';
import { View, ScrollView, Image } from 'react-native';
import IconAwesome from 'react-native-vector-icons/FontAwesome';
import { Dropdown } from 'react-native-material-dropdown';
import DatePicker from 'react-native-datepicker'
import {appointmentsTypes,servicesValues,currencies,appointmentsRepeatTypes,daysConstant} from '../../constants'



class AppointmentTime extends Component {
    constructor(props){
        super(props);
    }

    render() {
        const {initial,onTimeStartChange,onTimeEndChange,timeStart,timeEnd,editable} = this.props;
        if(editable){
            return(
                <View style={styles.formRow}>
                    <Item style={{ ...styles.inlineInput, ...styles.inlineInputMargin }} stackedLabel>
                        <Label style={styles.label}>{'start time'.toUpperCase()}</Label>
                        <Input
                            style={initial || timeStart.length > 0
                    ? { ...styles.input }
                    : {...styles.input,...styles.inputInvalid}}
                            value={timeStart}
                            onFocus={() => this.datepickerTimeStart.onPressDate()}/>
                        <DatePicker
                            style={{width: 0,height:0}}
                            date={timeStart}
                            mode="time"
                            format="HH:mm"
                            confirmBtnText="Confirm"
                            cancelBtnText="Cancel"
                            hideText={true}
                            showIcon={false}
                            onDateChange={(time_start) => onTimeStartChange(time_start)}
                            ref={(d) => { this.datepickerTimeStart = d }}
                        />
                    </Item>
                    <Item stackedLabel style={styles.inlineInput}>
                        <Label style={styles.label}>{'End time'.toUpperCase()}</Label>
                        <Input
                            style={initial || timeEnd.length > 0
                    ? { ...styles.input }
                    : {...styles.input,...styles.inputInvalid}}
                            value={timeEnd}
                            onFocus={() => this.datepickerTimeEnd.onPressDate()}/>
                        <DatePicker
                            style={{width: 0,height:0}}
                            date={timeEnd}
                            mode="time"
                            format="HH:mm"
                            confirmBtnText="Confirm"
                            cancelBtnText="Cancel"
                            hideText={true}
                            showIcon={false}
                            onDateChange={(time_end) => onTimeEndChange(time_end)}
                            ref={(d) => { this.datepickerTimeEnd = d }}
                        />
                    </Item>
                </View>
            )
        }else{
            return(
                <View style={styles.formRow}>
                    <Item style={{ ...styles.inlineInput, ...styles.inlineInputMargin }} stackedLabel>
                        <Label style={styles.label}>{'start time'.toUpperCase()}</Label>
                        <Input
                            editable={false}
                            style={initial || timeStart.length > 0
                    ? { ...styles.input }
                    : {...styles.input,...styles.inputInvalid}}
                            value={timeStart}/>
                    </Item>
                    <Item stackedLabel style={styles.inlineInput}>
                        <Label style={styles.label}>{'End time'.toUpperCase()}</Label>
                        <Input
                            editable={false}
                            style={initial || timeEnd.length > 0
                    ? { ...styles.input }
                    : {...styles.input,...styles.inputInvalid}}
                            value={timeEnd}/>
                    </Item>
                </View>
            )
        }

    }
}

AppointmentTime.propTypes = {
    onTimeStartChange: PropTypes.func,
    onTimeEndChange: PropTypes.func,
    timeStart: PropTypes.string.isRequired,
    timeEnd: PropTypes.string.isRequired,
    initial: PropTypes.bool,
    editable: PropTypes.bool,
}


export default AppointmentTime;
