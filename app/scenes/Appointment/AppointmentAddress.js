import React, {Component} from 'react';
import PropTypes from 'prop-types'
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
    Item,
    Label,
    Input
} from 'native-base';
import { View, ScrollView, Image } from 'react-native';
import styles from '../../assets/styles/index';
import IconAwesome from 'react-native-vector-icons/FontAwesome';
import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete';
import {setNetworkStatus} from '../../store/network'



class AppointmentAddress extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        const {address, onChangeAddress,initial,editable,isOnlineNetwork} = this.props;
        if(isOnlineNetwork && editable){
            return (
                <Item
                    style={initial || address.length > 0
                    ? { ...styles.inlineInput }
                    : {...styles.inlineInput,...styles.inputInvalid}}
                    stackedLabel>
                    <Label style={styles.label}>{'Address'.toUpperCase()}</Label>
                    <View style={styles.stackedInputIconContainer}>
                        <IconAwesome
                            active name='map-marker'
                            style={[styles.stackedInputIcon,styles.selectIconBlue,styles.mapMarkerIcon,{position:'absolute',top:18,zIndex:10,}]}/>
                        <GooglePlacesAutocomplete
                            placeholder="Address"
                            minLength={2}
                            autoFocus={false}
                            returnKeyType={'search'}
                            listViewDisplayed="auto"
                            fetchDetails={false}
                            renderDescription={row => row.description}
                            placeholderTextColor="white"
                            getDefaultValue={() => {
                              return this.props.address;
                            }}
                            onPress={(data) => { onChangeAddress(data.description)}}
                            query={{
                        key: 'AIzaSyCDLvhGAlADSoGUYQuhAk2P2d5pkWDexZE',
                        language: 'en',
                        types: '(cities)',
                      }}
                            textInputProps={{
                        autoCorrect: false,
                        onFocus:(data) => this.props.setNetworkStatus(),
                      }}
                            styles={{
                        textInputContainer: {
                          backgroundColor: 'rgba(0,0,0,0)',
                          borderTopWidth: 0,
                          borderBottomWidth:0,
                          paddingLeft:10
                        },
                         textInput: {
                          marginLeft: 0,
                          marginRight: 0,
                          height: 38,
                          color: '#5d5d5d',
                          fontSize: 16,
                          marginTop:0,
                          paddingBottom:0,
                          paddingTop:0,
                          alignSelf:'center'
                        },
                        predefinedPlacesDescription: {
                          color: '#1faadb'
                        },
                      }}
                            nearbyPlacesAPI="GooglePlacesSearch"
                            GooglePlacesSearchQuery={{
                        rankby: 'distance',
                      }}
                            debounce={200}
                        />
                    </View>
                </Item>
            );
        }else {
            return(
                <Item style={{ ...styles.inlineInput }} stackedLabel>
                    <Label style={styles.label}>{'Address'.toUpperCase()}</Label>
                    <View style={styles.stackedInputIconContainer}>
                        <IconAwesome active name='map-marker' style={{ ...styles.stackedInputIcon,...styles.selectIconBlue, ...styles.mapMarkerIcon}} />
                        <Input 
                            style={styles.input}
                            onFocus={(data) => this.props.setNetworkStatus()}
                            value={address || ''}
                            editable={editable} 
                            onChangeText={(data) => onChangeAddress(data)}/>
                    </View>
                </Item>
            )
        }


    }
}

AppointmentAddress.propTypes = {
    onChangeAddress: PropTypes.func,
    address: PropTypes.string.isRequired,
    initial: PropTypes.bool,
    editable: PropTypes.bool,
}

AppointmentAddress.defaultProps = {
    editable: true,
}

const mapDispatchToProps = dispatch => bindActionCreators({
    setNetworkStatus
}, dispatch);

const mapStateToProps = state => ({
    isOnlineNetwork: state.network.isOnlineNetwork
});

export default connect(mapStateToProps, mapDispatchToProps)(AppointmentAddress);
