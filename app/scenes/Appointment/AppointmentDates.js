import React, {Component} from 'react';
import PropTypes from 'prop-types'
import {
    Item,
    Label,
    Input,
} from 'native-base';
import styles from '../../assets/styles/index';
import { View, ScrollView, Image } from 'react-native';
import { Dropdown } from 'react-native-material-dropdown';
import DatePicker from 'react-native-datepicker'
import {appointmentsTypes} from '../../constants'



class AppointmentDates extends Component {
    constructor(props){
        super(props);
    }

    render() {
        const {initial,appointmentType,date,onDateChange,dateEnd,onDateEndChange} = this.props;
        if(this.props.editable){
            if(appointmentType == appointmentsTypes[0].value || this.props.onlyOneDate){
                return(
                    <Item style={{ ...styles.inlineInput }} stackedLabel>
                        <Label style={styles.label}>{'Date'.toUpperCase()}</Label>
                        <Input
                            style={initial || date.length > 0
                    ? { ...styles.input }
                    : {...styles.input,...styles.inputInvalid}}
                            value={date}
                            onFocus={() => this.datepicker.onPressDate()}/>
                        <DatePicker
                            style={{width: 0,height:0}}
                            date={date}
                            mode="date"
                            format="MM/DD/YYYY"
                            minDate={new Date()}
                            confirmBtnText="Confirm"
                            cancelBtnText="Cancel"
                            hideText={true}
                            showIcon={false}
                            onDateChange={(date) => onDateChange(date)}
                            ref={(d) => { this.datepicker = d }}
                        />
                    </Item>
                )
            }else if(appointmentType == appointmentsTypes[1].value){
                if(this.props.editable){
                    return(
                        <View style={styles.formRow}>
                            <Item style={{ ...styles.inlineInput, ...styles.inlineInputMargin }} stackedLabel>
                                <Label style={styles.label}>{'Start Date'.toUpperCase()}</Label>
                                <Input
                                    style={styles.input}
                                    value={date}
                                    onFocus={() => this.datepicker.onPressDate()}/>
                                <DatePicker
                                    style={{width: 0,height:0}}
                                    date={date}
                                    mode="date"
                                    format="MM/DD/YYYY"
                                    minDate={new Date()}
                                    confirmBtnText="Confirm"
                                    cancelBtnText="Cancel"
                                    hideText={true}
                                    showIcon={false}
                                    onDateChange={(date) => onDateChange(date)}
                                    ref={(d) => { this.datepicker = d }}
                                />
                            </Item>
                            <Item stackedLabel style={styles.inlineInput}>
                                <Label style={styles.label}>{'End Date'.toUpperCase()}</Label>
                                <Input
                                    style={styles.input}
                                    value={dateEnd}
                                    onFocus={() => this.datepickerEnd.onPressDate()}/>
                                <DatePicker
                                    style={{width: 0,height:0}}
                                    date={dateEnd}
                                    mode="date"
                                    format="MM/DD/YYYY"
                                    minDate={new Date()}
                                    confirmBtnText="Confirm"
                                    cancelBtnText="Cancel"
                                    hideText={true}
                                    showIcon={false}
                                    onDateChange={(date_end) => onDateEndChange(date_end)}
                                    ref={(d) => { this.datepickerEnd = d }}
                                />
                            </Item>
                        </View>
                    )
                }

            }else{
                return null;
            }
        }else{
            return(
                <Item stackedLabel style={styles.inlineInput}>
                    <Label style={styles.label}>{'Date'.toUpperCase()}</Label>
                    <Input style={styles.input} value={date} editable={false}/>
                </Item>
            )
        }

    }
}

AppointmentDates.propTypes = {
    appointmentType: PropTypes.string.isRequired,
    onDateChange: PropTypes.func,
    date: PropTypes.string.isRequired,
    dateEnd: PropTypes.string,
    initial: PropTypes.bool,
    editable: PropTypes.bool,
    onlyOneDate: PropTypes.bool,
}

AppointmentDates.defaultProps = {
    editable: true
}

export default AppointmentDates;
