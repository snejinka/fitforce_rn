import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
  Container,
  Content,
  Text,
  Form,
  Item,
  Label,
  Icon,
  Button,
  Input,
} from 'native-base';
import { Actions } from 'react-native-router-flux';
import { View, ScrollView, Image } from 'react-native';
import styles from '../../assets/styles/index';
import Header from '../../components/Header';
import Avatar from '../../assets/images/appointmentAvatar1.png';
import {createAppointments,deleteAppointment,deleteSerialAppointments,getAppointments} from '../../store/appointment'
import {setNetworkStatus} from '../../store/network'
import {currencies,appointmentsTypes} from '../../constants'
import { Dropdown } from 'react-native-material-dropdown';
import {TextInputMask} from 'react-native-masked-text';
import moment from 'moment';
import momentRecur from 'moment-recur';
import AppointmentType from './AppointmentType';
import RepeatType from './RepeatType';
import AppointmentDates from './AppointmentDates';
import AppointmentTime from './AppointmentTime';
import AppointmentAddress from './AppointmentAddress';
import ModalConfirm from '../../components/ModalConfirm';
import ModalBottom from '../../components/ModalBottom';



class ViewAppointment extends Component {
  constructor(props){
    super(props);
    this.state = {
      currency_symbol:'$',
      isModalVisible:false
    }
  }

  componentWillMount(){
    this.props.setNetworkStatus();
    this.props.getAppointments();
  }

  componentDidMount(){
    this.renderCurrencySymbol()
  }

  renderCurrencySymbol(){
    currencies.map((currency) => {
      if(currency.value == this.props.onboarding.currency){
        this.setState({
          currency_symbol:currency.symbol
        })
      }
    })
  }

  renderHeader(){
    const {client,appointment} = this.props;
    if(moment(appointment.date).format('MM/DD/YYYY') >= moment().format('MM/DD/YYYY')){
      return <Header title="View Appointment" rightButtonCallback={() => Actions.editAppointment({client,appointment})} buttonTitle="Edit"/>
    }else {
      return <Header title="View Appointment" buttonTitle=" "/>
    }
  }

  handleDeleteSingle(){
    this.setState({isModalVisible:false})
    this.props.deleteAppointment(this.props.appointment)
    Actions.pop()
  }

  handleDeleteSeries(){
    this.setState({isModalVisible:false})
    this.props.deleteSerialAppointments(this.props.appointment)
    Actions.pop()
  }

  handleDeleteButton(){
    if(this.props.appointment.type == appointmentsTypes[1].value){
      this.setState({isModalVisible:true})
    }else{
      this.handleDeleteSingle()
    }
  }

  render() {
    const {client,appointment} = this.props;
    const {date,time_start,time_end,address,description,amount_in_cents,fitness_programm,type,frequency} = appointment;
    return (
      <Container>
        {this.renderHeader()}
        <Content>
          <ScrollView style={{ ...styles.scrollviewNopadding }}>
            <Form style={[styles.form,{marginBottom:150}]}>
              <Item style={{ ...styles.inlineInput }} stackedLabel>
                <Label style={styles.label}>{'Client Name'.toUpperCase()}</Label>
                <View style={styles.inlineInputRow}>
                  <Image source={Avatar} style={styles.inlineInputImage}/>
                  <Input
                      style={styles.input}
                      value={client.first_name}
                      editable={false}/>
                </View>
              </Item>
              <AppointmentType appointmentType={appointment.type} editable={false}/>
              <RepeatType
                  appointmentType={type}
                  appointmentRepeatType={frequency}
                  editable={false}/>
              <AppointmentDates
                  appointmentType={type}
                  date={date}
                  editable={false}/>
              <AppointmentTime
                  timeStart={time_start}
                  timeEnd={time_end}
                  editable={false}
              />
              <AppointmentAddress
                  address={address}
                  editable={false}/>

              <Item style={{ ...styles.inlineInput }} stackedLabel>
                <Label style={styles.label}>{'Description'.toUpperCase()}</Label>
                <Input
                    style={{...styles.input}}
                    value={description}
                    editable={false}/>
              </Item>
              <Item style={{ ...styles.inlineInput }} stackedLabel>
                <Label style={styles.label}>{'Fee (PER APPOInTMENT)'.toUpperCase()}</Label>
                <TextInputMask
                    ref={'currency'}
                    placeholderTextColor="black"
                    style={[styles.input,{flex:1,alignSelf:'stretch'}]}
                    value={amount_in_cents}
                    type={'money'}
                    editable={false}
                    options={{
					    unit: this.state.currency_symbol,
                      }} />
              </Item>
              <Item style={{ ...styles.inlineInput }} stackedLabel>
                <Label style={styles.label}>{'Fitness Program'.toUpperCase()}</Label>
                <View style={styles.stackedInputIconContainer}>
                  <Input style={styles.input} value={fitness_programm} editable={false}/>
                </View>
              </Item>
              <Button style={[styles.btnPrimary,styles.btnDelete,{marginTop:40}]}
                      block
                      onPress={() => this.handleDeleteButton()}>
                <Text style={styles.btnPrimaryText}>{'Delete Appointment'.toUpperCase()}</Text>
              </Button>
            </Form>
          </ScrollView>
          <ModalBottom
              isVisible={this.state.isModalVisible}
              title="What would you want to delete?"
              firstLineText="Only this appointment"
              secondLineText="All appointments in the series"
              handleCancel={() => this.setState({isModalVisible:false})}
              handleFirst={() => this.handleDeleteSingle()}
              handleSecond={() => this.handleDeleteSeries()}/>
        </Content>
      </Container>
    );
  }
}

const mapDispatchToProps = dispatch => bindActionCreators({
  createAppointments,
  deleteAppointment,
  deleteSerialAppointments,
  getAppointments,
  setNetworkStatus
}, dispatch);

const mapStateToProps = state => ({
  onboarding: state.onboarding,
  appointment: state.appointment.selectedAppointment
});

export default connect(mapStateToProps, mapDispatchToProps)(ViewAppointment);