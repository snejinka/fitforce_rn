import React, {Component} from 'react';
import PropTypes from 'prop-types'
import {
    Item,
    Icon,
    Input,
    Label
} from 'native-base';
import styles from '../../assets/styles/index';
import { View, ScrollView, Image } from 'react-native';
import IconAwesome from 'react-native-vector-icons/FontAwesome';
import { Dropdown } from 'react-native-material-dropdown';
import {appointmentsTypes} from '../../constants'


class AppointmentType extends Component {
    constructor(props){
        super(props);
    }

    render() {
        const {appointmentType,editable} = this.props;

        if(editable){
            return (
                <Item style={{ ...styles.inlineInput }} stackedLabel>
                    <Label style={styles.label}>{'Appointment Type'.toUpperCase()}</Label>
                    <View style={styles.stackedInputIconContainer}>
                        {appointmentType !== appointmentsTypes[0].value && <IconAwesome active name='refresh' style={{ ...styles.stackedInputIcon,...styles.selectIconBlue, ...styles.refreshIcon}} />}
                        <Dropdown
                            label=" "
                            fontSize={17}
                            rippleOpacity={0.1}
                            textColor={'rgb(102, 120, 144)'}
                            value={appointmentType}
                            onChangeText={(value) => this.props.onChangeType(value)}
                            inputContainerStyle={[{borderBottomColor: 'transparent'}]}
                            pickerStyle={{backgroundColor : "#fff",borderWidth:0}}
                            containerStyle={[styles.dropdown,{width:'auto',flex:1,borderWidth:0,paddingBottom:15}]}
                            data={appointmentsTypes}
                            renderAccessory={() => {return <Icon name="ios-arrow-down" style={{ ...styles.stackedInputIcon, ...styles.selectIcon,...styles.selectIconBlue }} />}}
                        />
                    </View>
                </Item>
            );
        }else{
            return(
                <Item style={{ ...styles.inlineInput }} stackedLabel>
                    <Label style={styles.label}>{'Appointment Type'.toUpperCase()}</Label>
                    <View style={styles.stackedInputIconContainer}>
                        {appointmentType !== appointmentsTypes[0].value && <IconAwesome active name='refresh' style={{ ...styles.stackedInputIcon,...styles.selectIconBlue, ...styles.refreshIcon}} />}
                        <Input style={styles.input} value={appointmentType} editable={false}/>
                    </View>
                </Item>
            )
        }

    }
}

AppointmentType.defaultProps = {
    editable: true
}

AppointmentType.propTypes = {
    appointmentType: PropTypes.string.isRequired,
    onChangeType: PropTypes.func,
    editable: PropTypes.bool
}

export default AppointmentType;
