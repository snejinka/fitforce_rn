import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
  Container,
  Content,
  Text,
  Form,
  Item,
  Label,
  Icon,
  Button,
  Input,
} from 'native-base';
import { Actions } from 'react-native-router-flux';
import { View, ScrollView, Image } from 'react-native';
import styles from '../../assets/styles/index';
import Header from '../../components/Header';
import Avatar from '../../assets/images/appointmentAvatar1.png';
import {createAppointments,editSingleAppointments,getSelectedAppointment,editSerialAppointments,getAppointments} from '../../store/appointment';
import {setNetworkStatus} from '../../store/network';
import {appointmentsTypes,servicesValues,currencies,appointmentsRepeatTypes,daysConstant} from '../../constants';
import { Dropdown } from 'react-native-material-dropdown';
import {TextInputMask} from 'react-native-masked-text';
import moment from 'moment';
import momentRecur from 'moment-recur';
import uuid from 'uuid';
import AppointmentType from './AppointmentType';
import RepeatType from './RepeatType';
import AppointmentDates from './AppointmentDates';
import AppointmentTime from './AppointmentTime';
import AppointmentAddress from './AppointmentAddress';
import Modal from 'react-native-modal';


class EditAppointment extends Component {
  constructor(props){
    super(props);
    this.state = {
      id:props.appointment.id,
      date:props.appointment.date,
      time_start:props.appointment.time_start,
      time_end:props.appointment.time_end,
      client_id: props.client.id,
      address:props.appointment.address,
      description:props.appointment.description || '',
      fee:0,
      stringFee:0,
      fitness_programm:'',
      currency_symbol:'$',
      appointment_type:props.appointment.type,
      frequency:props.appointment.frequency || '',
      amount_in_cents:props.appointment.amount_in_cents || 0,
      initial:true,
      numberOfAppointments: 0,
      isModalVisible:false
    };
    this.handleSave = this.handleSave.bind(this);
    this.newAppointmentData = {};
  }

  componentWillMount(){
    this.props.setNetworkStatus();
    this.props.getAppointments();
  }
  componentDidMount(){
    this.renderCurrencySymbol()
  }

  handleSave(){
    this.setState({initial:false})
    const {date,time_start,time_end,client_id,address,description,fee,appointment_type,numberOfAppointments} = this.state;
    if(!date || !time_start || !time_end || !address )
        return null;

    if(appointment_type == appointmentsTypes[1].value){
      this.setState({isModalVisible:true})
    }else{
      this.props.editSingleAppointments(this.newAppointmentData)
      this.props.getSelectedAppointment(this.newAppointmentData.id)
      Actions.pop();
    }
  }

  handleSingle(){
    this.setState({isModalVisible:false})
    this.props.editSingleAppointments(this.newAppointmentData)
    this.props.getSelectedAppointment(this.newAppointmentData.id)
    Actions.pop();
  }

  handleSeries(){
    const {time_start,time_end,client_id,address,description,currency,fitness_programm} = this.state;
    this.setState({isModalVisible:false})
    this.props.editSerialAppointments({...this.newAppointmentData,serial_id:this.props.appointment.serial_id})
    this.props.getSelectedAppointment(this.newAppointmentData.id)
    Actions.pop();
  }

  renderCurrencySymbol(){
    currencies.map((currency) => {
      if(currency.value == this.props.onboarding.currency){
        this.setState({
          currency_symbol:currency.symbol
        })
      }
    })
  }
  

  render() {
    const {client,appointment} = this.props;
    const {
        id,date, time_start, time_end, client_id, address, description, amount_in_cents, fitness_programm,
        stringFee, initial, appointment_type, numberOfAppointments, frequency
    } = this.state;

    this.newAppointmentData = {
      id,
      date,
      time_start,
      time_end,
      client_id,
      address,
      description,
      amount_in_cents,
      currency: this.props.onboarding.currency,
      fitness_programm,
      type:appointment_type,
      frequency
    };

    return (
      <Container>
        <Header title="Edit Appointment" rightButtonCallback={this.handleSave}/>
        <Content>
          <Modal isVisible={this.state.isModalVisible} style={{justifyContent:'flex-end'}}>
            <View style={[styles.modalPrimary,styles.modalBottom ]}>
              <Text style={[styles.modalPrimaryTitle,styles.modalBottomTitle]}>What would you want to update?</Text>
              <Button onPress={() => this.handleSingle()} style={[styles.btnPrimary,styles.btnModal,styles.btnModalBordered,{backgroundColor:'transparent'}]}>
                <Text style={[styles.btnPrimaryText,styles.btnModalText,{fontWeight:'400'}]}>Only this appointment</Text>
              </Button>
              <Button onPress={() => this.handleSeries()} style={[styles.btnPrimary,styles.btnModal,styles.btnModalBordered,{backgroundColor:'transparent'}]}>
                <Text style={[styles.btnPrimaryText,styles.btnModalText,{fontWeight:'400'}]}>All appointments in the series</Text>
              </Button>
            </View>
            <Button style={[styles.btnPrimary,styles.btnModal,{marginTop:8,}]} onPress={() => this.setState({isModalVisible:false})}>
              <Text style={[styles.btnPrimaryText,styles.btnModalText]}>Cancel</Text>
            </Button>
          </Modal>
          <ScrollView style={{ ...styles.scrollviewNopadding }}>
            <Form style={[styles.form,{marginBottom:150}]}>
              <Item style={{ ...styles.inlineInput }} stackedLabel>
                <Label style={styles.label}>{'Client Name'.toUpperCase()}</Label>
                <View style={styles.inlineInputRow}>
                  <Image source={Avatar} style={styles.inlineInputImage}/>
                  <Input style={styles.input} value={client.first_name} editable={false}/>
                </View>
              </Item>
              <AppointmentType
                  appointmentType={appointment_type}
                  editable={false}/>
              <RepeatType 
                  appointmentType={this.state.appointment_type} 
                  appointmentRepeatType={frequency}
                  editable={false}/>
              <AppointmentDates
                  appointmentType={appointment_type}
                  date={date}
                  onDateChange={(date) => this.setState({date})}
                  initial={initial}
                  onlyOneDate={true} />
              <AppointmentTime
                  timeStart={time_start}
                  onTimeStartChange={(time_start) => this.setState({time_start})}
                  timeEnd={time_end}
                  onTimeEndChange={(time_end) => this.setState({time_end})}
                  initial={this.state.initial}
                  editable={true}
              />

              <AppointmentAddress
                  address={address}
                  initial={this.state.initial}
                  onChangeAddress={(data) => { this.setState({address:data})}}
                  editable={true} />

              <Item style={{ ...styles.inlineInput }} stackedLabel>
                <Label style={styles.label}>{'Description'.toUpperCase()}</Label>
                <Input
                    style={{...styles.input}}
                    value={description}
                    onChangeText={(description) => this.setState({description})}/>
              </Item>
              <Item style={{ ...styles.inlineInput }} stackedLabel>
                <Label style={styles.label}>{'Fee (PER APPOInTMENT)'.toUpperCase()}</Label>
                <TextInputMask
                    ref={'currency'}
                    placeholderTextColor="black"
                    style={[styles.input,{flex:1,alignSelf:'stretch'}]}
                    value={stringFee}
                    type={'money'}
                    onChangeText={stringFee => this.setState({stringFee,amount_in_cents: Number(stringFee.replace(/[^0-9\-]+/g, ''))})}
                    options={{
					    unit: this.state.currency_symbol,
                      }} />
              </Item>
              <Item
                  style={[styles.inlineInput]}
                  stackedLabel>
                <Input style={styles.label} value={'Fitness Program'.toUpperCase()} />
                <View style={styles.stackedInputIconContainer}>
                  <Dropdown
                      label=" "
                      fontSize={17}
                      textColor={'rgb(102, 120, 144)'}
                      value={fitness_programm}
                      onChangeText={(fitness_programm) => this.setState({fitness_programm})}
                      inputContainerStyle={[{borderBottomColor: 'transparent'}]}
                      pickerStyle={{backgroundColor : "#fff",borderWidth:0}}
                      containerStyle={[styles.dropdown,{width:'auto',flex:1,borderWidth:0,paddingBottom:15}]}
                      data={servicesValues}
                      renderAccessory={() => {return <Icon name="ios-arrow-down" style={{ ...styles.stackedInputIcon, ...styles.selectIcon,...styles.selectIconBlue }} />}}
                  />
                </View>
              </Item>
            </Form>
          </ScrollView>
        </Content>
      </Container>
    );
  }
}

const mapDispatchToProps = dispatch => bindActionCreators({
  createAppointments,
  editSingleAppointments,
  getSelectedAppointment,
  editSerialAppointments,
  setNetworkStatus,
  getAppointments
}, dispatch);

const mapStateToProps = state => ({
  onboarding: state.onboarding
});

export default connect(mapStateToProps, mapDispatchToProps)(EditAppointment);