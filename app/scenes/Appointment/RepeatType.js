import React, {Component} from 'react';
import PropTypes from 'prop-types'
import {
    Item,
    Icon,
    Input,
    Label
} from 'native-base';
import styles from '../../assets/styles/index';
import { View, ScrollView, Image } from 'react-native';
import { Dropdown } from 'react-native-material-dropdown';
import {appointmentsTypes,appointmentsRepeatTypes} from '../../constants'




class RepeatType extends Component {
    constructor(props){
        super(props);
    }

    render() {
        const {editable,appointmentType,appointmentRepeatType} = this.props;
        if(appointmentType !== appointmentsTypes[1].value){
            return null;
        }else if(editable){
            return (
                <Item style={{ ...styles.inlineInput }} stackedLabel>
                    <Label style={styles.label}>{'Repeat'.toUpperCase()}</Label>
                    <View style={styles.stackedInputIconContainer}>
                        <Dropdown
                            label=" "
                            fontSize={17}
                            rippleOpacity={0.1}
                            textColor={'rgb(102, 120, 144)'}
                            value={this.props.appointmentRepeatType}
                            onChangeText={(appointment_repeat_type) => this.props.onChangeType(appointment_repeat_type)}
                            inputContainerStyle={[{borderBottomColor: 'transparent'}]}
                            pickerStyle={{backgroundColor : "#fff",borderWidth:0}}
                            containerStyle={[styles.dropdown,{width:'auto',flex:1,borderWidth:0,paddingBottom:15}]}
                            data={appointmentsRepeatTypes}
                            renderAccessory={() => {return <Icon name="ios-arrow-down" style={{ ...styles.stackedInputIcon, ...styles.selectIcon,...styles.selectIconBlue }} />}}
                        />
                    </View>
                </Item>
            );
        }else {
            return(
                <Item style={{ ...styles.inlineInput }} stackedLabel>
                    <Label style={styles.label}>{'Repeat'.toUpperCase()}</Label>
                    <View style={styles.stackedInputIconContainer}>
                        <Input style={styles.input} value={appointmentRepeatType} editable={false}/>
                    </View>
                </Item>
            )
        }


    }
}

RepeatType.propTypes = {
    appointmentRepeatType: PropTypes.string.isRequired,
    appointmentType: PropTypes.string.isRequired,
    onChangeType: PropTypes.func,
    editable: PropTypes.bool
}

RepeatType.defaultProps = {
    editable: true
}

export default RepeatType;
