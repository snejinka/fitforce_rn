import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
  Container,
  Content,
  Text,
  Form,
  Item,
  Label,
  Icon,
  Button,
  Input,
} from 'native-base';
import { View, ScrollView, Image } from 'react-native';
import styles from '../../assets/styles/index';
import Header from '../../components/Header';
import Avatar from '../../assets/images/appointmentAvatar1.png';
import {createAppointments} from '../../store/appointment'
import {appointmentsTypes,servicesValues,currencies,appointmentsRepeatTypes,daysConstant} from '../../constants'
import { Dropdown } from 'react-native-material-dropdown';
import {TextInputMask} from 'react-native-masked-text';
import moment from 'moment';
import momentRecur from 'moment-recur';
import uuid from 'uuid';
import AppointmentType from './AppointmentType';
import RepeatType from './RepeatType';
import AppointmentDates from './AppointmentDates';
import AppointmentTime from './AppointmentTime';
import AppointmentAddress from './AppointmentAddress';
import { Actions } from 'react-native-router-flux';



class CreateAppointment extends Component {
  constructor(props){
    super(props);
    this.state = {
      date:'',
      date_end:'',
      time_start:'',
      time_end:'',
      client_id: props.client.id,
      address:'',
      description:'',
      fee:0,
      stringFee:0,
      fitness_programm:'',
      currency_symbol:'$',
      appointment_type:appointmentsTypes[0].value,
      appointment_repeat_type:appointmentsRepeatTypes[0].value,
      initial:true,
      selectedDays:[],
      numberOfAppointments: 0,
    }
    this.handleSave = this.handleSave.bind(this)
    this.newAppointmentData = {}
    this.newAppointments = [],
    this.newSerialAppointments = [],
    this.seriesDates = [],
    this.serialId = '',
    this.selectedDays = []

  }

  componentDidMount(){
    this.renderCurrencySymbol()
  }

  handleSave(){
    this.setState({initial:false})
    const {date,date_end,time_start,time_end,client_id,address,description,fee,appointment_type,numberOfAppointments} = this.state;

    if(appointment_type == appointmentsTypes[0].value){
      if(date && time_start && time_end && client_id && address){
        this.newAppointments.push(this.newAppointmentData)
        this.props.createAppointments(this.newAppointments)
        Actions.pop()
      }
    }else {
      if(date && date_end && time_start && time_end && client_id && address){
        this.serialId = uuid.v1()
        this.seriesDates.map((date) => {
          return(
              this.newSerialAppointments.push({
                ...this.newAppointmentData,
                id: uuid.v4(),
                date: moment(date).format('MM/DD/YYYY'),
                serial_id: this.serialId
              })
          )
        })
        if(numberOfAppointments == this.newSerialAppointments.length){
          this.props.createAppointments(this.newSerialAppointments)
          Actions.pop()
        }
      }
    }
  }

  renderCurrencySymbol(){
    currencies.map((currency) => {
      if(currency.value == this.props.onboarding.currency){
        this.setState({
          currency_symbol:currency.symbol
        })
      }
    })
  }

  createSeries(date_end){
    const {date,time_start,time_end,client_id,address,description,fee,fitness_programm,stringFee,initial,appointment_type,appointment_repeat_type,selectedDays} = this.state;
    if(appointment_repeat_type == appointmentsRepeatTypes[0].value){
      this.seriesDates = moment().recur(moment(date),moment(date_end)).every(selectedDays).daysOfWeek().all()
      this.setState({
        numberOfAppointments: this.seriesDates.length
      })
    }else {
      this.seriesDates = moment().recur(moment(date),moment(date_end)).every(1).days().all()
      this.setState({
        numberOfAppointments: this.seriesDates.length
      })
    }
  }

  handlePressDay(day){
    if(this.selectedDays.length > 0){
      if(this.selectedDays.includes(day.value)){

        this.selectedDays.splice(this.selectedDays.indexOf(day.value) ,1)
        this.setState({selectedDays:this.selectedDays})
      }else{
        this.selectedDays.push(day.value)
        this.setState({selectedDays:this.selectedDays})
      }
    }else{
      this.selectedDays.push(day.value)
      this.setState({selectedDays:this.selectedDays})
    }
    if(this.state.date_end){
      this.createSeries(this.state.date_end)
    }
  }

  handleChangeRepeatType(value){
    this.setState({appointment_repeat_type:value})
    if(this.state.date_end){
      this.createSeries(this.state.date_end)
    }
  }

  render() {
    const {client,user} = this.props;
    const {
        date, date_end, time_start, time_end, client_id, address, description, fee, fitness_programm,
        stringFee, initial, appointment_type, numberOfAppointments, appointment_repeat_type
    } = this.state;
    this.newAppointmentData = {
      id:uuid.v4(),
      date,
      time_start,
      time_end,
      client_id,
      address,
      description,
      amount_in_cents: fee, 
      currency: this.props.onboarding.currency,
      fitness_programm,
      type:appointment_type,
      frequency:appointment_repeat_type,
    };

    return (
      <Container>
        <Header title="Create New Appointment" rightButtonCallback={this.handleSave}/>
        <Content>
          <ScrollView style={{ ...styles.scrollviewNopadding }}>
            <Form style={[styles.form,{marginBottom:150}]}>
              <Item style={{ ...styles.inlineInput }} stackedLabel>
                <Label style={styles.label}>{'Client Name'.toUpperCase()}</Label>
                <View style={styles.inlineInputRow}>
                  <Image source={Avatar} style={styles.inlineInputImage}/>
                  <Input style={styles.input} value={client.first_name}/>
                </View>
              </Item>
              <AppointmentType appointmentType={appointment_type} onChangeType={(value) => this.setState({appointment_type:value})}/>
              <RepeatType 
                  appointmentType={this.state.appointment_type} 
                  appointmentRepeatType={appointment_repeat_type} 
                  onChangeType={(value) => this.handleChangeRepeatType(value)}/>

              {appointment_type == appointmentsTypes[1].value && appointment_repeat_type == appointmentsRepeatTypes[0].value &&
                <Item style={{ ...styles.inlineInput, ...styles.inlineInputUnbordered }} stackedLabel>
                  <Label style={styles.label}>{'Which Days'.toUpperCase()}</Label>
                  <View style={[styles.dayList]}>
                    {daysConstant.map((day) => {
                      return(
                          <Button
                              key={day.key}
                              onPress={() => this.handlePressDay(day)}
                              style={this.state.selectedDays.includes(day.value) ? {...styles.dayContainer, ...styles.dayContainerActive} :{...styles.dayContainer} }>
                            <Text style={{...styles.dayText}}>{day.value.toUpperCase()}</Text>
                          </Button>
                      )
                    })}
                  </View>
                </Item>
              }
              <AppointmentDates
                  appointmentType={appointment_type}
                  date={date}
                  dateEnd={date_end}
                  onDateChange={(date) => this.setState({date})}
                  onDateEndChange={(date_end) => {this.setState({date_end});this.createSeries(date_end)}}
                  initial={initial} />

              {appointment_type == appointmentsTypes[1].value &&
                <Item style={{ ...styles.inlineInput }} stackedLabel>
                  <Label style={styles.label}>{'Number of Appointments'.toUpperCase()}</Label>
                  <Input
                      style={{...styles.input}}
                      value={numberOfAppointments.toString()}/>
                </Item>
              }
              <AppointmentTime
                  timeStart={time_start}
                  onTimeStartChange={(time_start) => this.setState({time_start})}
                  timeEnd={time_end}
                  onTimeEndChange={(time_end) => this.setState({time_end})}
                  initial={this.state.initial}
                  editable={true}
              />

              <AppointmentAddress
                  address={address}
                  initial={this.state.initial}
                  onChangeAddress={(data) => { this.setState({address:data})}}
                  editable={true} />

              <Item style={{ ...styles.inlineInput }} stackedLabel>
                <Label style={styles.label}>{'Description'.toUpperCase()}</Label>
                <Input
                    style={{...styles.input}}
                    value={description}
                    onChangeText={(description) => this.setState({description})}/>
              </Item>
              <Item style={{ ...styles.inlineInput }} stackedLabel>
                <Label style={styles.label}>{'Fee (PER APPOInTMENT)'.toUpperCase()}</Label>
                <TextInputMask
                    ref={'currency'}
                    placeholderTextColor="black"
                    style={[styles.input,{flex:1,alignSelf:'stretch'}]}
                    value={stringFee}
                    type={'money'}
                    onChangeText={stringFee => this.setState({stringFee,fee: Number(stringFee.replace(/[^0-9\-]+/g, ''))})}
                    options={{
					    unit: this.state.currency_symbol,
                      }} />
              </Item>
              <Item
                  style={[styles.inlineInput]}
                  stackedLabel>
                <Input style={styles.label} value={'Fitness Program'.toUpperCase()} />
                <View style={styles.stackedInputIconContainer}>
                  <Dropdown
                      label=" "
                      fontSize={17}
                      textColor={'rgb(102, 120, 144)'}
                      value={fitness_programm}
                      onChangeText={(fitness_programm) => this.setState({fitness_programm})}
                      inputContainerStyle={[{borderBottomColor: 'transparent'}]}
                      pickerStyle={{backgroundColor : "#fff",borderWidth:0}}
                      containerStyle={[styles.dropdown,{width:'auto',flex:1,borderWidth:0,paddingBottom:15}]}
                      data={servicesValues}
                      renderAccessory={() => {return <Icon name="ios-arrow-down" style={{ ...styles.stackedInputIcon, ...styles.selectIcon,...styles.selectIconBlue }} />}}
                  />
                </View>
              </Item>
            </Form>
          </ScrollView>
        </Content>
      </Container>
    );
  }
}

const mapDispatchToProps = dispatch => bindActionCreators({
  createAppointments
}, dispatch);

const mapStateToProps = state => ({
  client: state.client.client,
  user: state.user,
  onboarding: state.onboarding
});

export default connect(mapStateToProps, mapDispatchToProps)(CreateAppointment);