const color1 = 'rgb(79, 155, 235)',
      color2 = 'rgb(102, 120, 144)',
      color3 = 'rgb(97, 114, 136)',
      color4 = 'rgb(255, 121, 121)',
      color5 = 'rgb(157, 171, 194)',
      color6 = 'rgb(171, 183, 203)',
      white = 'rgb(255, 255, 255)',
      transpWhite = 'rgba(255, 255, 255, 0.8)',
      grey = 'rgb(97, 114, 136)',
      mainFont = 'Ubuntu',
      italicFont = 'Ubuntu-Italic',
      lightFont = 'Ubuntu-Light',
      borderColor1 = 'rgba(57, 63, 98, 0.07)';

const styles = {
  responsiveContainer:{
    height:'100%',
    width:'100%'
  },
  mainFont: {
    fontFamily: mainFont,
  },
  mainTitle: {
    paddingLeft: 20,
    paddingRight: 20,
    marginBottom: 23,
    marginTop: 17,
    fontSize: 20,
    color: color2,
    letterSpacing: 0.5,
    fontWeight: 'bold',
    fontFamily: mainFont,
  },
  footerBtnText: {
    fontFamily: mainFont,
    color: 'rgb(79, 155, 235)',
  },
  transparent: {
    backgroundColor: 'transparent',
    borderColor: 'transparent',
    borderBottomColor: 'transparent',
  },
  mainHeader: {
    backgroundColor: '#fff',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    height: 100,
  },
  footerTabs: {
    backgroundColor: '#fff',
    shadowColor: 'rgba(57,63,98,1)',
    shadowOpacity: 0.03,
    shadowRadius: 4,
    shadowOffset: { width: 0, height: -1 },
    flexDirection: 'row',
    alignItems: 'center',
  },
  footerTabsItem: {
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'transparent',
    paddingLeft: 0,
    paddingRight: 0,
  },
  footerTabsItemActive: {

  },
  footerTabsText: {
    fontFamily: mainFont,
    fontSize: 10,
    flexWrap: 'nowrap',
    color: color1,
  },
  footerTabsImage: {
  },
  leftButtonIconStyle: {
    tintColor: 'white',
  },
  fullopacity: {
    opacity: 1,
  },
  appOpacity: {
    opacity: 0.9,
  },
  content: {
    width: '75%',
    marginLeft: 'auto',
    marginRight: 'auto',
    marginTop: 64,
  },
  responsiveContent: {
    width: '100%',
    marginLeft: 'auto',
    marginRight: 'auto',
    paddingLeft:'5%',
    paddingRight:'5%',
  },
  logoImage: {
    marginLeft: 'auto',
    marginRight: 'auto',
    marginTop: 0,
  },
  text: {
    fontFamily: mainFont,
    color: 'white',
    fontSize: 17,
    lineHeight: 23,
  },
  boldText: {
    fontFamily: 'Ubuntu-Medium',
    color: 'white',
    fontSize: 17,
  },
  thinText: {
    fontFamily: 'Ubuntu-Light',
    color: 'white',
    fontSize: 17,
  },
  item: {
    marginLeft: 0,
    marginTop: 0,
    borderColor: 'rgba(255, 255, 255, 0.3)',
  },
  itemInvalid: {
    marginLeft: 0,
    marginTop: 0,
  },
  itemFirst: {
    marginLeft: 0,
    borderColor: 'rgba(255, 255, 255, 0.3)',
  },
  itemFirstInvalid: {
    marginLeft: 0,
  },
  title: {
    fontSize: 24,
    fontFamily: 'Ubuntu-Medium',
    marginBottom: 23,
    textAlign: 'center',
  },
  subTitle: {
    fontFamily: 'Ubuntu-Light',
    textAlign: 'center',
  },
  fastTilesContainer: {
    paddingLeft: 20,
    paddingRight: 20,
    paddingBottom: 20,
    flexDirection: 'row',
    flexWrap: 'nowrap',
  },
  fastTilesItem: {
    backgroundColor: color1,
    flex: 1,
    marginRight: 10,
    borderRadius: 5,
    borderWidth: 1,
    borderColor: 'transparent',
    height: 105,
    paddingLeft: 15,
    paddingRight: 15,
    paddingTop: 10,
    paddingBottom: 10,
    flexDirection: 'column',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  fastTilesItemTitle: {
    fontSize: 12,
    color: '#fff',
    fontWeight: '600',
    textAlign: 'center',
    fontFamily: mainFont,
  },
  fastTilesItemSubtitle: {
    fontSize: 11,
    color: '#fff',
    fontWeight: '400',
    textAlign: 'center',
    fontFamily: mainFont,
  },
  fastTilesItemImage: {
    backgroundColor: 'transparent',
  },
  textBold: {
    fontWeight: 'bold',
    fontFamily: mainFont,
  },
  container: {
    marginTop: 64,
  },
  containerBlue: {
    height: '100%',
    backgroundColor: 'rgb(79, 155,235)',
  },
  scrollview: {
    height: 316,
    flex: 1,
    paddingTop: 15,
    paddingBottom: 15,
  },
  scrollviewFull: {
    height: 550,
  },
  scrollviewNopadding: {
    paddingTop: 0,
  },
  separator: {
    height: 44,
    backgroundColor: 'rgba(57, 63, 98, 0.05)',
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    paddingLeft: 20,
    paddingRight: 20,
    flex: 0,
    borderBottomWidth: 0,
    borderTopWidth: 0,
  },
  separatorInForm: {
    marginLeft: -20,
    marginRight: -20,
    marginTop: 25,
  },
  separatorNoMargin: {
    marginTop: 0
  },
  scrollviewHeaderText: {
    color: color3,
    fontSize: 12,
    fontFamily: mainFont,
    letterSpacing: 1.2,
  },
  appointment: {
    flexDirection: 'row',
    paddingRight: 20,
    paddingLeft: 50,
    marginBottom: 15,
  },
  appointmentMade: {
    opacity: 0.5,
  },
  appointmentInfo: {
    flexDirection: 'column',
    flex: 9,
  },
  appointmentImageContainer: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
  },
  appointmentImage: {
    width: 30,
    height: 30,
  },
  appointmentHeader: {
    flexDirection: 'row',
  },
  appointmentBody: {
    flexDirection: 'row',
  },
  appointmentFooter: {
    flexDirection: 'row',
  },
  appointmentTime: {
    fontSize: 15,
    color: 'rgb(255, 121, 121)',
    fontFamily: mainFont,
    fontWeight: '600',
  },
  appointmentTotalTime: {
    fontSize: 15,
    color: color5,
    fontFamily: mainFont,
    fontWeight: '600',
    marginRight: 2,
  },
  appointmentText: {
    fontSize: 15,
    color: color2,
    fontFamily: mainFont,
    fontWeight: '600',
  },
  appointmentTextBlue: {
    fontSize: 15,
    color: color1,
    fontFamily: mainFont,
  },
  appointmentName: {
    marginLeft:3
  },
  appointmentLocationIcon: {
    marginRight: 6,
  },
  bullet: {
    borderColor: 'rgb(255, 121, 121)',
    borderWidth: 3,
    borderRadius: 10,
    width: 14,
    height: 14,
    backgroundColor: 'transparent',
    position: 'absolute',
    top: 2,
    left: 20,
  },
  bulletLine: {
    borderWidth: 0.5,
    borderColor: 'rgb(219, 233, 243)',
    position: 'relative',
    right: 24,
    top: 16,
  },
  sidebarContainer: {
    flex: 1,
    backgroundColor: '#fff',
  },
  sidebarContainerShadow: {
    flex: 1,
    backgroundColor: '#fff',
    shadowColor: 'rgb(0, 0, 0)',
    shadowOffset: {
      width: 16,
      height: 0,
    },
    shadowOpacity: 0.24,
    shadowRadius: 16,
  },
  sidebarHeader: {
    backgroundColor: color1,
    padding: 20,
  },
  sidebarAvatar: {
    marginTop: 20,
    marginBottom: 13,
    width: 64,
    height: 64,
  },
  sidebarName: {
    fontSize: 15,
    color: '#fff',
    lineHeight: 20,
    fontFamily: mainFont,
  },
  sidebarEmail: {
    fontSize: 15,
    fontFamily: mainFont,
    color: '#fff',
    opacity: 0.7,
    lineHeight: 20,
  },
  sidebarBody: {
    backgroundColor: 'rgb(250, 250, 250)',
    flex: 1,
    paddingTop: 8,
  },
  sidebarMenuItem: {
    marginLeft: 0,
    paddingLeft: 20,
    height: 48,
  },
  sidebarMenuItemActive: {
    backgroundColor: 'rgba(157, 171, 194, 0.07)',
  },
  sidebarMenuItemBody: {
    borderBottomWidth: 0,
  },
  sidebarMenuItemText: {
    color: color3,
    fontSize: 15,
    lineHeight: 24,
  },
  exercisesList:{

  },
  exercisesListItem:{
    minHeight: 70,
    borderColor: borderColor1,
    borderBottomWidth: 2,
    marginLeft:0
  },
  exercisesListItemBody:{
    borderBottomWidth: 0,
    marginLeft:0,
  },
  exercisesListName:{
    fontSize: 15,
    lineHeight: 15,
    letterSpacing: 0.3,
    color: color2,
    fontFamily: mainFont,
    fontWeight:'bold',
    marginTop:5
  },
  exercisesListDescription:{
    fontFamily: mainFont,
    color: color2,
    fontSize: 15,
    lineHeight: 15,
    flexWrap: 'nowrap',
    flexDirection: 'row',
    marginTop: 8,
    marginBottom: 7,
  },
  exercisesListItemRight:{
    borderBottomWidth: 0,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    paddingRight: 0,
    alignSelf:'center',
  },
  exercisesListItemRightTop:{
    alignSelf:'flex-start'
  },
  exercisesListSetsText:{
    fontSize:15,
    color:color2,
    fontFamily: mainFont,
  },
  exercisesListItemOptions:{
    transform: [{ rotate: '90deg'}],
    color:'rgb(80, 155, 235)',
    fontSize:35,
  },
  exercisesListLabel:{
    color:color5,
    fontSize:13,
    paddingBottom:10,
    paddingTop:15
  },
  exercisesListLabelContainer:{
    borderBottomWidth:2,
    borderColor:borderColor1,
    flexDirection:'row',
    justifyContent:'space-between'
  },
  userList: {

  },
  userListItem: {
    height: 70,
    borderColor: borderColor1,
    borderBottomWidth: 2,
    marginLeft: 20,
    marginRight: 20,
  },
  userListUsername: {
    fontSize: 17,
    lineHeight: 15,
    letterSpacing: 0.3,
    color: color3,
    fontFamily: mainFont,
  },
  userListAvatar: {
    width: 40,
    height: 40,
    borderRadius: 20,
  },
  userListItemBody: {
    borderBottomWidth: 0,
  },
  userListItemRight: {
    borderBottomWidth: 0,
    flexDirection: 'column',
    alignItems: 'flex-end',
    justifyContent: 'center',
    paddingRight: 0,
  },
  userListClock: {
    width: 17,
    height: 17,
  },
  userListMessage: {
    fontFamily: mainFont,
    color: 'rgb(145, 159, 182)',
    fontSize: 14,
    lineHeight: 14,
    flexWrap: 'nowrap',
    flexDirection: 'row',
    marginTop: 7,
  },
  userListTime: {
    color: 'rgb(145, 159, 182)',
    fontSize: 12,
    lineHeight: 15,
    fontFamily: mainFont,
  },
  userListUnreadMessages: {
    fontSize: 12,
    lineHeight: 10,
    color: '#fff',
    fontFamily: mainFont,
    fontWeight: '600',
  },
  userListUnreadMessagesContainer: {
    borderRadius: 25,
    backgroundColor: color1,
    width: 28,
    height: 23,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 4,
  },
  uploadPhotoContainer: {
    borderColor: borderColor1,
    borderBottomWidth: 2,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    paddingTop: 18,
    paddingBottom: 11,
    position:'relative'
  },
  uploadPhotoMessageIconContainer:{
    backgroundColor:color1,
    borderRadius:22,
    width:44,
    height:44,
    flexDirection:'row',
    justifyContent:'center',
    alignItems:'center',
    position:'absolute',
    top:20,
    right:120,
    zIndex:2
  },
  uploadPhotoMessageIcon:{
    
  },
  uploadPhotoImage: {
    marginBottom: 9,
    borderColor: color1,
    borderWidth: 3,
    borderRadius: 45,
    width: 90,
    height: 90,
  },
  uploadPhotoText: {
    fontSize: 12,
    color: color1,
    fontFamily: mainFont,
  },
  formRow: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  form: {
    padding: 20,
    paddingTop: 0,
    paddingBottom: 0,
  },
  inlineInput: {
    flex: 1,
    marginLeft: 0,
    marginTop: 20,
    borderBottomWidth: 2,
    borderColor: borderColor1,
  },
  inlineInputMargin: {
    marginRight: 30,
  },
  inlineInputUnbordered: {
    borderBottomWidth:0
  },
  input: {
    height: 48,
    lineHeight: 20,
    fontFamily: mainFont,
    color: color2,
    fontSize: 17,
  },
  inputActive: {
    borderColor: 'rgb(79, 155, 235)'
  },
  dropdown: {
    height: 48,
    flexDirection:'column',
    alignItems:'stretch',
    justifyContent:'center',
    alignSelf:'stretch'
  },
  dropdownText: {
    fontFamily: mainFont,
    color: color2,
    fontSize: 17,
    alignSelf:'stretch',
  },
  dropdownItemsContainer: {
    // flex:1,
    alignSelf:'stretch',
    backgroundColor:'#fff'
  },
  inputInvalid: {
    borderBottomWidth:2,
    borderColor:'red'
  },
  inputFocused: {
    borderColor: color1,
    borderBottomWidth: 2,
  },
  formText: {
    minHeight: 48,
    lineHeight: 20,
    fontFamily: mainFont,
    color: color2,
    fontSize: 17,
    position:null,
    top:5,
    paddingTop:5,
    alignSelf:'flex-start'
  },
  label: {
    color: 'rgb(145, 159, 182)',
    fontSize: 11,
    lineHeight: 10,
    letterSpacing: 1.1,
    fontFamily: mainFont,
    flex: 0,
    height: 'auto',
  },
  labelBordered: {
    color: color5,
    fontSize: 12,
    lineHeight: 12,
    fontFamily: mainFont,
    height: 'auto',
    marginBottom:10,
    marginTop:20
  },
  borderBottom:{
    borderBottomWidth: 2,
    borderColor: borderColor1,
  },
  buttonAdd: {
    paddingLeft: 0,
  },
  buttonAddText: {
    color: 'rgb(80, 155, 235)',
    fontSize: 12,
    lineHeight: 19,
    letterSpacing: 1.2,
    fontFamily: mainFont,
    fontWeight: 'bold',
    paddingLeft: 8
  },
  noBorderBottom: {
    borderBottomWidth: 0,
  },
  btnPrimary: {
    backgroundColor: 'rgba(79, 155, 235, 0.99)',
    borderRadius: 0,
    height: 40,
    marginBottom: 10,
  },
  btnPrimaryText: {
    fontFamily: mainFont,
    fontSize: 13,
    letterSpacing: 0.3,
    fontWeight: '600',
  },
  btnDelete: {
    backgroundColor: color5,
  },
  btnDisable: {
    opacity: 0.5,
  },
  btnWhite:{
    backgroundColor:'#fff',
    marginBottom:20,
    height:40
  },
  btnWhiteText:{
    fontWeight:'600',
    color:color1,
    fontSize:16,
    fontFamily:mainFont
  },
  btnLink:{
    backgroundColor:'transparent',
    marginBottom:20
  },
  btnLinkText:{
    fontWeight:'300',
    color:'#fff',
    fontSize:16
  },
  stackedInputIconContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  stackedInputIconContainerInvalid: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  stackedInputIcon: {
    marginTop: 0,
    paddingRight: 0,
    color: color1,
    marginLeft: 15,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    alignSelf: 'center',
  },
  selectIcon: {
    color: color2,
    opacity: 0.7,
  },
  selectIconBlue: {
    color: color1,
    opacity: 1,
  },
  selectRightText: {
    fontFamily: mainFont,
    fontSize: 17,
    lineHeight: 14,
    color: color2,
  },
  fileList: {

  },
  fileListItem: {
    marginLeft: 0,
    height: 51,
    borderBottomWidth: 2,
    borderColor: borderColor1,
  },
  fileListItemLeft: {
    paddingRight: 11,
  },
  fileListItemBody: {
    borderBottomWidth: 0,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  fileListItemRight: {
    borderBottomWidth: 0,
    paddingRight: 0,
  },
  fileListItemText: {
    fontFamily: mainFont,
    fontSize: 16,
    color: color2,
    fontWeight: '600',
    marginRight: 0,
    flex: 0,
    flexShrink: 1,
  },
  fileListItemDate: {
    color: 'rgb(159, 172, 192)',
    flexShrink: 0,
  },
  fileListItemRightIcon: {
    color: 'rgb(145, 159, 182)',
  },
  fileListItemLeftIcon: {
    color: 'rgb(57, 63, 98)',
    opacity: 0.3,
  },
  fileListItemLast: {
    marginBottom: 20,
  },
  profileLinks: {
    flexDirection: 'row',
    borderBottomWidth: 2,
    borderColor: borderColor1,
  },
  profileLinksItem: {
    flex: 1,
    height: 78,
    flexDirection: 'column',
    justifyContent: 'flex-start',
    alignItems: 'center',
    borderRightWidth: 2,
    borderColor: borderColor1,
    paddingLeft: 4,
    paddingRight: 4,
    paddingTop: 12,
    backgroundColor:'transparent'
  },
  profileLinksItemImage: {
    marginBottom: 8,
  },
  profileLinksItemText: {
    fontFamily: mainFont,
    fontSize: 11,
    color: color5,
    textAlign: 'center',
    lineHeight:11
  },
  profileLinksItemTextActive: {
    color: color1,
  },
  inlineInputSpace: {
    borderBottomWidth: 0,
  },
  photoBox: {
    marginLeft: -20,
    marginRight: -20,
    flexDirection: 'row',
    flexWrap: 'wrap',
  },
  photoBoxItem: {
    width: 93,
    height: 93,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    marginRight: 1,
  },
  photoBoxAdd: {
    borderWidth: 1,
    borderColor: 'rgb(80, 155, 235)',
    marginRight: 0,
  },
  photoBoxAddIcon: {
    fontSize: 37,
    color: 'rgb(80, 155, 235)',
  },
  tabDot: {
    backgroundColor: color5,
    width: 8,
    height: 8,
    opacity: 0.3,
    borderRadius: 4,
  },
  tabDotActive: {
    backgroundColor: '#000',
    width: 8,
    height: 8,
    opacity: 0.3,
    borderRadius: 4,
  },
  tabHeading: {
    flex: 1,
    borderBottomWidth: 0,
    justifyContent: 'center',
    padding: 10,
    backgroundColor: '#fff',
  },
  tabHeadingLast: {
    justifyContent: 'flex-start',
  },
  tabs: {
    flex:1
  },
  planBox: {

  },
  planBoxHeader: {
    height: 126,
    backgroundColor: color1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    shadowColor: 'rgba(0, 0, 0, 0.1)',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 1,
    shadowRadius: 7,
  },
  planBoxHeaderTitle: {
    fontSize: 38,
    color: '#fff',
    fontFamily: mainFont,
    fontWeight: '600',
  },
  planBoxHeaderSubTitle: {
    fontSize: 16,
  },
  planBoxBody: {
    width: 335,
    height: 308,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 10,
    paddingLeft: 34,
    paddingRight: 34,
  },
  planBoxBodyTitle: {
    fontSize: 27,
    lineHeight: 28,
    fontFamily: mainFont,
    color: '#fff',
    fontWeight: '600',
    textAlign: 'center',
  },
  planBoxBodySubTitle: {
    fontSize: 18,
    fontWeight: '400',
  },
  planBoxBodyImage: {
    flex: 1,
    position: 'absolute',
    width: 335,
    height: 308,
  },
  planBoxBodyImageShadow: {
    backgroundColor: 'rgb(0, 0, 0)',
    flex: 1,
    width: 335,
    height: 308,
    position: 'absolute',
    opacity: 0.2,
  },
  tabContent: {
    paddingLeft: 20,
    paddingRight: 20,
    paddingTop:10,
    paddingBottom:10
  },
  tabsTitle: {
    fontSize: 17,
    color: color3,
    fontFamily: mainFont,
    textAlign: 'center',
    marginTop: 20,
  },
  labelMarginBottom: {
    marginBottom: 13,
  },
  inputMultiline: {
    height: 'auto',
    marginBottom: 20,
  },
  checkboxText: {
    fontSize: 17,
    color: color3,
    fontFamily: mainFont,
    marginLeft:0,
    marginRight:0
  },
  headerTitle: {
    fontFamily: mainFont,
    fontSize: 16,
    fontWeight: 'bold',
    color: '#4f9beb',
  },
  rightHeaderButton: {
    fontFamily: mainFont,
    fontSize: 12,
    fontWeight: '500',
    color: '#4f9beb',
  },
  labelCheckbox:{
    marginTop:20
  },
  buttonHide:{
    backgroundColor:'transparent',
    height:'auto',
    paddingTop:0,
    paddingBottom:0,
    marginTop:15

  },
  buttonHideText:{
    fontSize:12,
    color:color5
  },
  buttonHideContainer:{
    flexDirection:'row',
    justifyContent:'center',
    flex:10
  },
  exercisesListItemChangeCountIcon:{
    fontSize:20,
    color:'#9dabc1'
  },
  exercisesListItemPlusIcon:{
    marginRight:13
  },
  exercisesListItemMinusIcon:{
    marginLeft:13
  },
  exercisesListItemTickWrap:{
    flexDirection:'row'
  },
  exercisesListItemTickIcon:{
    alignSelf:'flex-start',
    marginTop:13,
    fontSize:24,
    marginRight:8,
    color:color1,
  },
  toggleBox:{
    backgroundColor:'rgba(57, 63, 98, 0.05)',
    marginBottom:10
  },
  toggleBoxRed:{
    backgroundColor:'rgba(255, 121, 121, 0.1)',
  },
  toggleBoxInForm:{
    marginRight:-20,
    marginLeft:-20
  },
  toggleContent:{

  },
  toggleContentTitle:{
    color:color5,
    fontSize:13,
    lineHeight:13,
    fontWeight:'600'
  },
  invoiceItem:{
    shadowColor:'rgba(0, 0, 0, 0.1)',
    shadowRadius:6,
    shadowOpacity:1,
    shadowOffset:{
      width: 0,
      height:0
    },
    paddingTop:9,
    paddingLeft:13,
    paddingRight:13,
    paddingBottom:20,
    marginTop:10
  },
  invoiceItemHeader:{
    flexDirection:'column',
    justifyContent:'space-between',
    alignItems:'stretch'
  },
  invoiceItemHeaderText:{
    flexDirection:'row',
    alignItems:'center',
  },
  invoiceItemHeaderTextMargin:{
    marginBottom:10
  },
  invoiceItemHeaderImage:{
    marginRight:6,
    width:30,
    height:30,
  },
  invoiceItemTitle:{
    color:color1,
    fontSize:13,
    flex:10,
    fontWeight:'bold'
  },
  invoiceItemInfoTitle:{
    fontSize:13,
    color:color3,
    lineHeight:13
  },
  invoiceItemInfoText:{
    fontSize:13,
    color:'rgb(145, 159, 182)',
    lineHeight:13
  },
  invoiceItemBody:{

  },
  invoiceItemBodyRow:{
    flexDirection:'row',
    justifyContent:'space-between',
  },
  invoiceItemBodyRowMargin:{
    paddingTop:10,
    paddingBottom:11,
    marginBottom:7,
    marginTop:0
  },
  invoiceItemText:{
    color:"rgb(102, 120, 144)",
    fontSize:15,
    lineHeight:16,
    flex:8,
  },
  invoiceItemTextRight:{
    flex:2,
    marginLeft:10
  },
  invoiceItemTextTotal:{
    textAlign:'right'
  },
  switchContainer:{
    flex:8,
    width:36,
    height:22,
    flexDirection:'row',
    alignItems:'center'
  },
  invoiceItemSwitchContainerRight:{
    flex:2,
    marginLeft:10,
    flexDirection:'row',
    justifyContent:'flex-end'
  },
  tabHeadingSquare:{
    backgroundColor:'#fff',
    borderRightWidth:2,
    borderColor:'rgba(57, 63, 98, 0.05)',
    borderBottomColor:'rgba(57, 63, 98, 0.05)',
  },
  tabHeadingSquareText:{
    fontSize:14,
    fontFamily:mainFont
  },
  toggleContentTitleMargin:{
    marginTop:20
  },
  textWarning:{
    color: color4
  },
  inlineInputRow:{
    flexDirection:'row',
    alignItems:'center',
    marginTop:10
  },
  inlineInputImage:{
    width:30,
    height:30,
    marginRight:11,
  },
  noMarginLeft:{
    marginLeft:0
  },
  noMarginRight:{
    marginRight:0
  },
  checkboxIcon:{
    alignSelf:'flex-start',
    marginTop:0,
    fontSize:24,
    marginRight:8,
    color:color1,
  },
  checkboxIconInactive:{
    color:color5,
  },
  noMarginTop:{
    marginTop:0
  },
  invoiceTotalContainer:{
    marginTop:7,
    marginBottom:20
  },
  blueText:{
    color:color1
  },
  greyText:{
    color:color5
  },
  invoiceItemTextRow:{
    flexDirection:'column',
    flex:8
  },
  invoiceItemTextRightBottom:{
    alignSelf:'flex-end'
  },
  invoiceItemTextSelectAll:{
    fontSize:17,
    color:color5,
    alignSelf:'center'
  },
  refreshIcon:{
    transform: [{ rotate: '90deg'}],
    fontSize:15,
    marginLeft:0,
    marginRight:6,
  },
  mapMarkerIcon:{
    fontSize:15,
    marginLeft:0,
    marginRight:6,
  },
  dayContainer:{
    borderBottomWidth:2,
    borderColor:'transparent',
    marginRight:10,
    marginLeft:10,
    backgroundColor:'transparent',
    paddingLeft:0,
    paddingTop:0,
    paddingRight:0,
    paddingBottom:5,
    height:'auto',
    minWidth:30,
    justifyContent:'center'
  },
  dayContainerActive:{
    borderBottomWidth:2,
    borderColor:color1
  },
  dayText:{
    fontSize:11,
    lineHeight:11,
    letterSpacing:1,
    color:color5
  },
  dayTextActive:{
    color:color1
  },
  dayList:{
    marginTop:20,
    // marginBottom:20,
    flexDirection:'row',
    justifyContent:'space-between',
    alignItems:'center'
  },
  tabContentCalendar:{

  },
  calendar:{
    maxHeight:311,
    borderBottomWidth:2,
    borderColor:'rgba(57, 63, 98, 0.05)',
    marginBottom:15,
    flex:5
  },
  inputSearch:{
    backgroundColor:'#f3f3f5',
  },
  searchResultsItemIcon:{
    width:30,
    height:30
  },
  searchResultsItem:{
    height:47
  },
  searchResultsItemText:{
    fontSize:15,
    lineHeight:15,
    color:color3
  },
  headerRightIcon:{
    marginLeft:23,
    fontSize:20,
    color:color1
  },
  headerRight:{
    flexDirection:'row',
    alignItems:'center',
    justifyContent:'flex-end',
    marginRight:10
  },
  chatView:{
    backgroundColor:'#000',
    flex:1
  },
  bubbleTimeContainer:{
    flexDirection:'row',
    position:'absolute',
    right:0,
    left:0,
    justifyContent:'flex-start',
    flex:1,
  },
  bubbleName:{
    fontSize:12,
    color:color3,
    fontWeight:'600',
    backgroundColor:'transparent',
    marginRight:5,
  },
  bubbleWrapper:{
    backgroundColor:'#000'
  },
  searchResultsContainer:{
    flexDirection:'row',
    justifyContent:'space-between',
    alignItems:'center',
    borderColor:'rgba(57, 63, 98, 0.05)',
    borderBottomWidth:2,
    paddingLeft:20,
    paddingRight:20
  },
  searchResultsList:{
    flexDirection:'row'
  },
  searchResultsListItem:{
    width:30,
    height:30,
    marginRight:10,
  },
  searchResultsButton:{
    backgroundColor:'transparent',
  },
  searchResultsButtonText:{
    color:color1,
    fontSize:12,
    lineHeight:20,
    fontWeight:'bold',
    fontFamily:mainFont
  },
  modalPrimary:{
    backgroundColor:'#fff',
    minHeight:150,
    borderRadius:5,
    borderWidth:1,
    borderColor:'transparent',
    padding:20,
    marginLeft:30,
    marginRight:30
  },
  modalPrimaryTitle:{
    textAlign:'center',
    fontFamily:mainFont,
    fontSize:16,
    lineHeight:20
  },
  modalPrimaryButtonsContainer:{
    flexDirection:'row',
    justifyContent:'center',
    alignItems:'center',
    marginTop:20
  },
  contentAppointments:{
    paddingLeft:20,
    paddingRight:20,
    flex:1
  },
  contentAppointmentsButton:{
    alignSelf:'stretch',
    flexDirection:'row',
    justifyContent:'flex-start'
  },
  dateWeekContainer:{
    flexDirection:'row',
    justifyContent:'space-between',
    alignItems:'center',
    borderBottomWidth: 2,
    borderTopWidth: 2,
    borderColor: borderColor1,
    height:43
  },
  dateWeekContainerIcon:{
    fontSize:30,
    color:color1
  },
  dateWeekContainerButtonArrow:{
    backgroundColor:'transparent',
    paddingTop:0,
    paddingBottom:0,
    paddingLeft:0,
    paddingRight:0,
    height:'auto',
    alignSelf:'center'
  },
  dateWeekContainerText:{
    color:color2,
    fontFamily:mainFont,
    fontSize:15,
    fontWeight:'500'
  },
  radio:{
    borderColor:'#b8c2d4',
    borderWidth:2,
    borderRadius:10,
    width:20,
    height:20,
    flexDirection:'row',
    justifyContent:'center',
    alignItems:'center',
    marginRight:8
  },
  radioSelected:{
    borderColor:color1,
  },
  radioDot:{
    backgroundColor:color1,
    width:8,
    height:8,
    borderRadius:4
  },
  btnModal:{
    alignSelf:'stretch',
    backgroundColor:'rgba(248, 248, 248, 0.82)',
    borderRadius:13,
    height:58,
    justifyContent:'center',
    marginBottom:0
  },
  btnModalBordered:{
    borderColor:'rgba(0, 0, 0, 0.5)',
    borderTopWidth:0.5,
    borderRadius:0
  },
  btnModalText:{
    color:color1,
    fontSize:20,
  },
  modalBottom:{
    padding:0,
    borderRadius:13,
    backgroundColor:'rgba(248, 248, 248, 0.82)',
  },
  modalBottomTitle:{
    paddingTop:16,
    paddingBottom:16,
    backgroundColor:'transparent',
    color:'rgb(143, 142, 148)',
    fontSize:13
  },
  whiteTitle:{
    fontSize:20,
    color:'#fff',
    marginBottom:15
  },
  whiteSubTitle:{
    fontSize:17,
    color:'#fff',
    marginBottom:15,
    fontWeight:'300'
  },
  // Fitness Programs --- Start
  programItem: {
    width: 335,
    height: 160,
    paddingTop: 15,
    paddingBottom: 25,
    paddingLeft: 20,
    paddingRight: 5,
    marginBottom: 5
  },
  programImage: {
    flex: 1,
    position: 'absolute',
    width: 335,
    height: 160
  },
  programItemHeader: {
    flex: 1,
    justifyContent: 'flex-start',
    flexDirection: 'row'
  },
  programLevelBlock: {
    borderRadius: 100,
    backgroundColor: transpWhite,
    paddingLeft: 15,
    paddingRight: 15,
    paddingTop: 5,
    paddingBottom: 5,
    alignSelf: 'flex-start'
  },
  programLevelContainer: {
    flex: 4
  },
  programButtonBlock: {
    flex: 2,
    justifyContent: 'space-around',
    alignItems: 'flex-start',
    flexDirection: 'row',
  },
  buttonRound: {
    width: 40,
    height: 30,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'transparent'
  },
  programLevelTitle: {
    color: grey,
    fontSize: 12,
    fontFamily: mainFont
  },
  programItemBody: {
    flex: 1,
    justifyContent: 'flex-end',
    flexDirection: 'column'
  },
  programBodyTitle: {
    color: white,
    fontSize: 18,
    fontFamily: mainFont,
    fontWeight: 'bold'
  },
  programBodySubTitle: {
    color: white,
    fontSize: 13,
    fontFamily: italicFont,
    opacity: 0.9
  },
  // Fitness Programs --- End
  // Exercise --- Start
  exerciseContainer: {
    marginTop: 15
  },
  exerciseTitleBlock: {
    marginLeft: 0
  },
  exerciseTitle: {
    color:color5,
    fontSize:13,
    lineHeight:13,
    fontWeight:'600',
    paddingBottom: 10
  },
  exerciseItem: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    flexBasis: 'auto',
    marginLeft: 0
  },
  exerciseLabel: {
    color:color2,
    fontSize:15,
    lineHeight:20,
    fontWeight: '600',
    paddingTop: 15,
    paddingBottom: 4
  },
  exerciseName: {
    fontSize: 15,
    color: color2,
    fontFamily: lightFont,
    lineHeight: 14,
    paddingTop: 4,
    paddingBottom: 20
  },
  buttonMore: {
    height: 20,
    paddingLeft: 20,
    backgroundColor: 'transparent',
    alignSelf: 'center'
  },
  modalAddExercise: {
    width: 335,
    minHeight: 150,
    margin: 0,
    paddingLeft: 25,
    backgroundColor: 'rgb(250, 250, 250)',
    shadowColor: 'black',
    shadowOffset: { width: 0, height: 8 },
    shadowOpacity: 0.24,
    shadowRadius: 8
  },
  hitDropDownItem: {
    paddingTop: 10,
    paddingBottom: 10
  },
  hitDropDownItemText: {
    color: 'rgb(97, 114, 136)',
    fontSize: 15
  },
  hitDropDown: {
    width: 335,
    height: 150,
    position: 'absolute',
    zIndex: 1000,
    top: 80,
    borderRadius: 2,
    backgroundColor: 'rgb(250, 250, 250)',
    shadowColor: 'black',
    shadowOffset: { width: 0, height: 8 },
    shadowOpacity: 0.24,
    shadowRadius: 8
  },
  hitDropDownScroll: {
    paddingLeft: 25,
    paddingTop: 5
  },
  // Exercise --- End
  // Drop Down Menu --- Start
  modal: {
    width: 140,
    margin: 0,
    marginLeft: '51%',
    paddingTop: 5,
    paddingBottom: 5,
    paddingLeft: 0,
    paddingRight: 0,
    backgroundColor: 'rgb(250, 250, 250)',
    shadowColor: 'black',
    shadowOffset: { width: 0, height: 8 },
    shadowOpacity: 0.24,
    shadowRadius: 8
  },
  edit: {
    margin: 0,
    backgroundColor: 'rgb(238, 238, 238)',
    paddingTop: 10,
    paddingBottom: 10,
    paddingLeft: 25
  },
  delete: {
    margin: 0,
    paddingTop: 10,
    paddingBottom: 10,
    paddingLeft: 25
  }
  // Drop Down Menu --- End
};

/* eslint-disable no-restricted-syntax*/
export const combineStyles = (...args) => {
  const objectStyles = {};
  args.forEach((item) => {
    for (const key in item) {
      if (Object.prototype.hasOwnProperty.call(item, key)) {
        objectStyles[key] = item[key];
      }
    }
  });
  return objectStyles;
};
/* eslint-enable no-restricted-syntax*/

export default styles;
