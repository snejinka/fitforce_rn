import Realm from 'realm';
import Platform from 'react-native';
import RNFS from 'react-native-fs';
import { sessionStatus } from '../constants';

class User {}
User.schema = {
    name: 'User',
    primaryKey: 'id',
    properties: {
        id: {type:'string',optional:true},
        firstName: {type:'string',optional:true},
        lastName: {type:'string',optional:true},
        email: {type:'string',optional:true},
        idToken: {type:'string',optional:true},
        accessToken: {type:'string',optional:true},
        refreshToken: {type:'string',optional:true},
        phone: {type:'string',optional:true},
        address: {type:'string',optional:true},
        lengthUnit: {type:'string',optional:true},
        weightUnit: {type:'string',optional:true},
        currency: {type:'string',optional:true,default:'USD'},
        taxRate: {type:'string',optional:true},
        session: {type:'string',optional:true,default:sessionStatus.guest},
        subscription_plan: {type:'string',optional:true,default:'free'},
        stripe_customer_id: {type:'string',optional:true},
        max_clients_count: {type:'int',optional:true,default:3},
    },
};


class Appointment {}
Appointment.schema = {
    name:'Appointment',
    primaryKey: 'id',
    properties: {
        id: {type:'string'},
        currency: {type:'string',optional:true},
        serial_id: {type:'string',optional:true},
        amount_in_cents: {type:'int',optional:true},
        client_id: {type:'string'},
        frequency: {type:'string',optional:true},
        type: {type:'string',optional:true},
        address: {type:'string',optional:true},
        time_start: {type:'string',optional:true},
        time_end: {type:'string',optional:true},
        date: {type:'string',optional:true},
        description: {type:'string',optional:true},
        fitness_programm: {type:'string',optional:true},
        synchronized:{type:'bool',optional:true, default:false},
        deleted:{type:'bool',optional:true, default:false},
        new:{type:'bool',optional:true, default:false},

    },
}


class Client {}
Client.schema = {
    name:'Client',
    primaryKey: 'id',
    properties: {
        medical_conditions:  {type:'string',optional:true},
        last_name:  {type:'string',optional:true},
        first_name:  {type:'string',optional:true},
        status:  {type:'string',optional:true},
        notes:  {type:'string',optional:true},
        DOB:  {type:'string',optional:true},
        phone_number:  {type:'string',optional:true},
        gender:  {type:'string',optional:true},
        invite_code:{type:'string',optional:true},
        address:{type:'string',optional:true},
        id:  {type:'string',optional:true},
        email:{type:'string',optional:true},
        synchronized:{type:'bool',optional:true, default:false},
        deleted:{type:'bool',optional:true, default:false},
        new:{type:'bool',optional:true, default:false},
    },
}

class Exercise {}
Exercise.schema = {
    name:'Exercise',
    properties: {
        id: {type:'number',optional:true},
        name: {type:'string',optional:true},
        body_part: {type:'string',optional:true},
        rest: {type:'int',optional:true},
        reps: {type:'int',optional:true},
        sets: {type:'int',optional:true},
        weight: {type:'int',optional:true}
    },
}

class FitnessProgram {}
FitnessProgram.schema = {
    name:'FitnessProgram',
    primaryKey: 'id',
    properties: {
        id: {type:'string',optional:true},
        name: {type:'string',optional:true},
        program_type: {type:'int',optional:true},
        description: {type:'string',optional:true},
        program_level: {type:'int',optional:true},
        // linkToObject:'Exercise',
        exercises: {type:'string',optional:true},
        // objectsLinkingToThisObject: {type: 'linkingObjects', objectType: 'Exercise', property: 'linkToObject'}
    },
}

export default new Realm({
    path: Platform.OS === 'ios'
        ? RNFS.MainBundlePath + '/default.realm'
        : RNFS.DocumentDirectoryPath + '/default.realm',
    schema: [User, Client, Appointment, FitnessProgram], schemaVersion: 41,
});