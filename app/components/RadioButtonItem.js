import React, {Component} from 'react';
import PropTypes from 'prop-types'
import {Image,View,TouchableOpacity} from 'react-native';
import styles from '../assets/styles/index';
import {
    ListItem,
    Radio,
    Body,
    Text
} from 'native-base';

class RadioButtonItem extends Component {
    constructor(props){
        super(props);
    }
    static propTypes = {
        text: PropTypes.string.isRequired,
        textStyle: PropTypes.object.isRequired,
        handleClick: PropTypes.func.isRequired,
        listItemStyle: PropTypes.array,
        selected: PropTypes.bool,
    }

    handleClick(){
        this.props.handleClick() 
    }

    renderRadio(){
        if(!this.props.selected){
            return(
                <TouchableOpacity onPress={() => this.handleClick()} style={[styles.radio]}/>
            )
        }else {
            return(
                <TouchableOpacity onPress={() => this.handleClick()} style={[styles.radio,styles.radioSelected]}>
                    <View style={[styles.radioDot]}/>
                </TouchableOpacity>

            )
        }
    }

    render() {
        return (
            <ListItem style={this.props.listItemStyle}>
                {this.renderRadio()}
                <Body>
                <Text style={this.props.textStyle}>{this.props.text}</Text>
                </Body>
            </ListItem>
        );
    }
}

export default RadioButtonItem;
