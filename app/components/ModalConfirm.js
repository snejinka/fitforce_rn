import React, { Component } from 'react';
import PropTypes from 'prop-types'
import {
  Text,
  Button,
} from 'native-base';
import { Image, View, ScrollView } from 'react-native';
import styles from '../assets/styles/index';
import Modal from 'react-native-modal'


class ModalConfirm extends Component {

  render() {
    const {isVisible,secondLineText,handleNo,handleYes,buttonNoText,buttonYesText,firstLineText,handleClose} = this.props;
    return (
        <Modal isVisible={isVisible} onBackdropPress={() => handleClose()}>
          <View style={styles.modalPrimary}>
            <Text style={styles.modalPrimaryTitle}>{firstLineText}</Text>
            <Text style={styles.modalPrimaryTitle}>{secondLineText}</Text>
            <View style={styles.modalPrimaryButtonsContainer}>
              <Button style={[styles.btnPrimary,{margin:10}]} block onPress={() => handleNo()}>
                <Text style={styles.btnPrimaryText}>{buttonNoText}</Text>
              </Button>
              <Button style={[styles.btnPrimary,{margin:10}]} block onPress={() => handleYes()}>
                <Text style={styles.btnPrimaryText}>{buttonYesText}</Text>
              </Button>
            </View>
          </View>
        </Modal>
    );
  }
}

ModalConfirm.PropTypes = {
  isVisible:PropTypes.bool,
  firstLineText:PropTypes.string,
  secondLineText:PropTypes.string,
  buttonNoText:PropTypes.string,
  buttonYesText:PropTypes.string,
  handleNo:PropTypes.func,
  handleYes:PropTypes.func,
  handleClose:PropTypes.func,
}

ModalConfirm.defaultProps = {
  buttonNoText: 'No',
  buttonYesText: 'Yes',
  firstLineText: 'Are you sure you want to delete',
}

export default ModalConfirm;
