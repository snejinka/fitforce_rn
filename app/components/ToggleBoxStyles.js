import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  touchableContainer: {
    flex: 1,
    flexDirection: 'row',
  },
  box: {
    flex: 1,
    overflow: 'hidden',
  },
  titleContainer: {
    flexDirection: 'row',
    paddingLeft: 20,
    paddingRight: 20,
    justifyContent: 'space-between',
    alignItems: 'center',
    height:44
  },
  label: {
    flex: 5,
    fontSize:12,
    lineHeight:12,
    letterSpacing:1.2,
    color:'rgb(102, 120, 144)',
  },
  value: {
    flex: 5,
    fontWeight: 'bold',
    textAlign: 'right',
  },
  buttonImage: {
    flex: 1,
    textAlign: 'right',
  },
  body: {
    paddingTop: 10,
    paddingBottom: 10,
    paddingLeft: 20,
    paddingRight: 20,
    backgroundColor:'#fff',
    flex:1
  }
})
