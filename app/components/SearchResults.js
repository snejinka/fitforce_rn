import React, { Component } from 'react';
import {
    Container,
    Content,
    Text,
    ListItem,
    Body,
    List,
    Left,
    Thumbnail
} from 'native-base';
import { View, ScrollView, Image } from 'react-native';
import styles from '../assets/styles/index';
import Header from '../components/HeaderSearch';
import Avatar from '../assets/images/appointmentAvatar1.png';


class SearchResults extends Component {
  render() {
    return (
        <Container>
          <Header title="Putting on Winter Mass" />
          <Content>
            <ScrollView style={{ ...styles.scrollviewNopadding }}>
              <List style={styles.userList}>
                <ListItem avatar style={{ ...styles.userListItem, ...styles.searchResultsItem }}>
                  <Left>
                    <Thumbnail source={Avatar} style={{...styles.userListAvatar, ...styles.searchResultsItemIcon}} />
                  </Left>
                  <Body style={styles.userListItemBody}>
                  <Text style={{...styles.userListUsername,...styles.searchResultsItemText}}>Anna Barber</Text>
                  </Body>
                </ListItem>
                <ListItem avatar style={{ ...styles.userListItem, ...styles.searchResultsItem }}>
                  <Left>
                    <Thumbnail source={Avatar} style={{...styles.userListAvatar, ...styles.searchResultsItemIcon}} />
                  </Left>
                  <Body style={styles.userListItemBody}>
                  <Text style={{...styles.userListUsername,...styles.searchResultsItemText}}>Ann Hawkins</Text>
                  </Body>
                </ListItem>
                <ListItem avatar style={{ ...styles.userListItem, ...styles.searchResultsItem }}>
                  <Left>
                    <Thumbnail source={Avatar} style={{...styles.userListAvatar, ...styles.searchResultsItemIcon}} />
                  </Left>
                  <Body style={styles.userListItemBody}>
                  <Text style={{...styles.userListUsername,...styles.searchResultsItemText}}>Ann Hawkins</Text>
                  </Body>
                </ListItem>
              </List>
            </ScrollView>
          </Content>
        </Container>
    );
  }
}

export default SearchResults;
