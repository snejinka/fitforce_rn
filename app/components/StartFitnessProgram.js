import React, { Component } from 'react';
import {
  Container,
  Content,
  Text,
  Form,
  Label,
  Separator,
  ListItem,
  Body,
  List,
  Left,
  Right,
  Thumbnail
} from 'native-base';
import { View, ScrollView, Image } from 'react-native';
import styles from '../assets/styles/index';
import Header from '../components/Header';
import Avatar from '../assets/images/appointmentAvatar1.png';
import IconMaterial from 'react-native-vector-icons/MaterialCommunityIcons';

class StartFitnessProgram extends Component {
  render() {
    return (
      <Container>
        <Header title="Putting on Winter Mass" />
        <Content>
          <ScrollView style={{ ...styles.scrollviewNopadding }}>
            <List style={styles.userList}>
              <ListItem avatar style={{ ...styles.userListItem, ...styles.noBorderBottom }}>
                <Left>
                  <Thumbnail source={Avatar} style={styles.userListAvatar} />
                </Left>
                <Body style={styles.userListItemBody}>
                <Text style={styles.userListUsername}>Anna Barber</Text>
                </Body>
              </ListItem>
            </List>
            <Separator bordered style={styles.separator}>
              <Text>{'Exercises'.toUpperCase()}</Text>
            </Separator>
            <Form style={styles.form}>
              <List style={styles.exercisesList}>
                <View style={styles.exercisesListLabelContainer}>
                  <Label style={styles.exercisesListLabel}>{'Quadriceps'}</Label>
                  <Label style={styles.exercisesListLabel}>{'Sets'}</Label>
                </View>
                <ListItem avatar style={styles.exercisesListItem}>
                  <View style={styles.exercisesListItemTickWrap}>
                    <IconMaterial style={{...styles.exercisesListItemTickIcon}} name="check-circle" />
                    <Body style={styles.exercisesListItemBody}>
                    <Text style={styles.exercisesListName}>Barbell lunge </Text>
                    <Text note style={styles.exercisesListDescription} ellipsizeMode="tail" numberOfLines={2}>6 Sets of 12 Reps - Rest 20 s Weight / Resistance 10 kg</Text>
                    </Body>
                    <Right style={{...styles.exercisesListItemRight, ...styles.exercisesListItemRightTop}}>
                      <Text style={styles.exercisesListSetsText}>6/6</Text>
                    </Right>
                  </View>

                </ListItem>
                <ListItem avatar style={styles.exercisesListItem}>
                  <View style={styles.exercisesListItemTickWrap}>
                    <IconMaterial style={{...styles.exercisesListItemTickIcon}} name="check-circle" />
                    <Body style={styles.exercisesListItemBody}>
                    <Text style={styles.exercisesListName}>Kumar Pratik</Text>
                    <Text note style={styles.exercisesListDescription} ellipsizeMode="tail" numberOfLines={2}>6 Sets of 12 Reps - Rest 20 s Weight / Resistance 10 kg </Text>
                    </Body>
                    <Right style={{...styles.exercisesListItemRight, ...styles.exercisesListItemRightTop}}>
                      <Text style={styles.exercisesListSetsText}>6/6</Text>
                    </Right>
                  </View>
                </ListItem>
                <View style={styles.exercisesListLabelContainer}>
                  <Label style={styles.exercisesListLabel}>{'Shoulders'}</Label>
                </View>
                <ListItem avatar style={styles.exercisesListItem}>
                  <View style={styles.exercisesListItemTickWrap}>
                    <IconMaterial style={{...styles.exercisesListItemTickIcon}} name="check-circle" />
                    <Body style={styles.exercisesListItemBody}>
                    <Text style={styles.exercisesListName}>Barbell lunge </Text>
                    <Text note style={styles.exercisesListDescription} ellipsizeMode="tail" numberOfLines={2}>6 Sets of 12 Reps - Rest 20 s Weight / Resistance 10 kg </Text>
                    </Body>
                    <Right style={{...styles.exercisesListItemRight, ...styles.exercisesListItemRightTop}}>
                      <Text style={styles.exercisesListSetsText}>6/6</Text>
                    </Right>
                  </View>
                </ListItem>
                <ListItem avatar style={styles.exercisesListItem}>
                  <View style={styles.exercisesListItemTickWrap}>
                    <IconMaterial style={{...styles.exercisesListItemTickIcon}} name="check-circle" />
                    <Body style={styles.exercisesListItemBody}>
                    <Text style={styles.exercisesListName}>Kumar Pratik</Text>
                    <Text note style={styles.exercisesListDescription} ellipsizeMode="tail" numberOfLines={2}>6 Sets of 12 Reps - Rest 20 s Weight / Resistance 10 kg </Text>
                    </Body>
                    <Right style={{...styles.exercisesListItemRight, ...styles.exercisesListItemRightTop}}>
                      <Text style={styles.exercisesListSetsText}>6/6</Text>
                    </Right>
                  </View>
                </ListItem>
                <ListItem avatar style={styles.exercisesListItem}>
                  <View style={styles.exercisesListItemTickWrap}>
                    <IconMaterial style={{...styles.exercisesListItemTickIcon}} name="check-circle" />
                    <Body style={styles.exercisesListItemBody}>
                    <Text style={styles.exercisesListName}>Kumar Pratik</Text>
                    <Text note style={styles.exercisesListDescription} ellipsizeMode="tail" numberOfLines={2}>6 Sets of 12 Reps - Rest 20 s Weight / Resistance 10 kg </Text>
                    </Body>
                    <Right style={{...styles.exercisesListItemRight, ...styles.exercisesListItemRightTop}}>
                      <IconMaterial style={{...styles.exercisesListItemChangeCountIcon,...styles.exercisesListItemPlusIcon}} name="plus-circle" />
                      <Text style={styles.exercisesListSetsText}>3</Text>
                      <IconMaterial style={{...styles.exercisesListItemChangeCountIcon,...styles.exercisesListItemMinusIcon}} name="minus-circle" />
                    </Right>
                  </View>
                </ListItem>
              </List>
            </Form>
          </ScrollView>
        </Content>
      </Container>
    );
  }
}

export default StartFitnessProgram;
