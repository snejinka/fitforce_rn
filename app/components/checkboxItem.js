import React, {Component} from 'react';
import PropTypes from 'prop-types'
import {Image} from 'react-native';
import {
    ListItem,
    CheckBox,
    Body,
    Text
} from 'native-base';

class CheckboxItem extends Component {
    constructor(props){
        super(props);
        this.state = {
            checked: props.checked
        }
    }
    static propTypes = {
        text: PropTypes.string,
        textStyle: PropTypes.object,
        handleClick: PropTypes.func.isRequired,
        listItemStyle: PropTypes.array,
        checked: PropTypes.bool,
    }

    handleClick(){
        this.setState({
            checked:!this.state.checked
        })
        this.props.handleClick() 
    }

    render() {
        return (
            <ListItem style={this.props.listItemStyle}>
                <CheckBox  onPress={() => this.handleClick()} checked={this.state.checked}/>
                <Body>
                <Text style={this.props.textStyle}>{this.props.text}</Text>
                </Body>
            </ListItem>
        );
    }
}


export default CheckboxItem;
