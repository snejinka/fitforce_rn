import React, { Component } from 'react';
import PropTypes from 'prop-types'
import {
  Text,
  Button,
} from 'native-base';
import { Image, View, ScrollView } from 'react-native';
import styles from '../assets/styles/index';
import Modal from 'react-native-modal'


class ModalBottom extends Component {

  render() {
    const {isVisible,secondLineText,handleFirst,handleSecond,title,firstLineText,handleCancel} = this.props;
    return (
        <Modal isVisible={isVisible} style={{justifyContent:'flex-end'}} onBackdropPress={() => handleCancel()}>
          <View style={[styles.modalPrimary,styles.modalBottom ]}>
            <Text style={[styles.modalPrimaryTitle,styles.modalBottomTitle]}>{title}</Text>
            <Button 
                onPress={() => handleFirst()} 
                style={[styles.btnPrimary,styles.btnModal,styles.btnModalBordered,{backgroundColor:'transparent'}]}>
              <Text style={[styles.btnPrimaryText,styles.btnModalText,{fontWeight:'400'}]}>{firstLineText}</Text>
            </Button>
            <Button 
                onPress={() => handleSecond()} 
                style={[styles.btnPrimary,styles.btnModal,styles.btnModalBordered,{backgroundColor:'transparent'}]}>
              <Text style={[styles.btnPrimaryText,styles.btnModalText,{fontWeight:'400'}]}>{secondLineText}</Text>
            </Button>
          </View>
          <Button style={[styles.btnPrimary,styles.btnModal,{marginTop:8,}]} onPress={() => handleCancel()}>
            <Text style={[styles.btnPrimaryText,styles.btnModalText]}>Cancel</Text>
          </Button>
        </Modal>

    );
  }
}

ModalBottom.PropTypes = {
  isVisible:PropTypes.bool,
  title:PropTypes.string,
  firstLineText:PropTypes.string,
  secondLineText:PropTypes.string,
  handleFirst:PropTypes.func,
  handleSecond:PropTypes.func,
  handleCancel:PropTypes.func,
}

export default ModalBottom;
