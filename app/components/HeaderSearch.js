import React from 'react';
import {
  Header as NativeBaseHeader,
  Button,
  Icon,
  Text,
  Left,
  Body,
  Right,
    Item,
    Input
} from 'native-base';
import { Actions as NavigationActions } from 'react-native-router-flux';
import styles from '../assets/styles/index';

const HeaderSearch = props => (
  <NativeBaseHeader searchBar rounded style={{backgroundColor:'#fff'}}>
      <Button
          transparent
          onPress={() => NavigationActions.pop()}
      >
          <Icon ios="ios-arrow-back" android="md-arrow-back" style={{color:"rgb(79, 155, 235)", marginRight:23 }} />
      </Button>
      <Item style={styles.inputSearch}>
          <Icon name="ios-search" style={{fontSize:22, color:"rgb(79, 155, 235)"}}/>
          <Input placeholder="Search" style={{fontSize:15}}/>
          <Icon name="ios-close" style={{fontSize:30, color:"rgb(102, 120, 144)"}}/>
      </Item>
  </NativeBaseHeader>
);

export default HeaderSearch;
