import React, { Component } from 'react';
import { Text } from 'native-base';
import { View, ScrollView, TouchableOpacity } from 'react-native';
import styles from '../assets/styles/index';

class HitDropDown extends Component {
  constructor(props) {
    super(props);
  }


capitalize = (string) => {
  return string.charAt(0).toUpperCase() + string.slice(1);
}

  render() {
    let { items, handleItemSelect } = this.props;
    return (
      <View style={styles.hitDropDown}>
        <ScrollView style={styles.hitDropDownScroll}>
          {items.map((item, i) =>
              <TouchableOpacity
                style={styles.hitDropDownItem}
                onPress={() => handleItemSelect(this.capitalize(item))}
                key={i}
                >
                <Text style={styles.hitDropDownItemText}>{item}</Text>
              </TouchableOpacity>
          )}
        </ScrollView>
      </View>
    );
  }
}

export default HitDropDown;
