import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
  Text,
  Tabs,
  Tab,
  TabHeading,
} from 'native-base';
import { Image, View, ScrollView } from 'react-native';
import styles from '../assets/styles/index';
import {getListInvoices} from '../store/invoice';
import InvoiceItem from '../components/InvoiceItem'


class TabInvoices extends Component {

  render() {
    return (
        <ScrollView>
          <Tabs style={styles.tabs} initialPage={1} tabBarUnderlineStyle={{ backgroundColor: 'transparent' }}>
            <Tab heading={<TabHeading style={styles.tabHeadingSquare}><Text style={styles.tabHeadingSquareText}>{'unpaid'.toUpperCase()}</Text></TabHeading>}>
              <View padder style={styles.tabContent}>
                <Text style={styles.toggleContentTitle}>This Month</Text>
                {this.props.invoices.map((invoice)=> {
                  if(!invoice.paid){
                    return(
                        <InvoiceItem key={invoice.id} invoice={invoice}/>
                    )
                  }
                })}
              </View>
            </Tab>
            <Tab heading={<TabHeading style={styles.tabHeadingSquare}><Text style={styles.tabHeadingSquareText}>{'paid'.toUpperCase()}</Text></TabHeading>}>
              <View padder style={styles.tabContent}>
                <Text style={styles.toggleContentTitle}>This Month</Text>
                {this.props.invoices.map((invoice)=> {
                  if(invoice.paid){
                    return(
                        <InvoiceItem key={invoice.id} invoice={invoice}/>
                    )
                  }
                })}
              </View>
            </Tab>
          </Tabs>
        </ScrollView>


    );
  }
}


const mapDispatchToProps = dispatch => bindActionCreators({
  getListInvoices
}, dispatch);

const mapStateToProps = state => ({
  user: state.user,
  invoices: state.invoice.invoices,
});

export default connect(mapStateToProps, mapDispatchToProps)(TabInvoices);
