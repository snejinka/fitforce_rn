import React, { Component } from 'react';
import {
  Content,
  Text,
  Form,
  Button,
  Icon,
} from 'native-base';
import Switch from 'react-native-customisable-switch';
import { Image, View, ScrollView } from 'react-native';
import styles from '../assets/styles/index';
import Avatar from '../assets/images/appointmentAvatar1.png';
import Appointment from '../assets/images/appointment.png';
import MessageIcon from '../assets/images/messageWhite.png';
import InvoicesIcon from '../assets/images/invoices.png';
import InvoicesIconActive from '../assets/images/invoicesBlue.png';
import FitnessRecord from '../assets/images/fitnessRecord.png';
import ToggleBox from './ToggleBox'


class Invoices extends Component {
  render() {
    return (
      <Content>
        <ScrollView style={{...styles.scrollviewNopadding }}>
          <View style={styles.uploadPhotoContainer}>
            <View style={styles.uploadPhotoMessageIconContainer}>
              <Image style={styles.uploadPhotoMessageIcon} source={MessageIcon} />
            </View>
            <Image style={styles.uploadPhotoImage} source={Avatar} />
            <Text style={styles.uploadPhotoText}>Change Photo</Text>
          </View>
          <View style={styles.profileLinks}>
            <View style={styles.profileLinksItem}>
              <Image style={styles.profileLinksItemImage} source={Appointment} />
              <Text style={styles.profileLinksItemText}>{'Appointments'.toUpperCase()}</Text>
            </View>
            <View style={styles.profileLinksItem}>
              <Image style={styles.profileLinksItemImage} source={InvoicesIconActive} />
              <Text style={{...styles.profileLinksItemText, ...styles.profileLinksItemTextActive}}>{'Invoices'.toUpperCase()}</Text>
            </View>
            <View style={styles.profileLinksItem}>
              <Image style={styles.profileLinksItemImage} source={FitnessRecord} />
              <Text style={styles.profileLinksItemText}>{'Fitness Records and Logs'.toUpperCase()}</Text>
            </View>

          </View>
          <Form style={styles.form}>
            <Button iconLeft transparent style={styles.buttonAdd}>
              <Icon name="md-add" />
              <Text style={styles.buttonAddText}>{'Create New Invoice'.toUpperCase()}</Text>
            </Button>
            <ToggleBox label={'Unpaid invoices'.toUpperCase()} style={{...styles.toggleBox, ...styles.toggleBoxInForm, ...styles.toggleBoxRed}} arrowColor="rgb(160, 173, 194)">
              <View style={styles.toggleContent}>
                <Text style={styles.toggleContentTitle}>This Month</Text>
                <View style={styles.invoiceItem}>
                  <View style={styles.invoiceItemHeader}>
                    <View style={styles.invoiceItemHeaderText}>
                      <Text style={styles.invoiceItemTitle}>Invoice #28</Text>
                    </View>
                    <Text style={styles.invoiceItemInfoTitle}>Created on: <Text style={styles.invoiceItemInfoText}>Apr 10, 2017 at 14:02</Text></Text>
                    <Text style={styles.invoiceItemInfoTitle}>Due Date: <Text style={styles.invoiceItemInfoText}>Apr 17, 2017 at 14:02</Text></Text>
                  </View>
                  <View style={styles.invoiceItemBody}>
                    <View style={{...styles.invoiceItemBodyRow, ...styles.invoiceItemBodyRowMargin}}>
                      <Text style={styles.invoiceItemText}>3 Fitness Session</Text>
                      <Text style={{...styles.invoiceItemText,...styles.invoiceItemTextRight}}>$100.00</Text>
                    </View>
                    <View style={styles.invoiceItemBodyRow}>
                      <Text style={{...styles.invoiceItemText, ...styles.invoiceItemTextTotal}}>Total:</Text>
                      <Text style={{...styles.invoiceItemText,...styles.invoiceItemTextRight}}>$100.00</Text>
                    </View>
                    <View style={styles.invoiceItemBodyRow}>
                      <Text style={{...styles.invoiceItemText, ...styles.invoiceItemTextTotal}}>Tax (7%):</Text>
                      <Text style={{...styles.invoiceItemText,...styles.invoiceItemTextRight}}>$100.00</Text>
                    </View>
                    <View style={styles.invoiceItemBodyRow}>
                      <Text style={{...styles.invoiceItemText, ...styles.invoiceItemTextTotal, ...styles.textBold}}>Total (tax incl.):</Text>
                      <Text style={{...styles.invoiceItemText,...styles.invoiceItemTextRight}}>$100.00</Text>
                    </View>
                    <View style={{...styles.invoiceItemBodyRow, ...styles.invoiceItemBodyRowMargin}}>
                      <Text style={{...styles.invoiceItemText, ...styles.invoiceItemTextTotal}}>Mark as Paid:</Text>
                      <View style={{...styles.switchContainer, ...styles.invoiceItemSwitchContainerRight}}>
                        <Switch switchWidth={36}
                                switchHeight={22}
                                buttonWidth={20}
                                buttonHeight={20}
                                padding={false}
                                activeBackgroundColor={'rgb(79, 155, 235)'}
                                inactiveBackgroundColor={'rgba(157, 171, 194, 0.4)'}
                             />
                      </View>
                    </View>
                  </View>
                </View>
              </View>
            </ToggleBox>
            <ToggleBox label={'Paid invoices'.toUpperCase()} style={{...styles.toggleBox, ...styles.toggleBoxInForm}} arrowColor="rgb(160, 173, 194)">
              <View style={styles.toggleContent}>
                <Text style={styles.toggleContentTitle}>This Month</Text>
                <View style={styles.invoiceItem}>
                  <View style={styles.invoiceItemHeader}>
                    <View style={styles.invoiceItemHeaderText}>
                      <Text style={styles.invoiceItemTitle}>Invoice #28</Text>
                    </View>
                    <Text style={styles.invoiceItemInfoTitle}>Created on: <Text style={styles.invoiceItemInfoText}>Apr 10, 2017 at 14:02</Text></Text>
                    <Text style={styles.invoiceItemInfoTitle}>Due Date: <Text style={styles.invoiceItemInfoText}>Apr 17, 2017 at 14:02</Text></Text>
                  </View>
                  <View style={styles.invoiceItemBody}>
                    <View style={{...styles.invoiceItemBodyRow, ...styles.invoiceItemBodyRowMargin}}>
                      <Text style={styles.invoiceItemText}>3 Fitness Session</Text>
                      <Text style={{...styles.invoiceItemText,...styles.invoiceItemTextRight}}>$100.00</Text>
                    </View>
                    <View style={styles.invoiceItemBodyRow}>
                      <Text style={{...styles.invoiceItemText, ...styles.invoiceItemTextTotal}}>Total:</Text>
                      <Text style={{...styles.invoiceItemText,...styles.invoiceItemTextRight}}>$100.00</Text>
                    </View>
                    <View style={styles.invoiceItemBodyRow}>
                      <Text style={{...styles.invoiceItemText, ...styles.invoiceItemTextTotal}}>Tax (7%):</Text>
                      <Text style={{...styles.invoiceItemText,...styles.invoiceItemTextRight}}>$100.00</Text>
                    </View>
                    <View style={styles.invoiceItemBodyRow}>
                      <Text style={{...styles.invoiceItemText, ...styles.invoiceItemTextTotal, ...styles.textBold}}>Total (tax incl.):</Text>
                      <Text style={{...styles.invoiceItemText,...styles.invoiceItemTextRight}}>$100.00</Text>
                    </View>
                    <View style={{...styles.invoiceItemBodyRow, ...styles.invoiceItemBodyRowMargin}}>
                      <Text style={{...styles.invoiceItemText, ...styles.invoiceItemTextTotal}}>Mark as Paid:</Text>
                      <View style={{...styles.switchContainer, ...styles.invoiceItemSwitchContainerRight}}>
                        <Switch switchWidth={36}
                                switchHeight={22}
                                buttonWidth={20}
                                buttonHeight={20}
                                padding={false}
                                activeBackgroundColor={'rgb(79, 155, 235)'}
                                inactiveBackgroundColor={'rgba(157, 171, 194, 0.4)'}
                             />
                      </View>
                    </View>
                  </View>
                </View>
              </View>
            </ToggleBox>
          </Form>
        </ScrollView>
      </Content>
    );
  }
}

export default Invoices;
