import React, { Component } from 'react';
import {
  Footer,
  Text,
  FooterTab,
  Button,
} from 'native-base';
import { Actions } from 'react-native-router-flux';
import { Image } from 'react-native';
import styles from '../assets/styles/index';
import HomeIcon from '../assets/images/house.png';
import ClientsIcon from '../assets/images/clients.png';
import MessageIcon from '../assets/images/message.png';
import CalendarIcon from '../assets/images/calendar.png';
import BillIcon from '../assets/images/bill.png';

class FooterTabs extends Component {
  render() {
    return (
      <Footer>
        <FooterTab style={styles.footerTabs}>
          <Button
            style={{ ...styles.footerTabsItem, ...styles.footerTabsItemActive }}
            onPress={Actions.dashboard}
            active
          >
            <Image style={styles.footerTabsImage} source={HomeIcon} />
            <Text style={styles.footerTabsText}>Home</Text>
          </Button>
          <Button style={styles.footerTabsItem} onPress={Actions.clients}>
            <Image style={styles.footerTabsImage} source={ClientsIcon} />
            <Text style={styles.footerTabsText}>Clients</Text>
          </Button>
          <Button style={styles.footerTabsItem} onPress={Actions.messages}>
            <Image style={styles.footerTabsImage} source={MessageIcon} />
            <Text style={styles.footerTabsText}>Messages</Text>
          </Button>
          <Button style={styles.footerTabsItem} onPress={Actions.calendarView}>
            <Image style={styles.footerTabsImage} source={CalendarIcon} />
            <Text style={styles.footerTabsText}>Calendar</Text>
          </Button>
          <Button style={styles.footerTabsItem} onPress={Actions.invoices}>
            <Image style={styles.footerTabsImage} source={BillIcon} />
            <Text style={styles.footerTabsText}>Invoices</Text>
          </Button>
        </FooterTab>
      </Footer>
    );
  }
}

export default FooterTabs;
