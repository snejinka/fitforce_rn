import React, { Component } from 'react';
import {
  Container,
  Content,
  Text,
  Form,
  Item,
  Label,
  Icon,
  ListItem,
  Body,
  Button,
  Input,
  Separator
} from 'native-base';
import { View, ScrollView, Image } from 'react-native';
import styles from '../assets/styles/index';
import Header from '../components/Header';
import Avatar from '../assets/images/appointmentAvatar1.png';
import IconMaterial from 'react-native-vector-icons/MaterialCommunityIcons';

class CreateInvoiceUnpaidSelected extends Component {
  render() {
    return (
      <Container>
        <Header title="Putting on Winter Mass" />
        <Content>
          <ScrollView style={{ ...styles.scrollviewNopadding }}>
            <Form style={styles.form}>
              <Item style={{ ...styles.inlineInput }} stackedLabel>
                <Label style={styles.label}>{'Client Name'.toUpperCase()}</Label>
                <View style={styles.inlineInputRow}>
                  <Image source={Avatar} style={styles.inlineInputImage}/>
                  <Input style={styles.input} value="Anna Barber"/>
                </View>
              </Item>
              <View style={styles.formRow}>
                <Item style={{ ...styles.inlineInput, ...styles.inlineInputMargin }} stackedLabel>
                  <Label style={styles.label}>{'Date'.toUpperCase()}</Label>
                  <Input style={styles.input} />
                </Item>
                <Item stackedLabel style={styles.inlineInput}>
                  <Label style={styles.label}>{'Due Date'.toUpperCase()}</Label>
                  <Input style={styles.input} />
                </Item>
              </View>
              <Item style={{ ...styles.inlineInput }} stackedLabel>
                <Label style={styles.label}>{'Memo'.toUpperCase()}</Label>
                <Input style={{...styles.input,...styles.inputMultiline}} multiline={true} value="Hello! That's the number of my new card: 1234 5678 91011 1213. Could you make a payment here?"/>
              </Item>
              <ListItem style={{...styles.noBorderBottom, ...styles.noMarginLeft}}>
                <IconMaterial style={{...styles.checkboxIcon, ...styles.checkboxIconInactive}} name="circle-outline" />
                <Body>
                <Text style={{...styles.checkboxText, ...styles.greyText }}>Custom Invoice</Text>
                </Body>
              </ListItem>
              <Separator bordered style={{ ...styles.separator, ...styles.separatorInForm, ...styles.noMarginTop }}>
                <Text>{'Body measurements'.toUpperCase()}</Text>
              </Separator>
              <View style={styles.invoiceTotalContainer}>
                <View style={{...styles.invoiceItemBodyRow,...styles.inlineInput, ...styles.invoiceItemBodyRowMargin}}>
                  <IconMaterial style={{...styles.checkboxIcon}} name="check-circle" />
                  <Text style={{...styles.invoiceItemText, ...styles.invoiceItemTextSelectAll}}>Select All</Text>
                </View>
                <View style={{...styles.invoiceItemBodyRow,...styles.inlineInput, ...styles.invoiceItemBodyRowMargin}}>
                  <IconMaterial style={{...styles.checkboxIcon}} name="check-circle" />
                  <View style={styles.invoiceItemTextRow}>
                    <Text style={styles.invoiceItemText}>Fri, Apr 18, 10:30 - 11:30 (1 h)</Text>
                    <Text style={{...styles.invoiceItemText, ...styles.textBold}}>5 fitness training sessions</Text>
                  </View>
                  <Text style={{...styles.invoiceItemText,...styles.invoiceItemTextRight, ...styles.invoiceItemTextRightBottom, ...styles.textBold}}>$480.00</Text>
                </View>
                <View style={{...styles.invoiceItemBodyRow,...styles.inlineInput, ...styles.invoiceItemBodyRowMargin}}>
                  <IconMaterial style={{...styles.checkboxIcon}} name="check-circle" />
                  <View style={styles.invoiceItemTextRow}>
                    <Text style={styles.invoiceItemText}>Fri, Apr 18, 10:30 - 11:30 (1 h)</Text>
                    <Text style={{...styles.invoiceItemText, ...styles.textBold}}>5 fitness training sessions</Text>
                  </View>
                  <Text style={{...styles.invoiceItemText,...styles.invoiceItemTextRight, ...styles.invoiceItemTextRightBottom, ...styles.textBold}}>$100.00</Text>
                </View>
                <View style={styles.invoiceItemBodyRow}>
                  <Text style={{...styles.invoiceItemText, ...styles.invoiceItemTextTotal}}>Total:</Text>
                  <Text style={{...styles.invoiceItemText,...styles.invoiceItemTextRight}}>$100.00</Text>
                </View>
                <View style={styles.invoiceItemBodyRow}>
                  <Text style={{...styles.invoiceItemText, ...styles.invoiceItemTextTotal}}>Tax (7%):</Text>
                  <Text style={{...styles.invoiceItemText,...styles.invoiceItemTextRight}}>$100.00</Text>
                </View>
                <View style={styles.invoiceItemBodyRow}>
                  <Text style={{...styles.invoiceItemText, ...styles.invoiceItemTextTotal, ...styles.textBold}}>Total (tax incl.):</Text>
                  <Text style={{...styles.invoiceItemText,...styles.invoiceItemTextRight, ...styles.textBold}}>$100.00</Text>
                </View>
              </View>
            </Form>
          </ScrollView>
        </Content>
      </Container>
    );
  }
}

export default CreateInvoiceUnpaidSelected;
