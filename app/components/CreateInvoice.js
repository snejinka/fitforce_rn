import React, { Component } from 'react';
import {
  Container,
  Content,
  Text,
  Form,
  Item,
  Label,
  Icon,
  ListItem,
  Body,
  Button,
  Input,
} from 'native-base';
import { View, ScrollView, Image } from 'react-native';
import styles from '../assets/styles/index';
import Header from '../components/Header';
import Avatar from '../assets/images/appointmentAvatar1.png';
import IconMaterial from 'react-native-vector-icons/MaterialCommunityIcons';

class CreateInvoice extends Component {
  render() {
    return (
      <Container>
        <Header title="Putting on Winter Mass" />
        <Content>
          <ScrollView style={{ ...styles.scrollviewNopadding }}>
            <Form style={styles.form}>
              <Item style={{ ...styles.inlineInput }} stackedLabel>
                <Label style={styles.label}>{'Client Name'.toUpperCase()}</Label>
                <View style={styles.inlineInputRow}>
                  <Image source={Avatar} style={styles.inlineInputImage}/>
                  <Input style={styles.input} value="Anna Barber"/>
                </View>
              </Item>
              <View style={styles.formRow}>
                <Item style={{ ...styles.inlineInput, ...styles.inlineInputMargin }} stackedLabel>
                  <Label style={styles.label}>{'Date'.toUpperCase()}</Label>
                  <Input style={styles.input} />
                </Item>
                <Item stackedLabel style={styles.inlineInput}>
                  <Label style={styles.label}>{'Due Date'.toUpperCase()}</Label>
                  <Input style={styles.input} />
                </Item>
              </View>
              <Item style={{ ...styles.inlineInput }} stackedLabel>
                <Label style={styles.label}>{'Memo'.toUpperCase()}</Label>
                <Input style={{...styles.input,...styles.inputMultiline}} multiline={true} value="Hello! That's the number of my new card: 1234 5678 91011 1213. Could you make a payment here?"/>
              </Item>
              <ListItem style={{...styles.noBorderBottom, ...styles.noMarginLeft}}>
                <IconMaterial style={{...styles.checkboxIcon}} name="check-circle" />
                <Body>
                  <Text style={{...styles.checkboxText, ...styles.blueText }}>Custom Invoice</Text>
                </Body>
              </ListItem>
              <View style={styles.borderBottom}>
                <Label style={{...styles.labelBordered}}>{'Invoice Line 1'}</Label>
              </View>
              <Item style={{ ...styles.inlineInput }} stackedLabel>
                <Label style={styles.label}>{'Description'.toUpperCase()}</Label>
                <View style={styles.stackedInputIconContainer}>
                  <Input style={styles.input} />
                </View>
              </Item>
              <Item style={{ ...styles.inlineInput }} stackedLabel>
                <Label style={styles.label}>{'Amount'.toUpperCase()}</Label>
                <View style={styles.stackedInputIconContainer}>
                  <Input style={styles.input} />
                </View>
              </Item>
              <View style={styles.borderBottom}>
                <Label style={{...styles.labelBordered}}>{'Invoice Line 2'}</Label>
              </View>
              <Item style={{ ...styles.inlineInput }} stackedLabel>
                <Label style={styles.label}>{'Description'.toUpperCase()}</Label>
                <View style={styles.stackedInputIconContainer}>
                  <Input style={styles.input} />
                </View>
              </Item>
              <Item style={{ ...styles.inlineInput }} stackedLabel>
                <Label style={styles.label}>{'Amount'.toUpperCase()}</Label>
                <View style={styles.stackedInputIconContainer}>
                  <Input style={styles.input} />
                </View>
              </Item>
              <Item style={{ ...styles.inlineInput, ...styles.noMarginTop }} stackedLabel>
                <Button iconLeft transparent style={styles.buttonAdd}>
                  <Icon name="md-add" />
                  <Text style={styles.buttonAddText}>{'Add Invoice Line 3 '.toUpperCase()}</Text>
                </Button>
              </Item>
              <View style={styles.invoiceTotalContainer}>
                <View style={styles.invoiceItemBodyRow}>
                  <Text style={{...styles.invoiceItemText, ...styles.invoiceItemTextTotal}}>Total:</Text>
                  <Text style={{...styles.invoiceItemText,...styles.invoiceItemTextRight}}>$100.00</Text>
                </View>
                <View style={styles.invoiceItemBodyRow}>
                  <Text style={{...styles.invoiceItemText, ...styles.invoiceItemTextTotal}}>Tax (7%):</Text>
                  <Text style={{...styles.invoiceItemText,...styles.invoiceItemTextRight}}>$100.00</Text>
                </View>
                <View style={styles.invoiceItemBodyRow}>
                  <Text style={{...styles.invoiceItemText, ...styles.invoiceItemTextTotal, ...styles.textBold}}>Total (tax incl.):</Text>
                  <Text style={{...styles.invoiceItemText,...styles.invoiceItemTextRight}}>$100.00</Text>
                </View>
              </View>
            </Form>
          </ScrollView>
        </Content>
      </Container>
    );
  }
}

export default CreateInvoice;
