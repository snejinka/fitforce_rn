import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
  Content,
  Text,
  Container,
  Icon
} from 'native-base';
import { Actions } from 'react-native-router-flux';
import { Image, View, ScrollView, TouchableOpacity } from 'react-native';
import styles from '../assets/styles/index';
import Header from '../components/Header';
import {GiftedChat, Send, Composer, Bubble, Message, MessageText, GiftedAvatar, utils, Avatar, Time, InputToolbar} from "react-native-gifted-chat";

class ChatView extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selected: undefined,
      messages: [],
    };
    this.renderBubble = this.renderBubble.bind(this)
    this.renderTime = this.renderTime.bind(this)
    this.renderMessageText = this.renderMessageText.bind(this)
    this.renderInputToolbar = this.renderInputToolbar.bind(this)
  }
  componentWillMount() {
    this.setState({
      messages: [
        {
          _id: 1,
          text: 'Hello developer',
          createdAt: new Date(),
          user: {
            _id: 2,
            name: 'React Native',
            avatar: 'https://facebook.github.io/react/img/logo_og.png',
          },
        },
      ],
    });
  }
  onSend(messages = []) {
    this.setState((previousState) => ({
      messages: GiftedChat.append(previousState.messages, messages),
    }));
  }

  renderBubble(props) {
    return (
        <Bubble
            {...props}
            textStyle={{
              right: {
                color: 'white',
                fontSize:15
              },
              left: {
                color: 'rgb(97, 114, 136)',
                fontSize:15
              }
            }}
            wrapperStyle={{
              left: {
                backgroundColor:'rgb(231, 231, 239)',
                borderBottomLeftRadius:5,
                borderTopLeftRadius: 15,
                borderTopRightRadius: 15,
                marginBottom:20,
                minWidth:80
              },
              right: {
                backgroundColor:'rgb(79, 155, 235)',
                borderBottomRightRadius:5,
                borderTopLeftRadius: 15,
                borderTopRightRadius: 15,
                marginBottom:20,
                minWidth:80
              },

            }}
            containerToPreviousStyle={{
              left: {
                borderTopLeftRadius: 15,
                borderTopRightRadius: 15,
              },
              right: {
                borderTopLeftRadius: 15,
                borderTopRightRadius: 15,
              },
            }}
        />
    );
  }

  renderTime(props){
    let name = props.messages[0].user.name
    return(
        <View style={styles.bubbleTimeContainer}>
          <Text style={styles.bubbleName}>{name}</Text>
          <Time
              {...props}
              containerStyle={{
              left: {
                flexDirection:'row',
                justifyContent:'flex-start',
                alignSelf:'flex-start',

              },
              right: {
                flexDirection:'row',
                justifyContent:'flex-end'
              },
            }}
              textStyle={{
              left: {
                color:'rgb(145, 159, 182)',
                fontSize:12,
                textAlign:'left',
              },
              right: {
                color:'rgb(145, 159, 182)',
                fontSize:12,
              },
            }}
          />
        </View>
    )
  }

  renderMessageText(props){
    return(
        <MessageText
            {...props}
            textStyle={{
               left: {
                marginTop: 13,
                marginBottom: 13,
              },
              right: {
                marginTop: 13,
                marginBottom: 13,
              }
            }}
        />
    )
  }

  renderInputToolbar(props){
    return(
        <View style={{flexDirection:'row',alignItems:'center', flex:10,alignSelf:'stretch',borderTopWidth:2, borderColor:'rgba(57, 63, 98, 0.07)', paddingLeft:20, paddingRight:20}}>
          <Icon name="md-attach" style={{flex:0, color:'rgb(79, 155, 235)', marginRight:10,fontSize:25}}/>
          <InputToolbar
              {...props}
              containerStyle={{flex:10,borderTopWidth:0}} />
        </View>
    )
  }

  renderSend(props){
    return(
        <Send {...props} containerStyle={{flex:0}}/>
    )
  }
  
  render() {
    return (
        <Container>
          <Header title="Anna Barber" />
          <GiftedChat
              style={styles.chatView}
              messages={this.state.messages}
              onSend={(messages) => this.onSend(messages)}
              renderBubble={this.renderBubble}
              renderTime={this.renderTime}
              renderMessageText={this.renderMessageText}
              renderInputToolbar={this.renderInputToolbar}
              renderSend={this.renderSend}
              renderAvatarOnTop={true}
              user={{
                _id: 1,
              }}
              showBubbleTriangles={true}
          />
        </Container>

    );
  }
}


const mapStateToProps = state => ({ user: state.user });

export default connect(mapStateToProps, null)(ChatView);
