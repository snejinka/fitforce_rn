import React, { Component } from 'react';
import {
  Container,
  Content,
  Text,
  Form,
  Item,
  Input,
  Label,
  Button,
  Icon,
  List,
  ListItem,
  Left,
  Body,
  Thumbnail,
  Separator,
} from 'native-base';
import { Image, View, ScrollView } from 'react-native';
import styles from '../assets/styles/index';
import Avatar from '../assets/images/appointmentAvatar1.png';
import Photo from '../assets/images/photo1.png';
import Header from '../components/Header';

class ViewClient extends Component {
  render() {
    return (
      <Container>
        <Header title="Fitness Record" hideButton />
        <Content>
          <ScrollView style={{ ...styles.scrollview, ...styles.scrollviewFull, ...styles.scrollviewNopadding }}>
            <List style={styles.userList}>
              <ListItem avatar style={{ ...styles.userListItem, ...styles.noBorderBottom }}>
                <Left>
                  <Thumbnail source={Avatar} style={styles.userListAvatar} />
                </Left>
                <Body style={styles.userListItemBody}>
                  <Text style={styles.userListUsername}>Anna Barber</Text>
                </Body>
              </ListItem>
            </List>
            <Separator bordered style={styles.separator}>
              <Text>{'GENERAL'.toUpperCase()}</Text>
            </Separator>
            <Form style={styles.form}>
              <View style={styles.formRow}>
                <Item style={{ ...styles.inlineInput, ...styles.inlineInputMargin }} stackedLabel>
                  <Label style={styles.label}>{'Date'.toUpperCase()}</Label>
                  <Input style={styles.input} value="Apr 27, 2017 09:00" />
                </Item>
                <Item stackedLabel style={{ ...styles.inlineInput, ...styles.inlineInputSpace }} />
              </View>
              <View style={styles.formRow}>
                <Item style={{ ...styles.inlineInput, ...styles.inlineInputMargin }} stackedLabel>
                  <Label style={styles.label}>{'Weight'.toUpperCase()}</Label>
                  <View style={styles.stackedInputIconContainer}>
                    <Input style={styles.input} value="67" />
                    <Text style={{ ...styles.stackedInputIcon, ...styles.selectRightText }}>cm</Text>
                  </View>
                </Item>
                <Item stackedLabel style={styles.inlineInput}>
                  <Label style={styles.label}>{'Height'.toUpperCase()}</Label>
                  <View style={styles.stackedInputIconContainer}>
                    <Input style={styles.input} value="67" />
                    <Text style={{ ...styles.stackedInputIcon, ...styles.selectRightText }}>cm</Text>
                  </View>
                </Item>
              </View>
              <Separator bordered style={{ ...styles.separator, ...styles.separatorInForm }}>
                <Text>{'Body measurements'.toUpperCase()}</Text>
              </Separator>
              <View style={styles.formRow}>
                <Item style={{ ...styles.inlineInput, ...styles.inlineInputMargin }} stackedLabel>
                  <Label style={styles.label}>{'Chest'.toUpperCase()}</Label>
                  <View style={styles.stackedInputIconContainer}>
                    <Input style={styles.input} value="67" />
                    <Text style={{ ...styles.stackedInputIcon, ...styles.selectRightText }}>cm</Text>
                  </View>
                </Item>
                <Item stackedLabel style={styles.inlineInput}>
                  <Label style={styles.label}>{'Waist'.toUpperCase()}</Label>
                  <View style={styles.stackedInputIconContainer}>
                    <Input style={styles.input} value="67" />
                    <Text style={{ ...styles.stackedInputIcon, ...styles.selectRightText }}>cm</Text>
                  </View>
                </Item>
              </View>
              <View style={styles.formRow}>
                <Item style={{ ...styles.inlineInput, ...styles.inlineInputMargin }} stackedLabel>
                  <Label style={styles.label}>{'L. upper arm'.toUpperCase()}</Label>
                  <View style={styles.stackedInputIconContainer}>
                    <Input style={styles.input} value="67" />
                    <Text style={{ ...styles.stackedInputIcon, ...styles.selectRightText }}>cm</Text>
                  </View>
                </Item>
                <Item stackedLabel style={styles.inlineInput}>
                  <Label style={styles.label}>{'R. upper arm'.toUpperCase()}</Label>
                  <View style={styles.stackedInputIconContainer}>
                    <Input style={styles.input} value="67" />
                    <Text style={{ ...styles.stackedInputIcon, ...styles.selectRightText }}>cm</Text>
                  </View>
                </Item>
              </View>
              <View style={styles.formRow}>
                <Item style={{ ...styles.inlineInput, ...styles.inlineInputMargin }} stackedLabel>
                  <Label style={styles.label}>{'Abdomen'.toUpperCase()}</Label>
                  <View style={styles.stackedInputIconContainer}>
                    <Input style={styles.input} value="67" />
                    <Text style={{ ...styles.stackedInputIcon, ...styles.selectRightText }}>cm</Text>
                  </View>
                </Item>
                <Item stackedLabel style={styles.inlineInput}>
                  <Label style={styles.label}>{'Hips'.toUpperCase()}</Label>
                  <View style={styles.stackedInputIconContainer}>
                    <Input style={styles.input} value="67" />
                    <Text style={{ ...styles.stackedInputIcon, ...styles.selectRightText }}>cm</Text>
                  </View>
                </Item>
              </View>
              <View style={styles.formRow}>
                <Item style={{ ...styles.inlineInput, ...styles.inlineInputMargin }} stackedLabel>
                  <Label style={styles.label}>{'L. Thigh'.toUpperCase()}</Label>
                  <View style={styles.stackedInputIconContainer}>
                    <Input style={styles.input} value="67" />
                    <Text style={{ ...styles.stackedInputIcon, ...styles.selectRightText }}>cm</Text>
                  </View>
                </Item>
                <Item stackedLabel style={styles.inlineInput}>
                  <Label style={styles.label}>{'R. Thigh'.toUpperCase()}</Label>
                  <View style={styles.stackedInputIconContainer}>
                    <Input style={styles.input} value="67" />
                    <Text style={{ ...styles.stackedInputIcon, ...styles.selectRightText }}>cm</Text>
                  </View>
                </Item>
              </View>
              <View style={styles.formRow}>
                <Item style={{ ...styles.inlineInput, ...styles.inlineInputMargin }} stackedLabel>
                  <Label style={styles.label}>{'L. Calf'.toUpperCase()}</Label>
                  <View style={styles.stackedInputIconContainer}>
                    <Input style={styles.input} value="87" />
                    <Text style={{ ...styles.stackedInputIcon, ...styles.selectRightText }}>cm</Text>
                  </View>
                </Item>
                <Item stackedLabel style={styles.inlineInput}>
                  <Label style={styles.label}>{'R. Calf'.toUpperCase()}</Label>
                  <View style={styles.stackedInputIconContainer}>
                    <Input style={styles.input} value="67" />
                    <Text style={{ ...styles.stackedInputIcon, ...styles.selectRightText }}>cm</Text>
                  </View>
                </Item>
              </View>
              <Separator bordered style={{ ...styles.separator, ...styles.separatorInForm }}>
                <Text>{'CUSTOM MEASUREMENTS'.toUpperCase()}</Text>
              </Separator>
              <View style={styles.formRow}>
                <Item style={{ ...styles.inlineInput, ...styles.inlineInputMargin }} stackedLabel>
                  <Input style={styles.label} value={'New Measurement'.toUpperCase()} />
                  <View style={styles.stackedInputIconContainer}>
                    <Input style={styles.input} value="0" />
                    <Icon name="ios-arrow-down" style={{ ...styles.stackedInputIcon, ...styles.selectIcon }} />
                    <Text style={{ ...styles.stackedInputIcon, ...styles.selectRightText }}>cm</Text>
                  </View>
                </Item>
                <Item stackedLabel style={{ ...styles.inlineInput, ...styles.inlineInputSpace }} />
              </View>

              <Item style={{ ...styles.inlineInput }} stackedLabel>
                <Button iconLeft transparent style={styles.buttonAdd}>
                  <Icon name="md-add" />
                  <Text style={styles.buttonAddText}>{'Add New measurement'.toUpperCase()}</Text>
                </Button>
              </Item>
              <Separator bordered style={{ ...styles.separator, ...styles.separatorInForm }}>
                <Text>{'Photos'.toUpperCase()}</Text>
              </Separator>
              <View style={styles.photoBox}>
                <Image style={styles.photoBoxItem} source={Photo} />
                <Image style={styles.photoBoxItem} source={Photo} />
                <Image style={styles.photoBoxItem} source={Photo} />
                <View style={{ ...styles.photoBoxItem, ...styles.photoBoxAdd }}>
                  <Icon style={styles.photoBoxAddIcon} name="md-add" />
                </View>
              </View>
            </Form>
          </ScrollView>
        </Content>
      </Container>
    );
  }
}

export default ViewClient;
