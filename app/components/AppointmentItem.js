import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
  Text,
    Button
} from 'native-base';
import { Actions } from 'react-native-router-flux';
import { Image, View, ScrollView, TouchableOpacity } from 'react-native';
import styles from '../assets/styles/index';
import AppointmentAvatar from '../assets/images/appointmentAvatar1.png';
import LocationIcon from '../assets/images/pinIcon.png';
import ReloadAppointment from '../assets/images/reload.png';
import {getSelectedAppointment} from '../store/appointment'
import moment from 'moment';
import momentDuration from "moment-duration-format";

class AppointmentItem extends Component {
    constructor(props){
        super(props);
        this.client = {};
    }

    componentWillMount(){
        this.renderClientName()
    }
    
    handleAppointmentItem(){
        this.props.getSelectedAppointment(this.props.appointment.id).then(() => {
            Actions.viewAppointment({client:this.client})
        })
    }

    renderClientName(){
        this.props.clients.forEach((client) => {
            if(client.id == this.props.appointment.client_id){
                this.client = client;
            }
        })
    }

    calcInterval(){
        const {appointment} = this.props;
        var startDate = moment(appointment.time_start, 'HH:mm'),
            endDate = moment(appointment.time_end, 'HH:mm');
        return moment.duration(endDate - startDate).format('H.m')
    }

    render() {
        this.calcInterval()
      const {appointment, appointments, keyProp} = this.props;
      return (
        <TouchableOpacity
            style={appointment.status == 'accepted' ? {...styles.appointment, ...styles.appointmentMade} : {...styles.appointment}}
            onPress={() => this.handleAppointmentItem()}>
            <View style={styles.bullet} />
            {appointments.length -1 !== keyProp &&
            <View style={styles.bulletLine} />
            }
            <View style={styles.appointmentInfo}>
                <View style={styles.appointmentHeader}>
                    <Text style={styles.appointmentTime}>{appointment.time_start} - {appointment.time_end}</Text>
                    <Text style={styles.appointmentTotalTime}>({this.calcInterval()}h)</Text>
                    <Image source={ReloadAppointment} />
                </View>
                <View style={styles.appointmentBody}>
                    <Text style={styles.appointmentText}>
                        Strength Training with
                    </Text>
                    <Text style={{...styles.appointmentTextBlue,...styles.appointmentName}}>{this.client.first_name}</Text>
                </View>
                <View style={styles.appointmentFooter}>
                    <Image style={styles.appointmentLocationIcon} source={LocationIcon} />
                    <Text style={styles.appointmentTextBlue}>{appointment.address}</Text>
                </View>
            </View>
            <View style={styles.appointmentImageContainer}>
                <Image style={styles.appointmentImage} source={AppointmentAvatar} />
            </View>
        </TouchableOpacity>
    );
  }
}

AppointmentItem.propTypes = {
    clients: PropTypes.array,
    appointment: PropTypes.object,
    keyProp: PropTypes.number,
}

const mapDispatchToProps = dispatch => bindActionCreators({
    getSelectedAppointment
}, dispatch);

const mapStateToProps = state => ({
    selectedAppointment: state.appointment.selectedAppointment
});

export default connect(mapStateToProps, mapDispatchToProps)(AppointmentItem);
