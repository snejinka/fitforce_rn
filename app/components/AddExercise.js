import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
  Container,
  Content,
  Form,
  Item,
  Input,
  Label
} from 'native-base';
import { View, ScrollView } from 'react-native';
import Header from '../components/Header';
import HitDropDown from '../components/HitDropDown';
import styles from '../assets/styles/index';
import {bodyPartsMock, exercisesMock} from '../constants';
import { addExercise, editExercise } from '../store/program';

import { Actions } from 'react-native-router-flux';

class AddExercise extends Component {
  constructor(props) {
    super(props);

    this.state = {
      bodyPart: '',
      exercise: '',
      openExercises: false,
      openBodyPart: false,
      sets: '',
      reps: '',
      load: '',
      rest: ''
    }
  }

  componentWillMount = () => {
    let { edit, exercise, program } = this.props;
    if (edit) {
      this.setState({
        bodyPart: exercise.body_part,
        exercise: exercise.name,
        sets: JSON.stringify(exercise.sets ? exercise.sets : 0),
        reps: JSON.stringify(exercise.reps ? exercise.reps : 0),
        rest: JSON.stringify(exercise.rest ? exercise.rest : 0),
        load: JSON.stringify(exercise.load ? exercise.load : 0)
      })
    }
  }

  openModal = () => {
    this.setState({openExName: true});
  }

  handleBodyPartInput = (bodyPart) => {
    let bodyParts = bodyPartsMock.filter((part) => {
      if (part.includes(bodyPart)) {
        return part;
      }
    })
    this.setState({bodyPart, bodyParts, openBodyPart: true });
  }

  handleSelectBodyPart = (bodyPart) => {
    this.removeFocus();
    this.setState({bodyPart});
  }

  removeFocus = () => {
    this.setState({openExercises: false});
    this.setState({openBodyPart: false});
  }

  handleExerciseInput = (exercise) => {
    let allExercises = exercisesMock[this.state.bodyPart],
        exercises = [];
    if (allExercises) {
      exercises = allExercises.filter((exItm) => {
        if (exItm.includes(exercise.toLowerCase())) {
          return exItm;
        }
      })
    }
    this.setState({exercise, exercises, openExercises: true });
  }

  handleSelectExercise = (exercise) => {
    this.removeFocus();
    this.setState({exercise});
  }

  numberify = (text) => {
    if (text && text.match(/\d/g)) {
      return text.match(/\d/g).join('');
    } else {
      return '';
    }
  }

  handleSave = () => {
    let {bodyPart, exercise, sets, reps, rest, load} = this.state,
        {program, edit} = this.props,
        exerciseItem = this.props.exercise,
        programEx = program.exercises ? JSON.parse(program.exercises) : [],
    newExercise = {
      id: programEx.length,
      body_part: bodyPart.toLowerCase(),
      name: exercise.toLowerCase(),
      sets: sets ? sets : 0,
      reps: reps ? reps : 0,
      rest: rest ? rest : 0,
      load: load ? load : 0
    }

    if (!edit) {
      this.props.addExercise({...program, exercises: [...programEx, newExercise]}).then((res)=>{
        Actions.pop();
      });
    } else {
      let updatedExercises = programEx.filter(ex => exerciseItem.id !== ex.id);
      newExercise.id = exerciseItem.id;
      this.props.editExercise({...program, exercises: [...updatedExercises, newExercise]}).then((res)=>{
        Actions.pop();
      });
    }
  }

  capitalizeFirstLetter = (string) => {
    return string.charAt(0).toUpperCase() + string.slice(1);
  }

  render() {
    let { bodyParts,
          openBodyPart,
          bodyPart,
          exercises,
          openExercises,
          exercise,
          sets,
          reps,
          rest,
          load
        } = this.state,
        { edit } = this.props;

    return (
      <Container>
        <Header title={edit ? 'Edit Exercise' : 'Add Exercise'} rightButtonCallback={this.handleSave} buttonTitle={edit ? 'Save' : 'Add'} />
        <Content>
          <ScrollView style={{ ...styles.scrollviewNopadding }}>
            <Form style={styles.form}>
              <Item style={{ ...styles.inlineInput, zIndex: 3 }} stackedLabel>
                <Label style={styles.label}>{'TARGET BODY PART'.toUpperCase()}</Label>
                <Input style={styles.input}
                  onSubmitEditing={this.removeFocus}
                  onChangeText={(text) => this.handleBodyPartInput(text)}
                  value={this.capitalizeFirstLetter(bodyPart)}
                />
                {bodyParts && bodyParts.length && openBodyPart ?
                  <HitDropDown
                    items={bodyParts}
                    handleItemSelect={(item) => this.handleSelectBodyPart(item)}
                  />
                : null }
              </Item>
              <Item style={{ ...styles.inlineInput, zIndex: 2 }} stackedLabel>
                <Label style={styles.label}>{'Exercise Name'.toUpperCase()}</Label>
                <Input style={styles.input}
                  onSubmitEditing={this.removeFocus}
                  onChangeText={(text) => this.handleExerciseInput(text)}
                  value={this.capitalizeFirstLetter(exercise)}
                />
                {exercises && exercises.length && openExercises ?
                  <HitDropDown
                    items={exercises}
                    handleItemSelect={(item) => this.handleSelectExercise(item)}
                  />
                : null }
              </Item>
              <View style={[styles.formRow, { zIndex: 1}]}>
                <Item style={{ ...styles.inlineInput, ...styles.inlineInputMargin }} stackedLabel>
                  <Label style={styles.label}>{'Number of Sets'.toUpperCase()}</Label>
                  <Input style={styles.input}
                    keyboardType='numeric'
                    onChangeText={(text) => this.setState({sets: this.numberify(text)})}
                    value={sets}
                  />
                </Item>
                <Item stackedLabel style={styles.inlineInput}>
                  <Label style={styles.label}>{'Number of Reps'.toUpperCase()}</Label>
                  <Input style={styles.input}
                    keyboardType='numeric'
                    onChangeText={(text) => this.setState({reps: this.numberify(text)})}
                    value={reps}
                  />
                </Item>
              </View>
              <View style={[styles.formRow, { zIndex: 1}]}>
                <Item style={{ ...styles.inlineInput, ...styles.inlineInputMargin }} stackedLabel>
                  <Label style={styles.label}>{'Rest'.toUpperCase()}</Label>
                  <Input style={styles.input}
                    keyboardType='numeric'
                    onChangeText={(text) => this.setState({rest: this.numberify(text)})}
                    value={rest}
                  />
                </Item>
                <Item stackedLabel style={styles.inlineInput}>
                  <Label style={styles.label}>{'Weight / Resistance'.toUpperCase()}</Label>
                  <Input style={styles.input}
                    keyboardType='numeric'
                    onChangeText={(text) => this.setState({load: this.numberify(text)})}
                    value={load}
                  />
                </Item>
              </View>
            </Form>
          </ScrollView>
        </Content>
      </Container>
    );
  }
}

const mapDispatchToProps = dispatch => bindActionCreators({
  addExercise,
  editExercise
}, dispatch);

export default connect(null, mapDispatchToProps)(AddExercise);