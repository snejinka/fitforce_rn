import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
  Header as NativeBaseHeader,
  Button,
  Icon,
  Text,
  Left,
  Body,
  Right,
} from 'native-base';
import { Image, TouchableHighlight } from 'react-native';
import BlueLogo from '../assets/images/logoFitforceBlue.png';
import { Actions } from 'react-native-router-flux';
import styles from '../assets/styles/index';
import ModalConfirm from '../components/ModalConfirm';


class Header extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            modalVisible:false
        }
    }
    
    handleAddClient(){
        if(this.props.clients.length +1 <= this.props.max_clients_count){
            Actions.addNewClient()
        }else {
            this.setState({
                modalVisible:true
            })
        }
    }

    render(){
        return(
            <NativeBaseHeader>
                <Left style={{flex:2}}>
                    {this.props.humburgerMenuVisible
                        ?
                        <Button
                            transparent
                            onPress={() => Actions.refresh({ key: '0_drawer', open: true })}
                        >
                            <Icon name="menu"/>
                        </Button>
                        :
                        <Button
                            transparent
                            onPress={() => Actions.pop()}
                        >
                            <Icon ios="ios-arrow-back" android="md-arrow-back"/>
                        </Button>
                    }

                </Left>
                <Body style={{flex:10}}>
                {this.props.headerWithLogo
                    ?
                    <Image source={BlueLogo}/>
                    :
                    <Text style={styles.headerTitle}>{this.props.title.toUpperCase()}</Text>
                }
                </Body>
                <Right style={{flex:2}}>
                    {this.props.buttonSearch &&
                    <Button
                        transparent
                        style={{paddingLeft:0,paddingRight:0,paddingTop:0,paddingBottom:0}}>
                        <Icon name="ios-search" style={styles.headerRightIcon} on/>
                    </Button>
                    }
                    {this.props.buttonAdd &&
                    <Button
                        transparent
                        onPress={() => this.props.plusButtonClick ? this.props.plusButtonClick() : this.handleAddClient()}
                        style={{paddingLeft:0,paddingRight:5,paddingTop:0,paddingBottom:0}}>
                        <Icon name="ios-add" style={{...styles.headerRightIcon, fontSize:30}}/>
                    </Button>
                    }
                    {!this.props.hideButton &&
                    <Button
                        transparent
                        onPress={() => this.props.rightButtonCallback()}
                    >
                        <Text style={styles.rightHeaderButton}>{this.props.buttonTitle ? this.props.buttonTitle : 'Save'}</Text>
                    </Button>
                    }
                </Right>
                <ModalConfirm
                    isVisible={this.state.modalVisible}
                    firstLineText="On your current plan can be a maximum of 3 client. To add a new client you need"
                    secondLineText="to upgrade your plan."
                    buttonNoText="Cancel"
                    buttonYesText="Ok"
                    handleNo={() => this.setState({modalVisible:false})}
                    handleYes={() => {this.setState({modalVisible:false});Actions.subscription()}}
                    handleClose={() => this.setState({modalVisible:false})}
                />
            </NativeBaseHeader>
        );
    }
}


const mapDispatchToProps = dispatch => bindActionCreators({
}, dispatch);

const mapStateToProps = state => ({
    user: state.user,
    clients: state.client.clients,
    isOnlineNetwork: state.network.isOnlineNetwork,
    appointments: state.appointment.appointments,
    success_last_payment: state.onboarding.success_last_payment,
    max_clients_count: state.onboarding.max_clients_count
});

export default connect(mapStateToProps, mapDispatchToProps)(Header);

