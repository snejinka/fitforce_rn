import React, { Component } from 'react';
import {
  Container,
  Content,
  Text,
  Form,
  Item,
  Label,
  Icon,
  Separator,
  ListItem,
  Body,
  Button,
  List,
  Left,
  Right,
} from 'native-base';
import { View, ScrollView, Image } from 'react-native';
import styles from '../assets/styles/index';
import Header from '../components/Header';

class FitnessProgram extends Component {
  render() {
    return (
      <Container>
        <Header title="Putting on Winter Mass" />
        <Content>
          <ScrollView style={{ ...styles.scrollviewNopadding }}>
            <Separator bordered style={styles.separator}>
              <Text>{'Fitness Program: Putting on winter mass'.toUpperCase()}</Text>
            </Separator>
            <Form style={styles.form}>
              <Item style={{ ...styles.inlineInput, ...styles.inlineInputUnbordered }} stackedLabel>
                <Label style={styles.label}>{'Difficulty Level'.toUpperCase()}</Label>
                <Text style={styles.formText}>Athlete</Text>
              </Item>
              <Item style={{ ...styles.inlineInput, ...styles.inlineInputUnbordered }} stackedLabel>
                <Label style={styles.label}>{'Program type'.toUpperCase()}</Label>
                <Text style={styles.formText}>Static Strength-training</Text>
              </Item>
              <Item style={{ ...styles.inlineInput, ...styles.inlineInputUnbordered }} stackedLabel>
                <Label style={styles.label}>{'Fitness Program Description'.toUpperCase()}</Label>
                <Text style={styles.formText} numberOfLines={5}>Static Strength-training Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto dignissimos dolorum ipsa maiores placeat quia quibusdam quos suscipit! Amet animi culpa doloremque excepturi ipsum iusto modi, optio tempora ullam voluptatibus! Delectus deleniti dolore modi, molestiae nihil numquam placeat sequi voluptas.</Text>
              </Item>
              <View style={styles.buttonHideContainer}>
                <Button iconLeft transparent style={styles.buttonHide}>
                  <Text style={styles.buttonHideText}>{'Hide'.toUpperCase()}</Text>
                </Button>
              </View>
              <Separator bordered style={{...styles.separator, ...styles.separatorInForm}}>
                <Text>{'Exercises'.toUpperCase()}</Text>
              </Separator>
              <Item style={{ ...styles.inlineInput, ...styles.noBorderBottom }} stackedLabel>
                <Button iconLeft transparent style={styles.buttonAdd}>
                  <Icon name="md-add" />
                  <Text style={styles.buttonAddText}>{'Add exercise'.toUpperCase()}</Text>
                </Button>
              </Item>
              <List style={styles.exercisesList}>
                <View style={styles.exercisesListLabelContainer}>
                  <Label style={styles.exercisesListLabel}>{'Quadriceps'}</Label>
                </View>
                <ListItem avatar style={styles.exercisesListItem}>
                  <Body style={styles.exercisesListItemBody}>
                  <Text style={styles.exercisesListName}>Barbell lunge </Text>
                  <Text note style={styles.exercisesListDescription} ellipsizeMode="tail" numberOfLines={1}>6 sets of 12 reps - Rest 20 s - Load n/a</Text>
                  </Body>
                  <Right style={styles.exercisesListItemRight}>
                    <Icon style={styles.exercisesListItemOptions} name="ios-more-outline" />
                  </Right>
                </ListItem>
                <ListItem avatar style={styles.exercisesListItem}>
                  <Body style={styles.exercisesListItemBody}>
                  <Text style={styles.exercisesListName}>Kumar Pratik</Text>
                  <Text note style={styles.exercisesListDescription} ellipsizeMode="tail" numberOfLines={1}>Doing what you like will always keep you happy</Text>
                  </Body>
                  <Right style={styles.exercisesListItemRight}>
                    <Icon style={styles.exercisesListItemOptions} name="ios-more-outline" />
                  </Right>
                </ListItem>
                <View style={styles.exercisesListLabelContainer}>
                  <Label style={styles.exercisesListLabel}>{'Shoulders'}</Label>
                </View>
                <ListItem avatar style={styles.exercisesListItem}>
                  <Body style={styles.exercisesListItemBody}>
                  <Text style={styles.exercisesListName}>Barbell lunge </Text>
                  <Text note style={styles.exercisesListDescription} ellipsizeMode="tail" numberOfLines={1}>6 sets of 12 reps - Rest 20 s - Load n/a</Text>
                  </Body>
                  <Right style={styles.exercisesListItemRight}>
                    <Icon style={styles.exercisesListItemOptions} name="ios-more-outline" />
                  </Right>
                </ListItem>
                <ListItem avatar style={styles.exercisesListItem}>
                  <Body style={styles.exercisesListItemBody}>
                  <Text style={styles.exercisesListName}>Kumar Pratik</Text>
                  <Text note style={styles.exercisesListDescription} ellipsizeMode="tail" numberOfLines={1}>Doing what you like will always keep you happy</Text>
                  </Body>
                  <Right style={styles.exercisesListItemRight}>
                    <Icon style={styles.exercisesListItemOptions} name="ios-more-outline" />
                  </Right>
                </ListItem>
                <ListItem avatar style={styles.exercisesListItem}>
                  <Body style={styles.exercisesListItemBody}>
                  <Text style={styles.exercisesListName}>Kumar Pratik</Text>
                  <Text note style={styles.exercisesListDescription} ellipsizeMode="tail" numberOfLines={1}>Doing what you like will always keep you happy</Text>
                  </Body>
                  <Right style={styles.exercisesListItemRight}>
                    <Icon style={styles.exercisesListItemOptions} name="ios-more-outline" />
                  </Right>
                </ListItem>
              </List>
            </Form>
          </ScrollView>
        </Content>
      </Container>
    );
  }
}

export default FitnessProgram;
