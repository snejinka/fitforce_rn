import React from 'react';
import { Image } from 'react-native';
import appStyles from '../assets/styles/index';

const source = require('../assets/images/logoFitforceFill.png');

const LogoImage = props => (
  <Image style={props.style ? props.style : appStyles.logoImage} source={source} />
);

export default LogoImage;
