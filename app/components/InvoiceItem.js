import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';
import {
  Text,
  Button,
  Icon
} from 'native-base';
import Switch from 'react-native-customisable-switch';
import { Image, View, ScrollView } from 'react-native';
import styles from '../assets/styles/index';
import Avatar from '../assets/images/appointmentAvatar1.png';
import moment from 'moment'


class InvoiceItem extends Component {
  constructor(props){
    super(props);
    this.state = {
      clientName:''
    }
  }

  componentWillMount(){
    this.renderClientName()
  }

  renderDate(date){
    return moment(date).format('MMM DD, YYYY')
  }

  renderIcon(){
    if(!this.props.invoice.paid){
      return (
          <Icon style={styles.exercisesListItemOptions} name="ios-more-outline" />
      )
    }
  }

  renderSwitch(){
    if(!this.props.invoice.paid){
      return (
          <View style={{...styles.invoiceItemBodyRow, ...styles.invoiceItemBodyRowMargin}}>
            <Text style={{...styles.invoiceItemText, ...styles.invoiceItemTextTotal}}>Mark as Paid:</Text>
            <View style={{...styles.switchContainer, ...styles.invoiceItemSwitchContainerRight}}>
              <Switch switchWidth={36}
                      switchHeight={22}
                      buttonWidth={20}
                      buttonHeight={20}
                      padding={false}
                      activeBackgroundColor={'rgb(79, 155, 235)'}
                      inactiveBackgroundColor={'rgba(157, 171, 194, 0.4)'}
                      style={{marginTop:0, marginBottom:0}}
              />
            </View>
          </View>
      )
    }
  }

  renderClientName(){
    this.props.clients && this.props.clients.map((client) => {
      if(this.props.invoice.client_id == client.id){
        this.setState({clientName:`${client.first_name} ${client.last_name}`})
      }
    })
  }

  render() {
    const {invoice} = this.props;
    return (
        <View style={styles.invoiceItem}>
          <View style={styles.invoiceItemHeader}>
            <View style={{...styles.invoiceItemHeaderText, ...styles.invoiceItemHeaderTextMargin}}>
              <Image style={styles.invoiceItemHeaderImage} source={Avatar}/>
              <Text style={styles.invoiceItemTitle}>{this.state.clientName} - Invoice #10</Text>
              {this.renderIcon()}
            </View>
            <Text style={styles.invoiceItemInfoTitle}>Created on: <Text style={styles.invoiceItemInfoText}>{this.renderDate(invoice.timestamp_created)}</Text></Text>
            <Text style={styles.invoiceItemInfoTitle}>Due Date: <Text style={styles.invoiceItemInfoText}>{this.renderDate(invoice.timestamp_due)}</Text></Text>
            <Text style={{...styles.invoiceItemInfoTitle,...styles.textWarning }}>Overdue by 5 days</Text>
          </View>
          <View style={styles.invoiceItemBody}>
            {invoice.appointments.map((item) => {
              return(
                  <View
                      style={[styles.invoiceItemBodyRow,{marginBottom:5}]}>
                    <Text style={styles.invoiceItemText}>3 Fitness Session</Text>
                    <Text style={{...styles.invoiceItemText,...styles.invoiceItemTextRight}}>$100.00</Text>
                  </View>
              )
            })}

            <View style={styles.invoiceItemBodyRow}>
              <Text style={{...styles.invoiceItemText, ...styles.invoiceItemTextTotal}}>Total:</Text>
              <Text style={{...styles.invoiceItemText,...styles.invoiceItemTextRight}}>$100.00</Text>
            </View>
            <View style={styles.invoiceItemBodyRow}>
              <Text style={{...styles.invoiceItemText, ...styles.invoiceItemTextTotal}}>Tax (7%):</Text>
              <Text style={{...styles.invoiceItemText,...styles.invoiceItemTextRight}}>$100.00</Text>
            </View>
            <View style={styles.invoiceItemBodyRow}>
              <Text style={{...styles.invoiceItemText, ...styles.invoiceItemTextTotal, ...styles.textBold}}>Total (tax incl.):</Text>
              <Text style={{...styles.invoiceItemText,...styles.invoiceItemTextRight}}>$100.00</Text>
            </View>
            {this.renderSwitch()}
          </View>
        </View>
    );
  }
}

InvoiceItem.propTypes = {
  appointment: PropTypes.object,
  invoice: PropTypes.object,
}

const mapDispatchToProps = dispatch => bindActionCreators({
}, dispatch);

const mapStateToProps = state => ({
  clients: state.client.clients,
  appointments: state.appointment.appointments,
});

export default connect(mapStateToProps, mapDispatchToProps)(InvoiceItem);

