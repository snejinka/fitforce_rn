import React from 'react';
import { Provider } from 'react-redux';
import configureStore from '../store/configureStore';
import CoreLayout from './CoreLayout';
import { RealmProvider } from 'react-native-realm';
import realm from '../models/RealmModels';

const store = configureStore();

const App = () => (
  <RealmProvider realm={realm}>
      <Provider store={store}>
          <CoreLayout />
      </Provider>
  </RealmProvider>
);

export default App;
