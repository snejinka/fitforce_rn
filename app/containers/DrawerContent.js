import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { BackHandler, Image } from 'react-native';
import { Text, View, List, ListItem, Left, Body } from 'native-base';
import { Actions as NavigationActions } from 'react-native-router-flux';
import styles from '../assets/styles/index';
import { logout } from '../store/user';
import SidebarAvatar from '../assets/images/sidebarAvatar.png';
import SidebarProfile from '../assets/images/sidebarProfile.png';
import SidebarProgram from '../assets/images/sidebarProgram.png';
import SidebarDollar from '../assets/images/dollar.png';
import SidebarSettings from '../assets/images/gear.png';
import SidebarSupport from '../assets/images/support.png';
import SidebarTerms from '../assets/images/termsCond.png';
import SidebarRate from '../assets/images/rate.png';
import SidebarLogout from '../assets/images/logout.png';
import {hardDeleteAppointments} from '../store/appointment'

class DrawerContent extends Component {
  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', () => {
      if (this.context.drawer.props.open) {
        this.toggleDrawer();
        return true;
      }
      return false;
    });
  }

  toggleDrawer() {
    this.context.drawer.toggle();
  }

  render() {
    const drawerOpen = this.context.drawer.props.open;
    const { user } = this.props;
    const { email, firstName, lastName } = user;
    let name = user.name;

    if (firstName && lastName) {
      name = `${firstName} ${lastName}`;
    }

    return (
      <View style={drawerOpen ? styles.sidebarContainerShadow : styles.sidebarContainer}>
        <View style={styles.sidebarHeader}>
          <Image style={styles.sidebarAvatar} source={SidebarAvatar} />
          { name !== email &&
            <Text style={styles.sidebarName}>{name}</Text>
          }
          <Text style={styles.sidebarEmail}>{email}</Text>
        </View>
        <View style={styles.sidebarBody}>
          <List>
            <ListItem
              icon
              style={{ ...styles.sidebarMenuItem, }}
              onPress={NavigationActions.myProfile}
            >
              <Left>
                <Image source={SidebarProfile} />
              </Left>
              <Body style={styles.sidebarMenuItemBody}>
                <Text style={styles.sidebarMenuItemText}>My Profile</Text>
              </Body>
            </ListItem>
            <ListItem
              icon
              style={styles.sidebarMenuItem}
              onPress={NavigationActions.fitnessPrograms}
            >
              <Left>
                <Image source={SidebarProgram} />
              </Left>
              <Body style={styles.sidebarMenuItemBody}>
                <Text style={styles.sidebarMenuItemText}>Fitness programs</Text>
              </Body>
            </ListItem>
            <ListItem
              icon
              style={styles.sidebarMenuItem}
              onPress={NavigationActions.subscription}
            >
              <Left>
                <Image source={SidebarDollar} />
              </Left>
              <Body style={styles.sidebarMenuItemBody}>
                <Text style={styles.sidebarMenuItemText}>Subscription</Text>
              </Body>
            </ListItem>
            <ListItem
              icon
              style={styles.sidebarMenuItem}
              onPress={NavigationActions.settings}
            >
              <Left>
                <Image source={SidebarSettings} />
              </Left>
              <Body style={styles.sidebarMenuItemBody}>
                <Text style={styles.sidebarMenuItemText}>Settings</Text>
              </Body>
            </ListItem>
            <ListItem icon style={styles.sidebarMenuItem}>
              <Left>
                <Image source={SidebarSupport} />
              </Left>
              <Body style={styles.sidebarMenuItemBody}>
                <Text style={styles.sidebarMenuItemText}>Support</Text>
              </Body>
            </ListItem>
            <ListItem icon style={styles.sidebarMenuItem}>
              <Left>
                <Image source={SidebarTerms} />
              </Left>
              <Body style={styles.sidebarMenuItemBody}>
                <Text style={styles.sidebarMenuItemText}>Terms & conditions</Text>
              </Body>
            </ListItem>
            <ListItem icon style={styles.sidebarMenuItem}>
              <Left>
                <Image source={SidebarRate} />
              </Left>
              <Body style={styles.sidebarMenuItemBody}>
                <Text style={styles.sidebarMenuItemText}>Rate FitForce</Text>
              </Body>
            </ListItem>
            <ListItem 
                icon 
                style={styles.sidebarMenuItem} 
                onPress={() => {
                  this.props.logout()
                  this.props.hardDeleteAppointments()
                  }}>
              <Left>
                <Image source={SidebarLogout} />
              </Left>
              <Body style={styles.sidebarMenuItemBody}>
                <Text style={styles.sidebarMenuItemText}>Log out</Text>
              </Body>
            </ListItem>
          </List>
        </View>
      </View>
    );
  }

}
DrawerContent.contextTypes = {
  drawer: React.PropTypes.object,
};

const mapDispatchToProps = dispatch => bindActionCreators({
  logout,
  hardDeleteAppointments
}, dispatch);

const mapStateToProps = state => ({ user: state.user });

export default connect(mapStateToProps, mapDispatchToProps)(DrawerContent);
