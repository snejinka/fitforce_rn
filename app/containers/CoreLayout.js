import React from 'react';
import { Root, Container,Text } from 'native-base';
import NavigationRouter from '../navigations/NavigationRouter';
import BlockScreen from '../scenes/BlockScreen';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {refreshAuth0Token, logout, setSession} from '../store/user';
import {AsyncStorage} from 'react-native';
import {isTokenExpired} from '../utils/jwtDecodeToken';
import {getClientsInfo} from '../store/client';
import {getAppointments,hardDeleteAppointments} from '../store/appointment';
import {getTrainerInfo} from '../store/onboarding';
import {setNetworkStatus} from '../store/network';
import { Actions } from 'react-native-router-flux';
import { sessionStatus } from '../constants';
import {getFitnessPrograms} from '../store/program';



class RootContainer extends React.Component{
    constructor(props){
        super(props);
        this.getToken = async () => {
            try {
                const refreshToken = await AsyncStorage.getItem('refreshToken');
                const token = await AsyncStorage.getItem('token');
                if (refreshToken !== null){
                    if(isTokenExpired(token)){
                        this.props.refreshAuth0Token(refreshToken).then(res => {
                            this.props.getTrainerInfo().then(() => {
                                if(!this.props.success_last_payment && this.props.user.session != sessionStatus.guest){
                                    if(this.props.clients.length > this.props.max_clients_count){
                                        this.props.setSession(sessionStatus.blocked);
                                    }else {
                                        this.props.setSession(sessionStatus.authorized);
                                    }
                                }else {
                                    this.props.setSession(sessionStatus.authorized);
                                }
                            })
                            this.props.getClientsInfo()
                            this.props.getAppointments()
                            this.props.getFitnessPrograms()
                        })
                    }else{
                        this.props.getTrainerInfo().then(() => {
                            if(!this.props.success_last_payment && this.props.user.session != sessionStatus.guest){
                                if(this.props.clients.length > this.props.max_clients_count ){
                                    this.props.setSession(sessionStatus.blocked);
                                }else {
                                    this.props.setSession(sessionStatus.authorized);
                                }
                            }else {
                                this.props.setSession(sessionStatus.authorized);
                            }
                        })
                        this.props.getClientsInfo()
                        this.props.getAppointments()
                        this.props.getFitnessPrograms()
                    }
                }else{
                    this.props.logout()
                    this.props.hardDeleteAppointments()
                }
            } catch (error) {
                this.props.logout()
                this.props.hardDeleteAppointments()
            }
        }
    }
    componentWillMount(){
        this.getToken()
        this.props.setNetworkStatus();
    }

    render(){
        return(
            <Root>
                <Container style={{ flex: 1 }} >
                    <NavigationRouter user={this.props.user}/>
                </Container>
            </Root>
        )
    }
}

const mapDispatchToProps = dispatch => bindActionCreators({
    refreshAuth0Token,
    logout,
    getClientsInfo,
    getAppointments,
    getTrainerInfo,
    getFitnessPrograms,
    setNetworkStatus,
    setSession,
    hardDeleteAppointments
}, dispatch);

const mapStateToProps = state => ({
    user: state.user,
    success_last_payment: state.onboarding.success_last_payment,
    max_clients_count: state.onboarding.max_clients_count,
    clients: state.client.clients,
});

export default connect(mapStateToProps, mapDispatchToProps)(RootContainer);
