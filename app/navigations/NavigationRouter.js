import React from 'react';
import { connect } from 'react-redux';
import { Scene, Router, Switch, Actions, ActionConst } from 'react-native-router-flux';
import styles from '../assets/styles/index';
import NavigationDrawer from './NavigationDrawer';

import Dashboard from '../scenes/MainScenes/Dashboard';
import Invoices from '../scenes/MainScenes/Invoices';
import Messages from '../scenes/MainScenes/Messages';
import CalendarView from '../scenes/MainScenes/CalendarView';
import Clients from '../scenes/MainScenes/Clients';

import AddNewClient from '../scenes/Client/AddNewClient';
import EditClient from '../scenes/Client/EditClient';
import ViewClient from '../scenes/Client/ViewClient';
import DeleteClients from '../scenes/Client/DeleteClients';

import FitnessRecord from '../components/FitnessRecord';
import Subscription from '../scenes/Subscription';
import BlockScreen from '../scenes/BlockScreen';
import MyProfile from '../scenes/Trainer/MyProfile';
import Settings from '../scenes/Trainer/Settings';
import FitnessProgram from '../components/FitnessProgram';
import StartFitnessProgram from '../components/StartFitnessProgram';
import AddExercise from '../components/AddExercise';
import CreateInvoice from '../scenes/Invoice/CreateInvoice';
import CreateInvoiceUnpaidSelected from '../components/CreateInvoiceUnpaidSelected';

import CreateAppointment from '../scenes/Appointment/CreateAppointment';
import EditAppointment from '../scenes/Appointment/EditAppointment';
import ViewAppointment from '../scenes/Appointment/ViewAppointment';

import FitnessPrograms from '../scenes/FitnessPrograms/FitnessPrograms';
import AddFitnessProgram from '../scenes/FitnessPrograms/AddFitnessProgram';
import FitnessProgramView from '../scenes/FitnessPrograms/FitnessProgramView';

import SearchResults from '../components/SearchResults';
import ChatView from '../components/ChatView';
import Signin from '../scenes/Guest/Signin';
import Signup from '../scenes/Guest/Signup/Signup';
import SignupFinish from '../scenes/Guest/Signup/SignupFinish';
import ForgotPassword from '../scenes/Guest/ForgotPassword';
import Welcome from '../scenes/Guest/Welcome';
import StartStep from '../scenes/Onboarding/StartStep';
import AddressStep from '../scenes/Onboarding/AddressStep';
import ServiceStep from '../scenes/Onboarding/ServiceStep';
import UnitsStep from '../scenes/Onboarding/UnitsStep';
import CurrencyStep from '../scenes/Onboarding/CurrencyStep';

const NavigationRouter = (props) => {
  const initialKey = props => props.user.session;
  return (
    <Router>
      <Scene
        key="root"
        component={connect(state => ({ user: state.user }))(Switch)}
        tabs
        unmountScenes
        selector={initialKey}
      >
        <Scene key="authorized">
          <Scene key="drawer" component={NavigationDrawer} open={false} type={ActionConst.RESET}>
            <Scene initial key="dashboard" component={Dashboard} title="Dashboard" type={ActionConst.RESET}/>
            <Scene key="clients" component={Clients} title="Clients" type={ActionConst.RESET}/>
            <Scene key="messages" component={Messages} title="Messages" />
            <Scene key="calendarView" component={CalendarView} title="CalendarView"/>
            <Scene key="invoices" component={Invoices} title="Invoices"/>
          </Scene>
          <Scene key="subscription" component={Subscription} hideNavBar />
          <Scene key="addNewClient" component={AddNewClient} title="AddNewClient" hideNavBar />
          <Scene key="editClient" component={EditClient} title="EditClient" hideNavBar />
          <Scene key="deleteClients" component={DeleteClients} hideNavBar />
          <Scene key="viewClient" component={ViewClient} title="ViewClient" hideNavBar />
          <Scene key="fitnessRecord" component={FitnessRecord} title="FitnessRecord" hideNavBar />
          <Scene key="myProfile" component={MyProfile} title="MyProfile" hideNavBar />
          <Scene key="settings" component={Settings} title="Settings" hideNavBar />
          <Scene key="fitnessProgram" component={FitnessProgram} title="FitnessProgram" hideNavBar />
          <Scene key="startFitnessProgram" component={StartFitnessProgram} title="StartFitnessProgram" hideNavBar />
          <Scene key="fitnessPrograms" component={FitnessPrograms} title="FitnessPrograms" hideNavBar />
          <Scene key="fitnessProgramView" component={FitnessProgramView} title="FitnessProgramView" hideNavBar />
          <Scene key="addFitnessProgram" component={AddFitnessProgram} title="AddFitnessProgram" hideNavBar />
          <Scene key="addExercise" component={AddExercise} title="AddExercise" hideNavBar />
          <Scene key="createInvoice" component={CreateInvoice} hideNavBar />
          <Scene key="createAppointment" component={CreateAppointment} title="CreateAppointment" hideNavBar />
          <Scene key="editAppointment" component={EditAppointment} title="EditAppointment" hideNavBar />
          <Scene key="viewAppointment" component={ViewAppointment} title="ViewAppointment" hideNavBar />
          <Scene key="searchResults" component={SearchResults} title="SearchResults" hideNavBar />
          <Scene key="chatView" component={ChatView} title="ChatView" hideNavBar />
        </Scene>
        <Scene key="guest">
          <Scene initial key="welcome" component={Welcome} title="" navigationBarStyle={styles.transparent} leftButtonIconStyle={styles.leftButtonIconStyle} />
          <Scene key="signin" component={Signin} title="" navigationBarStyle={styles.transparent} leftButtonIconStyle={styles.leftButtonIconStyle} />
          <Scene key="forgotPassword" component={ForgotPassword} title="" navigationBarStyle={styles.transparent} leftButtonIconStyle={styles.leftButtonIconStyle} />
          <Scene key="signup" component={Signup} title="" navigationBarStyle={styles.transparent} leftButtonIconStyle={styles.leftButtonIconStyle} />
          <Scene key="signupFinish" component={SignupFinish} title="" navigationBarStyle={styles.transparent} leftButtonIconStyle={styles.leftButtonIconStyle} />
          <Scene key="onboardingStart" component={StartStep} title="" navigationBarStyle={styles.transparent} leftButtonIconStyle={styles.opacity} />
          <Scene key="addressStep" component={AddressStep} title="" navigationBarStyle={styles.transparent} leftButtonIconStyle={styles.leftButtonIconStyle} />
          <Scene key="serviceStep" component={ServiceStep} title="" navigationBarStyle={styles.transparent} leftButtonIconStyle={styles.leftButtonIconStyle} />
          <Scene key="unitsStep" component={UnitsStep} title="" navigationBarStyle={styles.transparent} leftButtonIconStyle={styles.leftButtonIconStyle} />
          <Scene key="currencyStep" component={CurrencyStep} title="" navigationBarStyle={styles.transparent} leftButtonIconStyle={styles.leftButtonIconStyle} />
        </Scene>
        <Scene key="blocked">
          <Scene initial key="blockScreen" component={BlockScreen} hideNavBar/>
          <Scene key="deleteClients" component={DeleteClients} hideNavBar />
        </Scene>
      </Scene>
    </Router>
  );
};

export default NavigationRouter;
