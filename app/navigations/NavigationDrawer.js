import React from 'react';
import {
  Drawer
} from 'native-base';
import { Image, TouchableHighlight } from 'react-native';
import { DefaultRenderer, Actions as NavigationActions } from 'react-native-router-flux';
import DrawerContent from '../containers/DrawerContent';
import BlueLogo from '../assets/images/logoFitforceBlue.png';
import styles from '../assets/styles/index';
import { Actions } from 'react-native-router-flux';


const NavigationDrawer = (props) => {
  const state = props.navigationState;
  const children = state.children;
  return (
    <Drawer
      open={state.open}
      onOpen={() => NavigationActions.refresh({ key: state.key, open: true })}
      onClose={() => NavigationActions.refresh({ key: state.key, open: false })}
      content={<DrawerContent />}
      tapToClose
    >
      <DefaultRenderer navigationState={children[state.index]} onNavigate={props.onNavigate} />
    </Drawer>
  );
};

export default NavigationDrawer;
