import { AppRegistry } from 'react-native';
import App from './app/containers/App';
import './ReactotronConfig';
import './app/utils/storage';

AppRegistry.registerComponent('FitForce', () => App);
